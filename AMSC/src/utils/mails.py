import os
import smtplib
import pandas as pd
from datetime import datetime
from email.mime.text import MIMEText
from email.message import EmailMessage
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication

class Mail():
    def __init__(self):
        self.username = os.environ.get("AMSC_mail_username")
        self.token = os.environ.get("AMSC_mail_token")

    def send_mail_descarga_amsc(self, 
            destinatario,
            resultados):
        msg = MIMEMultipart()
        msg["Subject"] = f"Descarga Antioquia Mira Su cielo"
        msg["From"] = self.username
        msg["To"] = destinatario

        df = pd.DataFrame(data = resultados["Resultados"], columns= list(map(lambda x: x.split(".")[-1],resultados["Columnas"])))
        path_archivo = f"tmp/mails_amsc/{destinatario.split('@')[0].split('.')[0]}_{str(datetime.now()).replace(':','_').replace(' ','_').split('.')[0]}.csv"
        df.to_csv(f"{path_archivo}")

        html = f"""En los archivos de este correo usted encontrará un archivo de tipo <i> CSV </i>, llamado <b>{path_archivo.split('/')[-1]}</b>. En el cual se encuentra
                toda la información solicitada por la página de Antioquia Mira Su Cielo &#9925 
                <a href='http://54.146.188.73:8501/'>Antioquia Mira Su Cielo</a>."""
        part = MIMEText(html, "html")
        msg.attach(part)

        with open(f"{path_archivo}", "rb") as file:
            msg.attach(MIMEApplication(file.read(), name = f"{path_archivo.split('/')[-1]}"))

        with smtplib.SMTP("smtp.gmail.com", 587) as smtp:
            smtp.ehlo()
            smtp.starttls()
            smtp.ehlo()
            smtp.login(self.username, self.token)
            smtp.send_message(msg)
        os.remove(f"{path_archivo}")
        return df.head(5)
if __name__ == "mail_obj":
    mail_obj = Mail()
    mail_obj.send_Mail("juliosimonpedro@gmail.com")