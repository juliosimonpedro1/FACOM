resource "aws_organizations_organizational_unit" "FACOM_OU" {
  name      = "FACOM_OU"
  parent_id = "r-hhcv"
}

resource "aws_organizations_account" "FACOM" {
  name  = "FACOM"
  email = "esteban.silva.villa@gmail.com"
  parent_id = "ou-hhcv-iziigguz"
}
