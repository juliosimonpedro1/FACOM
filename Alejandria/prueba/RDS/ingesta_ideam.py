import requests
import json
import pandas as pd
import psycopg2
from psycopg2.extras import RealDictCursor
from sqlalchemy import create_engine

##Conectar a postgres

hostname="database-1.c6l43uy718kg.us-east-1.rds.amazonaws.com"
username="postgres"
password="Cripi123456+"
database="prueba_ideam"

# conn=psycopg2.connect(
#     host=hostname,
#     database=database,
#     user=username,
#     password=password
# )

engine=create_engine(
    f"postgresql://{username}:{password}@{hostname}/{database}"
)

auth={"X-App-Token":"iiNW2MaM6IEhc9ryL8yT2Ouz8"}


def ingesta_ideam_func():

    global engine, auth


    params={"$where":'fechaobservacion < "2022-10-26T23:59:00.000" AND fechaobservacion > "2022-10-25T23:59:59.000"',
            "$select": "codigoestacion, fechaobservacion, valorobservado",
            "$limit":1000000}

    codigos_var={"temperatura": "sbwg-7ju4",
                "precipitacion": "s54a-sgyg",
                "presion":"62tk-nxj5",
                "direccion_viento":"kiw7-v9ta",
                "velocidad_viento":"sgfv-3yp8",
                "humedad":"uext-mhny"}

    for var in codigos_var.keys():
        print(var)
        r=requests.get(f'https://www.datos.gov.co/resource/{codigos_var[var]}',
            headers=auth, params=params)
        r=r.json()
        db=pd.DataFrame(r)
        
        db.to_sql(name=var, con=engine, schema="public", if_exists="replace")
    
def ingesta_estaciones():

    global engine, auth

    r=requests.get("https://www.datos.gov.co/resource/hp9r-jxuu.json", headers=auth, params={"$limit":9000})
    r=r.json()
    resp=r
    estaciones=[]
    
    for r in resp:

        estaciones.append({
            "codigo":r["codigo"],
            "nombre":r["nombre"],
            "categoria":r["categoria"],
            "tecnologia":r["tecnologia"],
            "estado":r["estado"],
            "departamento":r["departamento"],
            "municipio":r["municipio"],
            "altitud":r["altitud"],
            "latitud":r["ubicaci_n"]["latitude"],
            "longitude":r["ubicaci_n"]["longitude"]
        })

    db=pd.DataFrame(estaciones)
    db.to_sql(name="estaciones", con=engine, schema="public", if_exists="replace")

def ingesta_agregado_temp():
    
    global engine, auth

    params={"$where":'fechaobservacion < "2022-10-26T23:59:00.000" AND fechaobservacion > "2022-10-25T23:59:59.000"',
            "$select": "codigoestacion, fechaobservacion, valorobservado",
            "$limit":1000000}

    r=requests.get("https://www.datos.gov.co/resource/sbwg-7ju4", headers=auth, params=params)
    r=r.json()

    db=pd.DataFrame(r)
    db["valorobservado"]=db["valorobservado"].astype(float)
    db=db[db["valorobservado"]<15]

    db.to_sql(name="temp_mayor", con=engine, schema="agregado", if_exists="replace")
# ingesta_ideam_func()
# ingesta_estaciones()
ingesta_agregado_temp()