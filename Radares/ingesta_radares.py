import os
import json
import boto3
import pyart
import shutil
import netCDF4
import glob
import logging
import numpy as np
import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
from botocore import UNSIGNED
from botocore.config import Config
from pyart.config import get_metadata
from matplotlib.transforms import Bbox
from datetime import datetime, timedelta

path=os.getcwd()
# os.chdir(path + "/Radares") 
t1 = datetime.now()

#Crear la carpeta de listas de radares si no está
if "lista_radares" not in os.listdir():
    os.mkdir("lista_radares")

#Definir cliente s3
s3_client=boto3.client("s3", config=Config(signature_version=UNSIGNED), region_name="us-east-1")
Bucket="s3-radaresideam"
Prefix='l2_data'

#Log file
logging.basicConfig(filename="radar_logs.txt", 
                    filemode='w', 
                    level=logging.INFO, 
                    format = "[%(asctime)s] %(levelname)s:  %(message)s",
                    datefmt = '%m-%d %H:%M:%S')

with open("datos_radares.json", "r") as file:
    radares=json.load(file)

class clase_radar:
    global radares, Bucket,Prefix, s3_client, path

    #Definir los atributos de cada radar
    def __init__(self, nombre):
        self.nombre=nombre
        self.folder_s3=radares[self.nombre]["Nombre s3"] #Nombre en s3
        self.lat=radares[self.nombre]["Coordenadas"]["lat"] #Latitud del radar
        self.lon=radares[self.nombre]["Coordenadas"]["lon"] #Longitud del radar
    
    def ingesta(self):
        if self.folder_s3 not in os.listdir("lista_radares"):
            os.mkdir("lista_radares/"+self.folder_s3)
        fecha=datetime.now()# + timedelta(hours=5)
        logging.info(fecha)
        dia=fecha.strftime("%d")
        mes=fecha.strftime("%m")
        ano=fecha.strftime("%Y")

        path_aws=Prefix+f"/{str(ano)}/{str(mes)}/{str(dia)}/{self.folder_s3}"
        logging.info(f"El path inicial es: {path_aws}")
        while "Contents" not in list(s3_client.list_objects(Bucket=Bucket, Prefix=path_aws).keys()):
            fecha=fecha-timedelta(days=1)
            path_aws=Prefix+f"/{fecha.strftime('%Y')}/{fecha.strftime('%m')}/{(fecha).strftime('%d')}/{self.folder_s3}/"
        
        logging.info(f"En la ingesta se encontró este que este path tiene contenido --> {path_aws}")
        paginator=s3_client.get_paginator('list_objects_v2') 
        pages=paginator.paginate(Bucket=Bucket, Prefix=path_aws)
        

        datos = []
        for page in pages:
            datos.extend(page["Contents"])
        
        datos_df = pd.DataFrame(datos)[["Key","LastModified"]]
        datos_df = datos_df[datos_df.LastModified >= datos_df.LastModified.max() - timedelta(minutes= 3)]
        datos_df = datos_df[~datos_df.Key.str.contains(".nc.gz")]
        self.archivos = datos_df.Key.tolist()
        self.archivos = list(map(lambda x: x.split('/')[-1], self.archivos))

        for Key in datos_df.Key:
            objeto = s3_client.get_object(Bucket = Bucket, Key = Key)["Body"]
            try: 
                with open(f"lista_radares/{self.folder_s3}/{Key.split('/')[-1]}", "wb") as f:
                    shutil.copyfileobj(objeto, f)
                logging.info(f"Se copió con éxito el archivo {Key.split('/')[-1]}")
            except:
                logging.error(f"Se encontró un error copiando el archivo {Key.split('/')[-1]}")

    def plot(self):
        nc_list = ["Bogotá", "Santa Elena"]
        archivos = {}

        plots = glob.glob(f"lista_radares/{self.folder_s3}/*.png")

        for file_erase in plots:
            os.remove(file_erase)

        for file in self.archivos:
            archivo = f"lista_radares/{self.folder_s3}/{file}"
            shape = 1000

            if self.nombre in nc_list:
                dataset = xr.open_dataset(archivo, decode_times=False)
                radar = pyart.testing.make_empty_ppi_radar(dataset.range.shape[0], dataset.azimuth.shape[0], 1)
                radar.time["data"] = np.array(dataset["time"][:])
                radar.elevation["data"] = dataset.elevation.values
                radar.azimuth["data"] = dataset.azimuth.values
                radar.range["data"]=dataset.range.values
                radar.latitude["data"] = np.array([float(dataset.latitude.values)])
                radar.longitude["data"] = np.array([float(dataset.longitude.values)])
                radar.sweep_number["data"] = np.array(dataset["sweep"])
                ref_dict = get_metadata("DBZH")
                ref_dict["data"] = dataset.DBZH.values
                radar.fields = {"DBZH": ref_dict}
                fields=["DBZH"]
                fecha = dataset.time.units.replace("since", "").split(" ")[-1] if self.nombre == "Santa Elena" else dataset.time.units.split(" ")[-1]

            else:
                radar = pyart.io.read(archivo)
                fields = ["reflectivity"]
                fecha = radar.time["units"].split(" ")[-1]
            elevacion = radar.elevation["data"][0]
            radio = radar.range["data"].max() * np.cos(np.radians(elevacion))
           
            nombre_archivo = file.split('.')[0]
            archivos[nombre_archivo] = {}
            archivos[nombre_archivo]["Radio"] = float(radio)
            archivos[nombre_archivo]["Elevacion"] = float(elevacion)
            archivos[nombre_archivo]["Fecha"] = str(datetime.strptime(fecha.replace("T"," ").replace("Z",""), "%Y-%m-%d %H:%M:%S") - timedelta(hours = 5))

            grid=pyart.map.grid_from_radars(
            radar,
            grid_shape=(1,shape, shape),
            grid_limits=((0,10000), (-radio, radio), (-radio, radio)),
            fields=fields
            )


            pyart.io.write_grid(
            grid=grid,
            filename=f"lista_radares/{self.folder_s3}/grid_{file.split('.')[0]}.nc",
            include_fields=fields[0],
            format="NETCDF4"
            )

            logging.info(f"Se acaba de hacer la conversión a objeto de grilla {file}")

            nc = netCDF4.Dataset(f"lista_radares/{self.folder_s3}/grid_{file.split('.')[0]}.nc")
            ref=nc.variables[fields[0]][::,::]
            fig=plt.figure(figsize=(15,15))
            plt.imshow(np.where(ref[0][0].filled(np.nan)<0, np.nan, ref[0][0].filled(np.nan)),
                origin="lower", cmap = pyart.graph.cm.NWSRef, vmin=0, vmax=70)
            plt.axis("off")
            bbox=Bbox([[0,0],[1000,1000]])
            bbox = bbox.transformed(fig.gca().transData).transformed(fig.dpi_scale_trans.inverted())
            plt.savefig(f"lista_radares/{self.folder_s3}/plot_{file.split('.')[0]}.png", transparent=True, bbox_inches=bbox)

            os.remove(f"lista_radares/{self.folder_s3}/{file}")
            os.remove(f"lista_radares/{self.folder_s3}/grid_{file.split('.')[0]}.nc")
            
        with open(f"lista_radares/{self.folder_s3}/{self.folder_s3}.json", "w") as f:
            json.dump(archivos, f) 

for radar in radares.keys():
    logging.info(f"Se comienza a procesar {radar}")
    radares[radar]["Objeto"]=clase_radar(radar)
    logging.info("Se comienza la ingesta")
    radares[radar]["Objeto"].ingesta()
    logging.info("Se comenza a crear el plot del radar")
    radares[radar]["Objeto"].plot()
    logging.info("/////////////////////////////////////////////////////////////////////////////////////////////")

logging.info(f"El script se demoró corriendo {str(datetime.now()-t1)}")