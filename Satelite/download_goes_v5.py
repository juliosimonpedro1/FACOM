#Version 5.0 
# 1. Las variables en los archivos .nc se definen como enteros para ahorrar espacio:
# --- i2 (int16) ---> x, y, Rad, LST
# --- i4 (int32) ---> goes imager projection
# --- u2 (uint16) ---> LVM
# --- f4 (float32) ---> pressure
# 2. Se introduce la funcion "def_coord" para crear las coordenadas del dataset.
# 3. Se introduce la funcion "create_Cim", que es una generalización de las anteriores "create_{*}" (Aun no se ha probado)

import GOES
import pandas as pd
import os
from netCDF4 import Dataset
from pyproj import Proj
import numpy as np

def def_coord(data, ds, y, x):
    ''' data: Dataset in
        ds: Dataset out '''
    
    x_par = {'scale_factor': data.variables['x'].scale_factor,
            'add_offset': data.variables['x'].add_offset,
            'units': 'rad',
            'axis': 'X'}
    
    y_par = {'scale_factor': data.variables['y'].scale_factor,
            'add_offset': data.variables['y'].add_offset,
            'units': 'rad',
            'axis': 'Y'}
    
    #Crear dimensiones
    ds.createDimension('y',len(y))
    ds.createDimension('x',len(x))
    
    #Agregar variables
    ys = ds.createVariable('y','i2',('y',))
    xs = ds.createVariable('x','i2',('x',))
    
    ys.setncatts({k: data.variables['y'].getncattr(k) for k in data.variables['y'].ncattrs()})
    xs.setncatts({k: data.variables['x'].getncattr(k) for k in data.variables['x'].ncattrs()})
    
    '''
    ys.scale_factor = y_par['scale_factor']
    ys.add_offset = y_par['add_offset']
    ys.unis = y_par['units']
    ys.axis = y_par['axis']
    xs.scale_factor = x_par['scale_factor']
    xs.add_offset = x_par['add_offset']
    xs.unis = x_par['units']
    xs.axis = x_par['axis']
    '''
    
    return(ys,xs)

#############################################################################
    
def create_LVMP(data,y,x,pres,val,gip,path,out_name):
    '''Create a dataset and write to a netCDF file. 
    This function was designed for LVMPF images'''
    
    ds = Dataset(path+out_name,'w',format='NETCDF4')
    
    #Crear dimensiones
    ys, xs = def_coord(data, ds, y, x)
    pressure = ds.createDimension('pressure',len(pres))
    
    #Agregar variables
    presiones = ds.createVariable('pressure','f4',('pressure',))
    presiones.setncatts({k: data.variables['pressure'].getncattr(k) for k in data.variables['pressure'].ncattrs()})
    LVM = ds.createVariable('LVM','u2',('y','x','pressure',), fill_value=data.variables['LVM']._FillValue)
    LVM.setncatts({k: data.variables['LVM'].getncattr(k) for k in data.variables['LVM'].ncattrs()})
    #LVM.units = 'percent'
    
    # Goes Imager projection 
    G_ = ds.createVariable('goes_imager_projection','i4')
    G_.setncatts({k: gip.getncattr(k) for k in gip.ncattrs()})
    
    #Asignar valores
    ys[:] = y
    xs[:] = x
    presiones[:] = pres
    LVM[:] = val
    #LVM._FillValue = 65535

########################################################################


def create_LST(data,y,x,val,gip,path,out_name):
    '''Create a dataset and write to a netCDF file.
    This function was designed for LSTF images'''
    ds = Dataset(path+out_name,'w',format='NETCDF4')
    
    #Crear dimensiones
    ys, xs= def_coord(data,ds, y, x)
    
    #Agregar variables
    LST = ds.createVariable('LST','i2',('y','x',),fill_value=data.variables['LST']._FillValue)
    LST.setncatts({k: data.variables['LST'].getncattr(k) for k in data.variables['LST'].ncattrs()})
    #LST.units = 'K'
    
    # Goes Imager projection 
    G_ = ds.createVariable('goes_imager_projection','i4')
    G_.setncatts({k: gip.getncattr(k) for k in gip.ncattrs()})
    
    #Asignar valores
    ys[:] = y
    xs[:] = x
    LST[:] = val
    #LVM._FillValue = 65535
    
    ds.close()

########################################################################

    
def create_Rad(data,y,x,val,gip,path,out_name):
    '''Create a dataset and write to a netCDF file.
    This function was designed for Rad C09 images'''
    ds = Dataset(path+out_name,'w',format='NETCDF4')
    
    #Crear dimensiones
    ys, xs = def_coord(data,ds,y,x)
    fill = data.variables['Rad']._FillValue
    #fill = 2047
    #units = data.variables['Rad'].units
    #units = 'mW m-2 sr-1 (cm-1)-1'
    
    #Agregar variables
    Rad = ds.createVariable('Rad','i2',('y','x',),fill_value=fill)
    #Rad.units = data.variables['Rad'].units
    #Rad._FillValue = data.variables['Rad']._FillValue
    Rad.setncatts({k: data.variables['Rad'].getncattr(k) for k in data.variables['Rad'].ncattrs()})
    
    # Goes Imager projection 
    G_ = ds.createVariable('goes_imager_projection','i4')
    G_.setncatts({k: gip.getncattr(k) for k in gip.ncattrs()})
    
    #Asignar valores
    ys[:] = y
    xs[:] = x
    Rad[:] = val
    
    ds.close()
    
########################################################################

    
def create_Cim(data,y,x,val,name,gip,path,out_name,z=None,z_name=None):
    '''Create a dataset and write to a netCDF file.
    This function was designed for Rad C09 images
    
    --------- input ------------
    data: Dataset of the original image
    y, x : coordinates to write
    val: Variable to write
    name: Variable name on data
    gip: goes image projection on data
    path: Path where image will be saved
    out_name: Output file name
    
    '''
    ds = Dataset(path+out_name,'w',format='NETCDF4')
    
    #Crear dimensiones
    ys, xs = def_coord(data,ds,y,x)
    if z!=None:
        ds.createDimension(z_name,len(z))
        zs = ds.createVariable(z_name,'f4',(z_name,))
    
    fill = data.variables[name]._FillValue
    units = data.variables[name].units
    
    #Agregar variables
    if z==None:
        var = ds.createVariable(name,'i2',('y','x',),fill_value=fill)
    else:
        var = ds.createVariable(name,'i2',('y','x',z_name),fill_value=fill)
    var.units = units
    
    # Goes Imager projection 
    G_ = ds.createVariable('goes_imager_projection','i4')
    G_.setncatts({k: gip.getncattr(k) for k in gip.ncattrs()})
    
    #Asignar valores
    ys[:] = y
    xs[:] = x
    var[:] = val
    if z!=None:
        zs[:] = z
    
    ds.close()
    

#############################################################################
    
def sat_info(dataset):
    '''Receives the dataset and returns some parameters of it (x, y, sat_h, sat_lon, sat_sweep, p).
    
    -------------
    INPUT
    
    image: path to a GOES image
    
    -------------
    OUTPUT
    
    x: horizontal coordinates on GOES system
    y: vertical coordinates on GOES system
    sat_h: Satellite's heigth
    sat_lon: Longitude of the satellite's orbit
    sat_sweep: Which direction do we sweep in?
    p: projection function. It converts coordinates to lat-lon system (uses pyproj module)
    '''
    #dataset = Dataset(image)
    x = dataset.variables['x'][:]
    y = dataset.variables['y'][:]
    # Height of the satellite's orbit
    sat_h = dataset.variables['goes_imager_projection'].perspective_point_height

    # Longitude of the satellite's orbit
    sat_lon = dataset.variables['goes_imager_projection'].longitude_of_projection_origin

    # Which direction do we sweep in?
    sat_sweep = dataset.variables['goes_imager_projection'].sweep_angle_axis
    p = Proj(proj='geos', h=sat_h, lon_0=sat_lon, sweep=sat_sweep)
    return(x,y,sat_h,sat_lon,sat_sweep,p)
    
########################################################################

def crop2D(dataset,var,ltmin = -15, ltmax = 15, lnmin = -90, lnmax = -60):
    '''Crop 2D Images
    var: variable name (Rad,LST)'''
    
    x,y,sat_h,sat_lon,sat_sweep,p = sat_info(dataset)

    value = dataset.variables[var][:]
    gip = dataset.variables['goes_imager_projection']

    # Define area boundaries
    xmin,ymin = p(lnmin,ltmin)/sat_h
    xmax,ymax = p(lnmax,ltmax)/sat_h
    sel_x = np.where((x>=xmin) & (x<=xmax))
    sel_y = np.where((y>=ymin) & (y<=ymax))

    # Select data with the above conditions
    x_col = x[sel_x]
    y_col = y[sel_y]
    value_col = value[sel_y[0].min():sel_y[0].max()+1,sel_x[0].min():sel_x[0].max()+1]
    lo_min, la_min = p(xmin*sat_h,ymin*sat_h,inverse=True)
    lo_max, la_max = p(xmax*sat_h,ymax*sat_h,inverse=True)
    
    return(y_col,x_col,value_col,gip)
    
##############################################################################

def crop3D(dataset,p_min):
    '''Crop LVMF images'''
    x,y,sat_h,sat_lon,sat_sweep,p = sat_info(dataset)
    
    LVM = dataset.variables['LVM'][:]
    hPa = dataset.variables['pressure'][:]
    gip = dataset.variables['goes_imager_projection']

    # Define area and pressure boundaries
    xmin,ymin = p(-90,-15)/sat_h
    xmax,ymax = p(-60,15)/sat_h
    sel_x = np.where((x>=xmin) & (x<=xmax))
    sel_y = np.where((y>=ymin) & (y<=ymax))
    sel_pres = np.where(hPa>=p_min)

    # Select data with the above conditions
    x_col = x[sel_x]
    y_col = y[sel_y]
    hPa = hPa[hPa>=100]
    LVM_col = LVM[sel_y[0].min():sel_y[0].max()+1,sel_x[0].min():sel_x[0].max()+1,sel_pres[0].min():sel_pres[0].max()+1]
    lo_min, la_min = p(xmin*sat_h,ymin*sat_h,inverse=True)
    lo_max, la_max = p(xmax*sat_h,ymax*sat_h,inverse=True)
    
    return(y_col,x_col,hPa,LVM_col,gip)
    
    
########################################################################

    
def gen_LVMPF(ini,fin,p_min,path,log_missing='No'):
    '''Download a GOES Legacy Vertical Moisture Profile Full-disk (LVMPF) image and create a new one for an specific region (Colombia and surroundings).
    Time interval must not exceed 10 minuts (Just one image must be downloaded)
    ---------------------
    INPUT
    ini: Str type variable. Initial time following the syntax 'AAAMMDD-hhmmss'
    fin: Str type variable. Final time following the syntax 'AAAMMDD-hhmmss'
    p_min: Float. Minimum pressure to be considered
    path: Str type variable. Directory path where final data will be saved. PLEASE END PATH WITH SLASH (/).
    log_missing: Str type variable. If No, log file for missng image will not be create. If you want to create the file, enter the path to the file where you will save this data,
    
    ---------------------
    OUTPUT
    
    flist: List of image(s) procceced (Just one image :D)
    
    Create an image in the 'path' directory.
    '''
    # Download image
    if not os.path.exists(path):
        os.makedirs(path)
    temp = 'LVMP_temp/'
    if not os.path.exists(temp):
        os.makedirs(temp)
    flist = GOES.download('goes16', 'ABI-L2-LVMPF',
             DateTimeIni = ini, DateTimeFin = fin, path_out=temp)
    
    # Open log file for missing images
    if log_missing!='No':
        lmiss = open(log_missing,'a')
    
    # List files in the temporary directory 
    fname = os.listdir(temp)
    
    if len(fname)==0: # In case that there is not image
        if log_missing!='No':
            print('Not LVMP image found at %s'%ini,file=lmiss)
        print('Not LVMP image found at %s'%ini)
        
    elif len(fname)==1: # Directory 'temp' should contain only one image
        # Create variables with the image data
        dataset = Dataset(temp+fname[0])

        y_col,x_col,hPa,LVM_col,gip = crop3D(dataset,p_min)
    
        # Create a file with the final data
        create_LVMP(dataset,y_col,x_col,hPa,LVM_col,gip,path,fname[0])
     
        # Erase original image
        os.remove(temp+fname[0])
     
        return flist[0]

########################################################################

    
def gen_LST(ini,fin,path,log_missing='No'):
    '''Download a GOES Land Surface Temperature Full-disk (LSTF) image and create a new one for an specific region (Colombia and surroundings).
    Time interval must not exceed 60 minuts (Just one image must be downloaded)
    ---------------------
    INPUT
    ini: Str type variable. Initial time following the syntax 'AAAMMDD-hhmmss'
    fin: Str type variable. Final time following the syntax 'AAAMMDD-hhmmss'
    path: Str type variable. Directory path where final data will be saved
    log_missing: Str type variable. If No, log file for missng image will not be create. If you want to create the file, enter the path to the file where you will save this data,
    
    ---------------------
    OUTPUT
    
    flist: List of image(s) procceced (Just one image :D)
    
    Create an image in the 'path' directory.
    '''
    # Temporary folder to download images
    if not os.path.exists(path):
        os.makedirs(path)
    temp = 'LST_temp/'
    if not os.path.exists(temp):
        os.makedirs(temp)
        
    # Download image
    flist = GOES.download('goes16', 'ABI-L2-LSTF',
             DateTimeIni = ini, DateTimeFin = fin, path_out=temp, 
                         show_download_progress=False)
    
    # Open log file for missing images
    if log_missing!='No':
        lmiss = open(log_missing,'w')
    
    # List files in the temporary directory 
    fname = np.sort(os.listdir(temp))
    
    if len(fname)==0: # In case that there is not image
        if log_missing!='No':
            print('Not image found at %s'%ini,file=lmiss)
        print('Not image found at %s'%ini)
        
    else:
        for j in range(len(fname)):
            # Create variables with the image data
            dataset = Dataset(temp+fname[j])
            
            y_col,x_col,LST_col,gip = crop2D(dataset,'LST')

            # Create a file with the final data
            create_LST(dataset,_col,x_col,LST_col,gip,path,fname[j])

            # Erase original image
            os.remove(temp+fname[j])
     
        return flist
    
########################################################################

def gen_Rad(ini,fin,path,ch,log_missing='No',ltmin = -15, ltmax = 15, lnmin = -90, lnmax = -60,temp = 'LST_temp/'):
    '''Download a GOES L1 Rad Image Full-disk (LSTF) image and create a new one for an specific region (Colombia and surroundings).
    Time interval must not exceed 10 minuts (Just one image must be downloaded)
    ---------------------
    INPUT
    ini: Str type variable. Initial time following the syntax 'AAAMMDD-hhmmss'
    fin: Str type variable. Final time following the syntax 'AAAMMDD-hhmmss'
    path: Str type variable. Directory path where final data will be saved
    ch: Channel [01,02,03, ..., 16]. Must be a string
    log_missing: Str type variable. If No, log file for missng image will not be create. If you want to create the file, enter the path to the file where you will save this data,
    
    ---------------------
    OUTPUT
    
    flist: List of image(s) procceced (Just one image :D)
    
    Create an image in the 'path' directory.
    '''
    
    # Create path if not exist
    if not os.path.exists(path):
        os.makedirs(path)
        
    # Temporary folder to download images
    #temp = 'LST_temp/'
    #print(temp)
    if not os.path.exists(temp):
        os.makedirs(temp)
        
    # Download image
    flist = GOES.download('goes16', 'ABI-L1b-RadF', channel=ch,
             DateTimeIni = ini, DateTimeFin = fin, path_out=temp, 
                         show_download_progress=False)
    
    # Open log file for missing images
    if log_missing!='No':
        lmiss = open(log_missing,'w')
    
    # List files in the temporary directory 
    fname = np.sort(os.listdir(temp))
    
    if len(fname)==0: # In case that there is not image
        if log_missing!='No':
            print('Not image found at %s'%ini,file=lmiss)
        print('Not image found at %s'%ini)
        
    else:
        for j in range(len(fname)):
            # Create variables with the image data
            dataset = Dataset(temp+fname[j])

            y_col,x_col,Rad_col,gip = crop2D(dataset,'Rad',ltmin = ltmin, ltmax = ltmax, lnmin = lnmin, lnmax = lnmax)
            
            # Create a file with the final data
            create_Rad(dataset,y_col,x_col,Rad_col,gip,path,fname[j])

            # Erase original image
            os.remove(temp+fname[j])
     
        return flist
    

