import os
import sqlite3
import smtplib
import pandas as pd
from email.mime.text import MIMEText
from datetime import datetime, timedelta
from email.message import EmailMessage
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication


# Send Downloaded data
def mail_downloaded_data (date_min, date_max, mail, aeropuertos = True):
    con = sqlite3.connect("aeropuertos.db")
    query = f"""SELECT * FROM datos_aeropuerto
            WHERE (Hora >= '{date_min}')
            AND (Hora <= '{date_max}')"""

    if "data_to_send" not in os.listdir():
        os.mkdir("data_to_send")
    name = mail.split("@")[0].replace(".","_") 
    df = pd.read_sql(query, con)
    destino_data = "data_to_send/" + name + "_data"+".csv"
    destino_ubicacion =  "data_to_send/" + name + "_ubicacion"+".csv"
    ubicacion = pd.read_sql("SELECT * FROM ubicacion", con)
    if type(aeropuertos) == list:
        df = df[df["Nombre"].isin(aeropuertos)]
        ubicacion = ubicacion[ubicacion["Nombre"].isin(aeropuertos)]
    df.to_csv(destino_data, index = False)
    ubicacion.to_csv(destino_ubicacion, index = False)
    
    
    msg = MIMEMultipart()
    html = f""" En el contenido de este correo usted encontrará la solicitud de descarga para los
                aeropuertos escogidos entre las fechas {str(date_min)} y {str(date_max)}"""

    part=MIMEText(html,"html")
    msg["Subject"] = f"Información solicitada de los Aeropuertos de Colombia"
    msg["To"] = mail
    msg["From"] = "julio.bohorquez@udea.edu.co"
    msg.attach(part)
    
    with open("data_to_send/" + name + "_data" + ".csv", "rb") as file:
        msg.attach(MIMEApplication(file.read(), name = name + "_data" + ".csv" ))
    with open("data_to_send/" + name + "_ubicacion" + ".csv", "rb") as file:
        msg.attach(MIMEApplication(file.read(), name = name + "_ubicacion" + ".csv" ))

    with smtplib.SMTP("smtp.gmail.com", 587) as smtp:
        smtp.ehlo()
        smtp.starttls()
        smtp.ehlo()
        smtp.login('julio.bohorquez@udea.edu.co',"blgtyjthlblzsufx")
        smtp.send_message(msg)
    os.remove("data_to_send/" + name + "_data" + ".csv")
    os.remove("data_to_send/" + name + "_ubicacion" + ".csv")

    con.commit()
    con.close()