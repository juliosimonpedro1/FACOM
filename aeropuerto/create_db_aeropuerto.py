import os
import numpy as np
import pandas as pd
import sqlite3 as sql

os.chdir(os.getcwd())

con = sql.connect('aeropuertos.db')
pd.read_csv('Aeropuertos.csv')[["Nombre","Longitud","Latitud","Departamento"]]\
        .to_sql('ubicacion', con = con, if_exists= 'replace', index = False)


datos_aeropuerto = """
CREATE TABLE datos_aeropuerto (
        Nombre varchar,
        Hora varchar,
        Temperatura float NULL,
        Punto_de_Rocio float NULL,
        Presion float NULL,
        Humedad_Relativa float NULL,
        PRIMARY KEY (Nombre, Hora)
)"""

cursor = con.cursor()
cursor.execute(datos_aeropuerto)
con.commit()
con.close()