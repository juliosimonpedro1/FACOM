import pandas as pd
from sqlalchemy import create_engine
from datetime import datetime, timedelta, date
import glob

hostname="amsc.c6l43uy718kg.us-east-1.rds.amazonaws.com"
username="maestro"
password="AMSC_123456*"
database="meteoro"

estaciones_codigo={"oriente":"1",
    "turbo":"2",
    "Sonson":"3",
    "yarumal":"4"}

engine=create_engine(
    f"postgresql://{username}:{password}@{hostname}/{database}"
)


def primer_populate(estacion):
    global estaciones_codigo
    ruta=f"/home/ubuntu/Data/DatosMeteo{estacion.capitalize()}/"
    files=glob.glob(ruta+"*/*.csv")

    
    df1=pd.DataFrame()
    for file in files:
        df=pd.read_csv(file)
        df=df[["Tiempo Sistema","Barometer","Outside Temperature","Wind Speed","Wind Direction",
            "Outside Humidity","Rain Rate","UV","Solar Radiation","Day Rain","Month Rain","Year Rain"]]
        df["codigo_estacion"]="amsc_"+estaciones_codigo[estacion]
        df1=pd.concat([df1,df])
    if estacion != "oriente":
        df1.to_sql(con=engine, name="DatosMeteoro", schema='public', if_exists="append", index=False)
    else:
        df1.to_sql(con=engine, name="DatosMeteoro", schema='public', if_exists="replace", index=False)

def populate_estaciones():
    global estaciones_codigo

    dic={"codigo_estacion":[],"nombre":[]}
    for estacion in estaciones_codigo.keys():
        dic["nombre"].append(estacion.capitalize())
        dic["codigo_estacion"].append("amsc_"+estaciones_codigo[estacion])
    pd.DataFrame(dic).to_sql(con=engine, name="Estaciones", schema="public",if_exists="replace", index=False)

for estacion in estaciones_codigo.keys():
    primer_populate(estacion)

primer_populate()