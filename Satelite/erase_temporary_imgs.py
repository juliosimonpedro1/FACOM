import numpy as np
import os

path = '/home/ubuntu/BanRep/Predic/Im_temp/Rad/'
canales = ['08','09','10']

suma = 0
for cc in canales:
    name = f'C{cc}'
    out_fold = f'{path}{name}/'
    ff = np.sort(os.listdir(out_fold))
    suma += len(ff)-5
    for i in range(len(ff)-5):
        os.remove(f'{out_fold}{ff[i]}')
        #print(f'{out_fold}{ff[i]}')
print(f'{suma} files erased')
