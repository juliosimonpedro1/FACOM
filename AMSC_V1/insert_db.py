import pandas as pd
import sqlite3 as sql
import glob
import numpy as np
import os
import shutil
import sqlalchemy
import pytz
from tqdm import tqdm
from funciones_server import send_mail
from datetime import datetime, date, timedelta
from sqlalchemy import create_engine


t1=datetime.now()

def insert_db(nombre_estacion_capital):
    print(nombre_estacion_capital)
    #Si no está la carpeta para recibir cambios, crearla
    if "DatosMeteo"+nombre_estacion_capital not in os.listdir("/home/ubuntu/Data/"):
        os.mkdir("/home/ubuntu/Data/DatosMeteo{}".format(nombre_estacion_capital))

    path="/home/ubuntu/Data/DatosMeteo{}/".format(nombre_estacion_capital)#.capitalize())

    pathdb="/home/ubuntu/Data/DatosMeteo{}/".format(nombre_estacion_capital)+\
      "meteoro-{}".format(nombre_estacion_capital.lower())+".db" #Prueba server


    if "meteoro-{}.db".format(nombre_estacion_capital.lower()) not in os.listdir(path):
        con=sql.connect(pathdb)
        columns=["Tiempo Sistema","Barometer","Outside Temperature","Wind Speed","Wind Direction",
            "Outside Humidity","Rain Rate","UV","Solar Radiation","Day Rain","Month Rain","Year Rain"]
        df=pd.DataFrame(columns=columns)
        dtypes={}
        for column in columns:
            dtypes[column]="TEXT" if column == "Tiempo Sistema" else "REAL"
        df.to_sql("DatosMeteoro", con, index=False, dtype=dtypes)
        con.commit()
        con.close()
        print("Se crea db")
        return None

    con=sql.connect(pathdb)
    cursor=con.cursor()
    datemax=cursor.execute('SELECT MAX("Tiempo Sistema") from DatosMeteoro').fetchall()[0][0]

    
    fecha_hoy=datetime.now()-timedelta(hours=5)
    if nombre_estacion_capital =="Oriente":
        fecha_hoy=datetime.now()
    if datemax is None:
        datemax=fecha_hoy
    else:
        datemax=datetime.strptime(datemax,
            "%Y-%m-%d %H:%M:%S.%f")
    df=pd.DataFrame()
    for i in reversed(range((fecha_hoy-datemax).days+2)):
        year=(fecha_hoy-timedelta(days=i)).strftime("%Y")
        month=(fecha_hoy-timedelta(days=i)).strftime("%m")
        date=(fecha_hoy-timedelta(days=i)).date()
        
        print(path+f"A{year}M{month}/DatosEstacion{date}.csv")

        try:
            df1=pd.read_csv(path+f"A{year}M{month}/DatosEstacion{date}.csv")
            df=pd.concat([df,df1])
        except:
          print("No File found")
          continue

    try:
        # Apply basic transformations
        if nombre_estacion_capital=="Oriente":
            df['Tiempo Sistema'] = pd.to_datetime(df['Tiempo Sistema']) - pd.to_timedelta(5,unit='h')
        else: 
            df['Tiempo Sistema'] = pd.to_datetime(df['Tiempo Sistema'])
        minutes=(df["Tiempo Sistema"].max()-(datemax)).total_seconds()

        # Insertar para mandar correo
        if (datetime.now()-timedelta(hours=5)-df["Tiempo Sistema"].max()).total_seconds()>= 60*30*2 and \
            (datetime.now()-timedelta(hours=5)-df["Tiempo Sistema"].max()).total_seconds()<= 60*30*4 and\
            (datetime.now().minute>=0 and datetime.now().minute<10):
            send_mail(nombre_estacion_capital,df["Tiempo Sistema"].max())
        if cursor.execute('SELECT MAX("Tiempo Sistema") from DatosMeteoro').fetchall()[0][0] is None:
            df=df
        else:
            df=df.iloc[-int((minutes/60+60)):].reset_index()
        df=df[["Tiempo Sistema","Barometer","Outside Temperature","Wind Speed","Wind Direction",
            "Outside Humidity","Rain Rate","UV","Solar Radiation","Day Rain","Month Rain","Year Rain"]]
        df['Rain Rate'] = df['Rain Rate']*0.2/60. #in units of cm/hour
        if nombre_estacion_capital in ["Caucasia", "SantaFe"]:
                df['Barometer'] = (df['Barometer']/1000.*(3386.389/100.0))
        else:
                df['Barometer'] = (df['Barometer']/1000.*(3386.389/100.0)) - (1011.0 - 788.0)
        df['Outside Temperature'] = ( df['Outside Temperature']/10. - 32.) * (5.0/9.0)
        df.loc[(df['Outside Temperature'] > 50) | (df['Outside Temperature'] < -15)] = np.nan
        df["Tiempo Sistema"]=df["Tiempo Sistema"].dt.strftime("%Y-%m-%d %H:%M:%S.%f")
        df.dropna(axis=0,subset=["Tiempo Sistema"],inplace=True)

    except:
        raise Exception("Los Cálculos necesarios no dan")
    df.to_sql("DatosMeteoro", con=con, if_exists="append", index=False)
    con.commit()
    con.close()

    shutil.copy(pathdb,f"/home/ubuntu/scripts/meteoro-{nombre_estacion_capital.lower()}.db")

for i in ["Turbo","Yarumal","Oriente",
            "Sonson", "Caucasia","SantaFe", "ITM"]:
    insert_db(i)
print("Se demora corriendo: ",datetime.now()-t1)
