CREATE TABLE Estaciones(
    Id_Estacion serial CONSTRAINT Id_Estacion_PK PRIMARY KEY,
    Nombre_Folder VARCHAR(20) NOT NULL,
    Nombre_Completo VARCHAR(50) NOT NULL,
    Latitud NUMERIC(20,17) NOT NULL,
    Longitud NUMERIC(20,18) NOT NULL,
    Color varchar(20) NOT NULL,
    Estado varchar(20) CONSTRAINT estado_cons CHECK (Estado IN ('Habilitada', 'Deshabilitada')), 
    CONSTRAINT Nombre_Completo UNIQUE (Nombre_Completo)
);

CREATE TABLE Observaciones(
    Fecha TIMESTAMP,
    Presion NUMERIC(9,5),
    Temperatura NUMERIC(7,5),
    Velocidad_Viento INT,
    Direccion_Viento INT,
    Humedad_Relativa INT,
    Radiacion_Solar INT,
    UV INT,
    Precipitacion NUMERIC(8,5),
    Precipitacion_Dia INT,
    Precipitacion_Mes INT,
    Precipitacion_Ano INT,
    Id_Estacion INT,
    CONSTRAINT Id_Estacion_FK
    FOREIGN KEY (Id_Estacion)
    REFERENCES Estaciones (Id_Estacion),
    PRIMARY KEY (Fecha, Id_Estacion)
);

CREATE TABLE Ciclo_Diurno(
    Mes INT,
    Hora TIME,
    Media_Temperatura NUMERIC(7,5),
    Media_Humedad_Relativa NUMERIC (8,5),
    Media_Radiacion_Solar NUMERIC (9,5),
    Media_Presion NUMERIC (9,5),
    Desviacion_Temperatura NUMERIC(7,5),
    Desviacion_Humedad_Relativa NUMERIC (8,5),
    Desviacion_Radiacion_Solar NUMERIC (9,5),
    Desviacion_Presion NUMERIC (9,5),
    Id_Estacion INT,
    CONSTRAINT Id_Estacion_FK
    FOREIGN KEY (Id_Estacion)
    REFERENCES Estaciones (Id_Estacion)
);

CREATE TABLE Eventos_Precipitacion(
    Numero_Evento_Estacion INT,
    Media_Precipitacion INT,
    Id_Estacion INT,
    CONSTRAINT Id_Estacion_FK
    FOREIGN KEY (Id_Estacion)
    REFERENCES Estaciones (Id_Estacion)
);

CREATE TABLE Compuesto(
    Minutos INT,
    Temperatura NUMERIC(7,5),
    Humedad_Relativa NUMERIC (8,5),
    Presion NUMERIC (9,5),
    Radiacion_solar NUMERIC (9,5),
    Precipitacion NUMERIC(8,5),
    Anomalia_Temperatura NUMERIC(7,5),
    Anomalia_Humedad_Relativa NUMERIC (8,5),
    Anomalia_Presion NUMERIC (9,5),
    Anomalia_Radiacion_Solar NUMERIC (9,5),
    Id_Estacion INT,
    CONSTRAINT Id_Estacion_FK
    FOREIGN KEY (Id_Estacion)
    REFERENCES Estaciones (Id_Estacion)
);

CREATE TABLE Responsable(
    Id_Responsable serial CONSTRAINT Id_Responsable_PK PRIMARY KEY,
    Nombre VARCHAR(70),
    Apellido VARCHAR(70),
    correo VARCHAR(70)
);

CREATE TABLE Responsable_Estacion(
    id_responsable INT,
    id_estacion INT, 
    CONSTRAINT Id_Estacion_FK
    FOREIGN KEY (Id_Estacion)
    REFERENCES Estaciones (Id_Estacion),
    CONSTRAINT Id_Responsable_FK
    FOREIGN KEY (Id_Responsable)
    REFERENCES Responsable (Id_Responsable)
);

CREATE TABLE ultimo_dato(
    id_estacion INT,
    fecha TIMESTAMP,
    Temperatura NUMERIC(7,5),
    Humedad_Relativa NUMERIC (8,5),
    Presion NUMERIC (9,5),
    Precipitacion NUMERIC(8,5),
    CONSTRAINT Id_Estacion_FK
    FOREIGN KEY (id_estacion)
    REFERENCES Estaciones (Id_Estacion),
    CONSTRAINT unique_id_estacion UNIQUE (id_estacion)
);