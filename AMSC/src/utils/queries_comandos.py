from .conexiones import conexion

class query():
    def __init__(self):
        self.con_obj = conexion("RDS")
        self.conexion = self.con_obj.conectar()
        
    def query_estaciones_habilitadas(self):
        con = self.conexion
        cursor = con.cursor()
        cursor.execute("""
                    SELECT 
                    id_estacion,
                    nombre_folder
                    FROM estaciones
                    WHERE estado = 'Habilitada'""")
        resultados = cursor.fetchall()
        con.commit()
        con.close()
        return resultados
    def return_path(self):
        return conexion("host").conectar()
    
    def query_observaciones_max(self, estacion_index):
        con = self.conexion
        cursor = con.cursor()
        cursor.execute(f"""
                    SELECT
                    max(fecha)
                    FROM observaciones
                    WHERE id_estacion = {estacion_index}""")
        resultados = cursor.fetchall()[0]
        con.commit()
        con.close()
        return resultados
    
    def query_estaciones_web(self, id_estacion = None):
        con = self.conexion
        cursor = con.cursor()
        sql = f"""  SELECT 
                        ultimo_dato.*,
                        nombre_completo,
                        latitud,
                        longitud,
                        color,
                        estado
                    FROM ultimo_dato
                    LEFT JOIN estaciones
                    ON ultimo_dato.id_estacion = estaciones.id_estacion 
                    """
        sql = f"{sql} WHERE ultimo_dato.id_estacion = {id_estacion}" if type(id_estacion) == int else sql
        cursor.execute(sql)
        resultados = cursor.fetchall()
        con.commit()
        con.close()
        return resultados
    
    def query_multivar_rango(self, 
            tupla_id_estacion, 
            lista_variables,
            fecha_inicial,
            fecha_final):
        variables_iniciales = ["observaciones.fecha",
                                "estaciones.nombre_completo",
                                "estaciones.estado"]
        variables_iniciales.extend(lista_variables)
        tupla_str = str(tupla_id_estacion) if len(tupla_id_estacion) > 1 else str(tupla_id_estacion).replace(",","")
        sql_select = "SELECT "
        for variable in variables_iniciales:
            sql_select = sql_select + variable 
            sql_select = sql_select + ", " if variable != variables_iniciales[-1] else sql_select + ' '
        sql = sql_select + """FROM observaciones
                                LEFT JOIN estaciones
                                ON observaciones.id_estacion = estaciones.id_estacion """
        sql = sql + f"WHERE observaciones.fecha >= '{str(fecha_inicial)} 00:00' AND observaciones.fecha <= '{str(fecha_final)} 00:00' "
        sql = sql + "AND observaciones.id_estacion IN "+tupla_str

        con = self.conexion
        cursor = con.cursor()
        cursor.execute(sql)
        resultados = cursor.fetchall()
        con.commit()
        con.close()

        return {"Resultados" : resultados, "Columnas": variables_iniciales}

class comando():
    def __init__(self):
        self.con_obj = conexion("RDS")
        self.conexion = self.con_obj.conectar()

    def insertar_batch(self, dataframe, tabla):
        engine = self.con_obj.create_engine_sql()
        dataframe.to_sql(tabla, con = engine, index = False, if_exists="append")
    
    def insertar_max(self):
        con = self.conexion
        cursor = con.cursor()
        with open("sql/commands/insert_max.sql", "r") as fp:
            sql = fp.read()
        cursor.execute(sql.replace("\n"," ").replace("\t", " "))
        con.commit()
        con.close()