import os
import json
import folium
import pandas as pd
from PIL import Image
import streamlit as st
from pyproj import Proj
from datetime import timedelta, datetime
from streamlit_folium import folium_static

st.set_page_config(layout="wide")
st.title("Radares de Colombia :satellite_antenna:")

st.sidebar.write("""<p style='text-align:center; font-size:9px;'>
              <i><b> ADVERTENCIA:</b> ESTE SITIO ES PRODUCIDO CON FINES NETAMENTE ACADÉMICOS. LA INFORMACIÓN AQUÍ PUBLICADA 
              NO SIGUE LOS MISMOS ESTÁNDARES DE AGENCIAS OFICIALES COMO EL <a href='http://www.siata.gov.co'>SIATA</a> O 
              <a href='http://www.ideam.gov.co'>IDEAM</a>. 
              POR TANTO NO NOS HACEMOS RESPONSABLES DEL USO QUE USTED LE DÉ A LA INFORMACIÓN DESPLEGADA EN ESTA PÁGINA. 
              PARA INFORMACIÓN OFICIAL POR FAVOR VISITAR LAS PÁGINAS COMPARTIDAS </i></p>""",unsafe_allow_html=True)

with open("datos_radares.json", "r") as file:
    radares=json.load(file)


st.sidebar.write("---")
st.sidebar.write("""
                <p style='text-align:center; font-size: 18px;'>
                Seleccione el radar que desea consultar 
                </p>""", unsafe_allow_html=True)


m=folium.Map(location=[4,-72], zoom_start=6)
black_list = []

def plot_radar(radar, All = True):
    global black_list

    lat=radares[radar]["Coordenadas"]["lat"]
    lon=radares[radar]["Coordenadas"]["lon"]
    folder=radares[radar]["Nombre s3"]

    p=Proj(proj='utm', zone=10, ellps="WGS84", preserve_units=False)
    xx, yy=p(lon,lat)

    df_radar = pd.read_json(f"lista_radares/{folder}/{folder}.json").transpose().sort_values(by=["Fecha", "Elevacion"])
    df_radar["elevacion_float"] = df_radar.Elevacion.apply(lambda x: round(x,1))
    df_radar = df_radar.drop_duplicates(subset = "elevacion_float", keep = 'first')

    c1, c2 = st.sidebar.columns((1.5,1))
    elevacion_escogida = c1.selectbox("Escoja la elevacion que desea", df_radar["elevacion_float"].sort_values()) if not All else df_radar["elevacion_float"].min()

    nombre_img = df_radar[df_radar["elevacion_float"] == elevacion_escogida].sort_values(by = "Fecha").iloc[[0]].index[0]
    radio = df_radar.loc[nombre_img]["Radio"]
    fecha = df_radar.loc[nombre_img]["Fecha"]

    valor=radio
    lon_max, lat_max=p(xx+valor, yy+valor, inverse=True)
    lon_min, lat_min=p(xx-valor, yy-valor, inverse=True)
    

    if radar not in black_list:
        img=os.path.abspath(f"lista_radares/{folder}/plot_{nombre_img}.png")


        if ((datetime.now()-timedelta(hours=5)) - datetime.strptime(fecha, "%Y-%m-%d %H:%M:%S")).total_seconds() \
                > 2*60*60:
            color = "red"
            if not All:
                st.sidebar.write(f"""<p style="color:red;"> <i><b> ALERTA </i></b></p>
                                <p> Esta estación se encuentra con una imagen de barrido de hace más de 2 horas
                                verificar las páginas del IDEAM para ver el estado del radar</p>""", unsafe_allow_html=True)
                folium.Circle([radares[radar]["Coordenadas"]["lat"],
                radares[radar]["Coordenadas"]["lon"]], 
                radius=valor, 
                color="grey",
                dash_array = '7',
                opacity = 2,
                weight = 2 ).add_to(m)

                folium.raster_layers.ImageOverlay(img, 
                                        bounds=[[lat_min,lon_min],[lat_max,lon_max]], 
                                        opacity=0.6).add_to(m)
        
        else:
            color = "green"
            folium.Circle([radares[radar]["Coordenadas"]["lat"],
                radares[radar]["Coordenadas"]["lon"]], 
                radius=valor, 
                color="grey",
                dash_array = '7',
                opacity = 2,
                weight = 2 ).add_to(m)
            folium.raster_layers.ImageOverlay(img, 
                                        bounds=[[lat_min,lon_min],[lat_max,lon_max]], 
                                        opacity=0.6).add_to(m)
        if not All:
            st.sidebar.write(f"<b> El radio es de: </b>{round(radio/1000,0)} km", unsafe_allow_html=True)
            st.sidebar.write(f"<b> La fecha del barrido es :</b> {fecha}", unsafe_allow_html=True)
          
        icon=folium.Icon(icon="glyphicon-cloud",color=color, prefix = 'glyphicon')
        html=f"""
            <p style='font-size:14px;'><i><b>{radar}</p></i></b>
            <li><b>Latitud:</b> {str(round(lat,2))}</li>
            <li><b>Longitud:</b> {str(round(lon,2))}</li></ul>
        """
        popup=folium.Popup(html, unsafe_allow_html=True,max_width=200)
        folium.Marker(location=[lat,lon], icon=icon, popup=popup).add_to(m)


    else: 
        st.sidebar.write("Lo Sentimos, este radar no se encuentra disponible en el momento")
    return [[lat_min, lon_min],[lat_max,lon_max]]

if st.sidebar.checkbox("Marque para desplegar"):
    radar=st.sidebar.selectbox(label="",options=radares.keys(), label_visibility="collapsed")
    coordenadas = plot_radar(radar, All = False)
    m.fit_bounds(coordenadas)
else:
    for radar in radares.keys():
        if radar not in black_list:
            plot_radar(radar)
folium_static(m,width=800,height=800)