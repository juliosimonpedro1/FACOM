import pandas as pd
import numpy as np
import datetime as dt
from datetime import date, timedelta
import streamlit as st
import matplotlib.pyplot as plt
import plotly.graph_objs as go
import plotly.express as px
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics
import sqlite3 as sql
from sqlalchemy import create_engine
from fbprophet import Prophet
from datetime import datetime,date
import plotly
from funciones_server import  ciclo_anom, tabla_anom, mail_downloaded_data
from streamlit_folium import folium_static
import folium


st.set_page_config(layout="wide")

# Load de data
def get_df_table(table_or_sql,eng):
       engine = create_engine(eng)
       conn = engine.connect()
       generator_object = pd.read_sql(table_or_sql,con=conn)
       generator_object["Tiempo Sistema"]=pd.to_datetime(generator_object["Tiempo Sistema"],
              format="%Y-%m-%d %H:%M:%S")
       generator_object.drop_duplicates(subset="Tiempo Sistema",inplace=True) 
       generator_object.sort_values(by="Tiempo Sistema",ascending=True,inplace=True)
       generator_object.set_index("Tiempo Sistema", inplace=True)
       return generator_object

# AGG funcion para tener la última fecha de la cual se tienen datos
def query_max(query,con):
       c=sql.connect(con)
       cur=c.cursor()
       x=cur.execute(query).fetchall()[0][0]
       c.commit()
       c.close()
       return x 

def plot_cd_plotly(df,variable,izq,title):
       df["Hora-Min"]=df.index.strftime("%H:%M")
       df=df.drop_duplicates(subset="Hora-Min")
       fig=go.Figure()
       colors={"Outside Temperature":"firebrick","Outside Humidity":"forestgreen", "Barometer":"indigo","Solar Radiation":"goldenrod",
       "Lim sup Outside Temperature":"rgba(178,34,34,0)", "Lim sup Outside Humidity":"rgba(34,139,34,0.5)","Lim sup Barometer":"rgba(75,0,130,0.5)",
       "Lim inf Outside Temperature":"rgba(178,34,34,0.5)", "Lim inf Outside Humidity":"rgba(34,139,34,0.5)","Lim inf Barometer":"rgba(75,0,130,0.5)",
       "Lim inf Solar Radiation":"rgba(218,165,32,0.5)","Lim sup Solar Radiation":"rgba(218,165,32,0.5)"}

       labels={"Outside Temperature": "°C", "Outside Humidity":"(%)", "Barometer":"hPA", "Rain Rate":"mm/h","Solar Radiation":"kW/h"}
       
       sup=fig.add_trace(go.Scatter(x=df["Hora-Min"],y=df["Lim sup "+variable], mode="lines",line=dict(color=colors["Lim sup "+variable], width=0), showlegend=False))
       inf=fig.add_trace(go.Scatter(x=df["Hora-Min"],y=df["Lim inf "+variable], mode="lines",line=dict(color=colors["Lim inf "+variable], width=0),fill="tonexty", showlegend=False))
       var=fig.add_trace(go.Scatter(x=df["Hora-Min"],y=df["CD "+variable], mode="lines", name=variable, line=dict(color=colors[variable],width=2), showlegend=False))    
       
       fig.update_layout(width=700, height=500, margin=dict(autoexpand=True))
       # fig.update_layout( paper_bgcolor='rgba(0,0,0,0)',plot_bgcolor='rgba(0,0,0,0)')
       fig.update_xaxes(showgrid=True,gridwidth=0.02)
       fig.update_yaxes(showgrid=True, gridwidth=0.02)
       fig.update_layout(xaxis=dict(title="Hora : Minutos", titlefont=dict(size=14)))
       fig.update_layout(title=dict(text=title,x=0.11,y=0.85,font=dict(size=15,family="Brush Script MT")))
       if izq is True:
              fig.update_layout(yaxis=dict(title="<i>"+variable+ " "+labels[variable]+"</i>", titlefont=dict(color=colors[variable],size=14)))
       return inf
       

st.sidebar.write("""<p style='text-align:center; font-size:9px;'>
              <i><b> ADVERTENCIA:</b> ESTE SITIO ES PRODUCIDO CON FINES NETAMENTE ACADÉMICOS. LA INFORMACIÓN AQUÍ PUBLICADA NO SIGUE LOS MISMOS ESTÁNDARES DE AGENCIAS OFICIALES COMO EL <a href='http://www.siata.gov.co'>SIATA</a> O <a href='http://www.ideam.gov.co'>IDEAM</a>. 
              POR TANTO NO NOS HACEMOS RESPONSABLES DEL USO QUE USTED LE DÉ A LA INFORMACIÓN DESPLEGADA EN ESTA PÁGINA. 
              PARA INFORMACIÓN OFICIAL POR FAVOR VISITAR LAS PÁGINAS COMPARTIDAS </i></p>""",unsafe_allow_html=True)

#st.sidebar.markdown("---")

# st.sidebar.markdown(""" <p style='font-size:11; text-align:justify'><i> A continuación se mostrarán todas las opciones disponibles para la visualización de
#                      los datos, para seleccionar y/o deseleccionar una opción clickee en la caja y ésta desplegará la información o por el
#                      contrario la ocultará. </p></i>
#                      """, unsafe_allow_html=True)

st.sidebar.markdown("---")

nombredb={"Oriente":"meteoro-oriente.db",
              "Turbo":"meteoro-turbo.db",
              "Sonson":"meteoro-sonson.db",
              "Yarumal":"meteoro-yarumal.db",
              "Santa Fé":"meteoro-santafe.db",
              "Caucasia":"meteoro-caucasia.db",
              "Itm": "meteoro-itm.db"} #Arreglar

for i in nombredb.keys():
       ult_act=query_max("""SELECT MAX("Tiempo Sistema") from DatosMeteoro""",
       nombredb[i]).split(".")[0]
       st.sidebar.markdown(f"**{i}:**  _{ult_act}_")
st.sidebar.markdown("---")


st.sidebar.markdown('### Rango de Fechas')


tmp1 = st.sidebar.date_input('Fecha inicial',date.today() - timedelta(5))
tmp2 = st.sidebar.date_input('Fecha final',date.today() + timedelta(1))

st.sidebar.markdown("---")

st.sidebar.markdown("_A continuación se mostrarán todas las opciones de gráficos._")
st.sidebar.markdown("")

my_query='''
SELECT * FROM DatosMeteoro
WHERE 
([Tiempo Sistema] >= '{}' ) 
AND 
([Tiempo Sistema] <= '{}' ) 
'''.format(str(tmp1),str(tmp2))

lista=["oriente","turbo","sonson","yarumal","santafe","caucasia", "itm"]
listadb=list(map(lambda x: "meteoro-"+x+".db",lista))

dbs={}
for i in range(len(listadb)):
       eng='sqlite:///'+listadb[i]
       if listadb[i]=="meteoro-santafe.db":
              dbs["Santa Fé"]=get_df_table(my_query,eng)
       else:
              dbs[str.capitalize(listadb[i].split("-")[1].split(".")[0])]=get_df_table(my_query,eng)
       globals()[f"df{lista[i]}"]=get_df_table(my_query,eng)


       
coordenadas={"Oriente":[6.106154134522639, -75.38783679205099],
              "Turbo":[8.094314856114538, -76.7338430092313],
              "Sonson":[5.719219419105346, -75.29528859371956],
              "Yarumal":[6.962234887882262, -75.41762081687851],
              "Santa Fé":[6.555843977773638, -75.82621410794272],
              "Caucasia":[7.992032085475798, -75.20143077333672],
              "Itm":[6.274057264413763, -75.5887129303584]}


variables={"Tiempo Sistema":"Fecha",
              "Outside Temperature":"Temperatura",
              "Outside Humidity":"Humedad Relativa",
              "Barometer":"Presión",
              "Rain Rate":"Precipitación"}

st.title('Antioquia Mira su Cielo :sun_small_cloud:')

st.write('#### _Última actualización_')

m=folium.Map(location=[7.15,-75.492212], zoom_start=7.5)
ant=f"Antioquia.geo.json"
style_function = lambda x: {'fillColor': '#a14216', "fillOpacity":0.1,
                            "color":"#000", "weight":2}


folium.GeoJson(data=ant,zoom_on_click=True,style_function=style_function).add_to(m)


for estacion in nombredb.keys():
       var_est={}
       for variable in variables.keys():
              query=f""" SELECT "{variable}" from DatosMeteoro where "Tiempo Sistema"=(SELECT 
              MAX("Tiempo Sistema") from DatosMeteoro where "Outside Temperature" is not null) """
              var_1=query_max(query,nombredb[estacion])
              try:
                     var_1=round(var_1,2)
              except:
                     if var_1 is None:
                            var_1="No se encuentra dato"
                     else:
                            var_1=datetime.strptime(var_1,"%Y-%m-%d %H:%M:%S.%f")
              var_est[variable]=var_1
       html=f"""
              <p style='font-size:14px;'><i><b>{estacion}</p></i></b>
              <ul style='list-style-type:square;'>
              <li><b>{variables["Tiempo Sistema"]}:</b> {var_est["Tiempo Sistema"]}</li>
              <li><b>{variables["Outside Temperature"]}:</b> {var_est["Outside Temperature"]} °C</li>
              <li><b>{variables["Outside Humidity"]}:</b> {var_est["Outside Humidity"]} %</li>
              <li><b>{variables["Barometer"]}:</b> {var_est["Barometer"]} hPa</li>
              <li><b>{variables["Rain Rate"]}:</b> {var_est["Rain Rate"]} mm/h</li></ul>
              """
       popup=folium.Popup(html=html,unsafe_allow_html=True,max_width=200)
       icon=folium.Icon(color="orange", icon="cloud", prefix="fa")
       folium.Marker(location=[coordenadas[estacion][0],coordenadas[estacion][1]],
              icon=icon,popup=popup).add_to(m)

no_listas={"Arboletes": [8.84938354904801, -76.42691784717519]}

for est in no_listas.keys():
       html=f"""<p style='font-size:14px;'><i><b>{est}</p></i></b>
       <p> Esta Estación no se encuentra en funcionamiento en este momento </p>"""

       popup=folium.Popup(html=html,unsafe_allow_html=True,max_width=200)
       icon=folium.Icon(color="purple",icon="cloud",prefix="fa")
       folium.Marker(location=[no_listas[est][0],no_listas[est][1]],icon=icon,popup=popup).add_to(m)



folium_static(m,width=750,height=700)

st.write('### _Gráficos_')
md=""" <p style='font-size:16px;'><i> A continuacón se mostrará un panel de 3 gráficas para cada variable y se explicará para cada tipo de gráfica la información que se obitene, la cuarta gráfica
       que indica la de pronóstico sólo se mostrará si se selecciona la caja abajo de este texto. Las gráficas son: </i></p>
       <ul style='list-style-type:square;'>
       <li><b> Valores rango vs Valores Medios: </b> En esta gráfica se podrá visualizar la comparación entre los valores del rango escogido y el ciclo diurno de la variable mensual, es decir, los valores medios de
       cada momento del dia (ciclo diurno) discriminado por cada mes del año</li>
       <li><b> Valores de Anomalía para el rango: </b> En esta gráfica se podrá visualizar las anomalías del rango de datos escogido, es decir, el valor medio de la viariable para cada momento del día por mes (ciclo diurno)
       restado a el valor actual de la variable</li>
       <li><b> Ciclo diurno del rango: </b> En esta gráfica se podrá visualizar el ciclo diurno del rango escogido, es decir, el comportamiento diurno de la variable obtenido de los valores medios de cada momento del día para el rango
       escogido</li>
       <li><b> Pronóstico de la variable: </b> En esta gráfia se podrá visualizar los datos de la variable en el rango escogido y su pronóstico para un día adelante</li>
       </ul>
       <p style='font-size:18px;'><i><b> NOTA: </b> La variable precipitación no cuenta con los gráficos mencionados anteriormente, por el contrario cuenta con una gráfica en la que 
       se ilustra los valores de precipitación del rango escogido y el valor acumulado del rango escogido</i></p>
       """
md=""" <p style='font-size:16px;text-align:justify'><i> En esta página se podrá visualizar diferentes tipos de gráficos como: los valores de las estaciones del rango escogido, las anomalías del rango escogido, el ciclo diurno del rango, el pronóstico 
       de algunas variables, y el análisis compuesto por estación, así como gráficos multivariables.
       Los gráficos contendrán información sobre el tipo de gráfico y además podrán ser escogidos por el usuario a manera de cajas de selección en la barra lateral izquierda para desplegarse
       en la página.</p>
       <p style='font-size:16px;color:#ff0000;text-align:justify;'><b> Notas: </b><ul>
       <li> Para todos los gráficos, exceptuando el análisis compuesto, el rango de fechas escogido será el mismo y se encuentra en la barra lateral izquierda </li>
       <li>La variable escogida será la misma para Valores del rango escogido, anomalías del rango, ciclo diurno del rango y Pronóstco</li>
       <li>La estación escogida será la misma para los gráficos multivariable y el análisis compuesto</li>
       </ul></p></i>
       """

st.write(md,unsafe_allow_html=True)



check_val=st.sidebar.checkbox("Valores del Rango escogido", value=True)
check_anom=st.sidebar.checkbox("Anomalías del rango escogido")
check_cd=st.sidebar.checkbox("Ciclo diurno del rango escogido")
check_multi=st.sidebar.checkbox("Gráfico multivariable")
check_composite=st.sidebar.checkbox("Análisis compuesto")
# check_pronostico=st.sidebar.checkbox("Pronóstico")
check_pronostico=False

st.sidebar.markdown("""  
                      
                      """)
## Temporal variation of variables

st.sidebar.markdown("---")
vars_pred = ['Temperatura','Presión','Humedad Relativa','Radiación Solar',"Precipitación"]
varpred = st.sidebar.selectbox('Escoja la variable que desea graficar',vars_pred,index=0)
estacion=st.sidebar.selectbox("Escoja la estación sobre la cual desea observar el gráfico multivariable y el análisi compuesto",
        nombredb.keys())
st.sidebar.markdown("---")

dic={"Temperatura":"Outside Temperature","Presión":"Barometer","Humedad Relativa":"Outside Humidity",
       "Radiación Solar":"Solar Radiation","Precipitación":"Rain Rate"}
var1 = dic[varpred]
emoji={"Outside Temperature":" :thermometer:","Rain Rate":" :umbrella_with_rain_drops:", "Outside Humidity":" :sun_behind_rain_cloud:", "Barometer":" :cyclone:",
"Solar Radiation":":sunny:","Rain Rate":":rain_cloud:"}

#FOR ML
dfori = dbs["Oriente"]
dftur = dbs["Turbo"]
dfson=dbs["Sonson"]
dfyar=dbs["Yarumal"]

if check_pronostico:
       varpred1=dic[varpred]
       if varpred1=="Rain Rate":
              pass
       else:
              df_ph = dfori[['Tiempo Sistema',varpred1]]
              df_ph = df_ph.rename(columns={'Tiempo Sistema': 'ds',varpred1: 'y'})
              df_ph['ds'] = pd.to_datetime(df_ph['ds'])
              m = Prophet(changepoint_prior_scale=0.01).fit(df_ph)
              future = m.make_future_dataframe(periods=1500, freq='min')
              fcst_oriente = m.predict(future)

       #######################################
       # ########### TURBO ###################
       #######################################
              df_ph = dftur[['Tiempo Sistema',varpred1]]
              df_ph = df_ph.rename(columns={'Tiempo Sistema': 'ds',varpred1: 'y'})
              df_ph['ds'] = pd.to_datetime(df_ph['ds'])
              m = Prophet(changepoint_prior_scale=0.01).fit(df_ph)
              future = m.make_future_dataframe(periods=1500, freq='min')
              fcst_turbo = m.predict(future)

       #######################################
       # ########### sonson ###################
       #######################################
              df_ph = dfson[['Tiempo Sistema',varpred1]]
              df_ph = df_ph.rename(columns={'Tiempo Sistema': 'ds',varpred1: 'y'})
              df_ph['ds'] = pd.to_datetime(df_ph['ds'])
              m = Prophet(changepoint_prior_scale=0.01).fit(df_ph)
              future = m.make_future_dataframe(periods=1500, freq='min')
              fcst_sonson = m.predict(future)

       #######################################
       # ########### yarumal ###################
       #######################################

              df_ph = dfyar[['Tiempo Sistema',varpred1]]
              df_ph = df_ph.rename(columns={'Tiempo Sistema': 'ds',varpred1: 'y'})
              df_ph['ds'] = pd.to_datetime(df_ph['ds'])
              m = Prophet(changepoint_prior_scale=0.01).fit(df_ph)
              future = m.make_future_dataframe(periods=1500, freq='min')
              fcst_yarumal = m.predict(future)


       #time_temp_plt = go.Scattergl(x=df2['Tiempo Sistema'], y=df2[var1], mode = 'markers', name='Observado')
       #ph_temp_plt   = go.Scattergl(x=fcst['ds'][len(df2):-1], y=fcst['yhat'][len(df2):-1], mode = 'markers', name='Predicción')
       #df_plt = [time_temp_plt,ph_temp_plt]
       #fig = go.Figure(data=df_plt, layout=layout)
       #st.write(fig) 

              ph_plt_obs_oriente = go.Scattergl(x=fcst_oriente['ds'][0:len(dfori)], y=fcst_oriente['yhat'][0:len(dfori)], mode = 'markers', name='Observado Oriente')
              ph_plt_pre_oriente = go.Scattergl(x=fcst_oriente['ds'][len(dfori):-1], y=fcst_oriente['yhat'][len(dfori):-1], mode = 'markers', name='Predicción Oriente')

              ph_plt_obs_oriente_err= go.Scattergl( x=pd.concat( [ fcst_oriente['ds'][len(dfori):-1], fcst_oriente['ds'][:len(dfori):-1] ] ),
                                   y=pd.concat( [ fcst_oriente['yhat_upper'][len(dfori):-1], fcst_oriente['yhat_lower'][:len(dfori):-1] ]),
                                   fill='toself',
                                   mode='lines',
       #                               hoveron='points',
                                   name='Error'
              )   

              ph_plt_obs_turbo = go.Scattergl(x=fcst_turbo['ds'][0:len(dftur)], y=fcst_turbo['yhat'][0:len(dftur)], mode = 'markers', name='Observado Turbo')
              ph_plt_pre_turbo = go.Scattergl(x=fcst_turbo['ds'][len(dftur):-1], y=fcst_turbo['yhat'][len(dftur):-1], mode = 'markers', name='Predicción Turbo')

              ph_plt_obs_turbo_err= go.Scattergl( x=pd.concat( [ fcst_turbo['ds'][len(dftur):-1], fcst_turbo['ds'][:len(dftur):-1] ] ),
                                   y=pd.concat( [ fcst_turbo['yhat_upper'][len(dftur):-1], fcst_turbo['yhat_lower'][:len(dftur):-1] ]),
                                   fill='toself',
                                   mode='lines',
       #                               hoveron='points',
                                   name='Error'
              )   

              ph_plt_obs_sonson = go.Scattergl(x=fcst_sonson['ds'][0:len(dfson)], y=fcst_sonson['yhat'][0:len(dfson)], mode = 'markers', name='Observado Sonson')
              ph_plt_pre_sonson = go.Scattergl(x=fcst_sonson['ds'][len(dfson):-1], y=fcst_sonson['yhat'][len(dfson):-1], mode = 'markers', name='Predicción Sonson')

              ph_plt_obs_sonson_err= go.Scattergl( x=pd.concat( [ fcst_sonson['ds'][len(dfson):-1], fcst_sonson['ds'][:len(dfson):-1] ] ),
                                   y=pd.concat( [ fcst_sonson['yhat_upper'][len(dfson):-1], fcst_sonson['yhat_lower'][:len(dfson):-1] ]),
                                   fill='toself',
                                   mode='lines',
       #                               hoveron='points',
                                   name='Error'
              )   

              ph_plt_obs_yarumal = go.Scattergl(x=fcst_yarumal['ds'][0:len(dfyar)], y=fcst_yarumal['yhat'][0:len(dfyar)], mode = 'markers', name='Observado yarumal')
              ph_plt_pre_yarumal = go.Scattergl(x=fcst_yarumal['ds'][len(dfyar):-1], y=fcst_yarumal['yhat'][len(dfyar):-1], mode = 'markers', name='Predicción yarumal')

              ph_plt_obs_yarumal_err= go.Scattergl( x=pd.concat( [ fcst_yarumal['ds'][len(dfyar):-1], fcst_yarumal['ds'][:len(dfyar):-1] ] ),
                                   y=pd.concat( [ fcst_yarumal['yhat_upper'][len(dfyar):-1], fcst_yarumal['yhat_lower'][:len(dfyar):-1] ]),
                                   fill='toself',
                                   mode='lines',
       #                               hoveron='points',
                                   name='Error'
              )   

              # ORIENTE 

              df_plt = [ph_plt_obs_oriente,ph_plt_pre_oriente,ph_plt_obs_oriente_err] 
              layout = go.Layout(xaxis_title="Date", yaxis_title=var1)
              fig = go.Figure(data=df_plt, layout=layout)

              #cf1.write(fig)
              fig_pron_ori=fig
              #TURBO 

              df_plt = [ph_plt_obs_turbo,ph_plt_pre_turbo,ph_plt_obs_turbo_err] 
              layout = go.Layout(xaxis_title="Date", yaxis_title=var1)
              fig = go.Figure(data=df_plt, layout=layout)
              fig_pron_turbo=fig

              #Sonson 
              df_plt=[ph_plt_obs_sonson, ph_plt_pre_sonson, ph_plt_obs_sonson_err]
              layout = go.Layout(xaxis_title="Date", yaxis_title=var1)
              fig = go.Figure(data=df_plt, layout=layout)
              fig_pron_sonson=fig

              #Yarumal
              df_plt=[ph_plt_obs_yarumal, ph_plt_pre_yarumal, ph_plt_obs_yarumal_err]
              layout = go.Layout(xaxis_title="Date", yaxis_title=var1)
              fig = go.Figure(data=df_plt, layout=layout)
              fig_pron_yarumal=fig

#Diccionarios que permiten tener control sobre los labels, los colores y el ancho de linea

labels={"Outside Temperature": "°C", "Outside Humidity":"(%)", "Barometer":"hPA", "Rain Rate":"mm/h","Solar Radiation":"kW/h",
       "Anomaly Outside Humidity": "(%)", "Anomaly Outside Temperature":"°C", "Anomaly Barometer":"hPA","Anomaly Solar Radiation":"kW/h",
       "Anomaly Rain Rate":"mm/h"}
colors={"Rain Rate":"teal","Outside Temperature":"firebrick","Outside Humidity":"forestgreen", "Barometer":"indigo","Solar Radiation":"goldenrod",
       "Anomaly Outside Humidity":"forestgreen","Anomaly Solar Radiation":"goldenrod" ,"Anomaly Outside Temperature":"firebrick", "Anomaly Barometer":"indigo",
       "Anomaly Rain Rate":"teal"}
linewidth={"Rain Rate":1.5, "Outside Temperature":1.5, "Outside Humidity":1.5, "Barometer":1.5, "Solar Radiation":1.5}


#Se grafica el valor comparado con el ciclo diurno mensual multianual
def plot_val_v_med(df,variable,izq, title):

       fig=go.Figure()
       global labels, colors
       valr=fig.add_trace(go.Scatter(x=df.index,y=df[variable], mode="lines",name=variable ,line=dict(color=colors[variable]),showlegend=not izq))
       valm=fig.add_trace(go.Scatter(x=df.index,y=df["mean "+variable],name="Mean "+variable ,mode="lines", line=dict(color="black", dash="dashdot"),showlegend=not izq))

       if izq is True:
              fig.update_layout(yaxis=dict(title="<i>"+variable+ " "+labels[variable]+"</i>", titlefont=dict(color=colors[variable],size=14)))
       fig.update_layout(xaxis=dict(title="Fecha", titlefont=dict(size=14)))
       fig.update_layout(width=700, height=500, margin=dict(autoexpand=True))
       fig.update_layout(title=dict(text=title,x=0.11,y=0.85,font=dict(size=15,family="Brush Script MT")))

       return fig

#Se grafica las anomalías que son el valor actual, menos el valor del ciclo diurno mensual multianual
def anomalias_plot(df,variable,izq,title):
       df["cero"]=0
       fig=go.Figure()
       global labels,colors
       fig.add_trace(go.Scatter(x=df.index,y=df["Anomaly "+variable], mode="lines", name="Anomaly "+variable,line=dict(color=colors[variable]), showlegend= False))
       fig.add_trace(go.Scatter(x=df.index, y=df["cero"], mode="lines", showlegend=False,line=dict(color=colors[variable],dash="dashdot",width=0.7)))

       if izq is True:
              fig.update_layout(yaxis=dict(title="<i>"+variable+ " "+labels[variable]+"</i>", titlefont=dict(color=colors[variable],size=14)))
       fig.update_layout(xaxis=dict(title="Fecha",titlefont=dict(size=14)))
       fig.update_layout(width=700, height=500, margin=dict(autoexpand=True))
       fig.update_layout(title=dict(text=title,x=0.11,y=0.85,font=dict(size=15,family="Brush Script MT")))

       return fig

# Graficar el acumulado de precipitación
def acumulado_ppt(df,izq,title):


       fig=go.Figure()
       global labels, colors
       fig.add_trace(go.Scatter(x=df.index,y=df["Rain Rate"], mode="lines", name="Precipitación", showlegend=not izq, line=dict(color=colors[variable])))
       fig.add_trace(go.Scatter(x=df.index,y=df["ppt_acumulado"],mode="lines",name="Acumulado",showlegend=not izq,line=dict(color="black",dash='dashdot'),yaxis="y2"))

       fig.update_layout(xaxis=dict(title="Fecha",titlefont=dict(size=14)))
       fig.update_layout(width=700, height=500, margin=dict(autoexpand=True))

       fig.update_layout(yaxis=dict(title="Precipitación mm/h", titlefont=dict(color="teal",size=14)))
       fig.update_layout(yaxis2=dict(title="Precipitación Acumulada mm/h", titlefont=dict(color="black",size=14),side="right",overlaying="y"))
       fig.update_layout(title=dict(text=title,x=0.11,y=0.85,font=dict(size=15,family="Brush Script MT")))

       return fig

md="""

"""
st.write(md)

dbanom={}

if check_val or check_anom:
       st.markdown("---")
       st.markdown("### _Valores y Anomalías del rango escogido_")
       md="""<p style='font-size:16px;text-align:justify'><ul style='list-style-type:square;'>
       <li><b> Valores rango vs Valores Medios: </b> En esta gráfica se podrá visualizar la comparación entre los valores del rango escogido y el ciclo diurno de la variable mensual, es decir, los valores medios de
       cada momento del dia (ciclo diurno) discriminado por cada mes del año</li>
       <li><b> Valores de Anomalía para el rango: </b> En esta gráfica se podrá visualizar las anomalías del rango de datos escogido, es decir, el valor medio de la viariable para cada momento del día por mes (ciclo diurno)
       restado a el valor actual de la variable</li></ul></p>
       <p style='font-size:16px; color:#ff0000;'><b><i>Nota: </p></b><p style='font-size:16px;'> La precipitación no cuenta con ciclo diurno en las gráficas,
       pero si cuenta con un acumulado de precipitación en el rango. Tampoco cuenta con anomalías."""
       st.markdown(md,unsafe_allow_html=True)
       st.markdown("")
       c1,c2=st.columns((1,2))
       estaciones_list=c1.multiselect("Escoja la estaciones que quiere graficar",dbs.keys())
for i in dbs.keys():
       dbanom[i]=tabla_anom(dbs[i],nombredb[i],5)
   
def plot_estaciones(variable,estaciones,anom):
       fig=go.Figure()
       global labels,dic,dbanom

       variable_1=dic[variable]

       est_color={"Oriente":"forestgreen","Turbo":"firebrick","Sonson":"Peru",
               "Yarumal":"navy", "Santa Fé":"olive", "Caucasia":"slategray", "Itm":"pink"}

       fig.update_layout(width=900, height=400, margin=dict(autoexpand=True))
       yi=0
       yd=1
       fig.update_layout(xaxis=dict(title="Fecha",domain=[yi,yd]))
       fig.update_layout(yaxis=dict(title=variable+" "+labels[variable_1]))
       fig.update_layout(margin=dict(l=0,r=0,b=0,t=20))


       for estacion in estaciones:
              estaciondf=dbanom[estacion]
       
              if variable_1=="Rain Rate":
                     fig.add_trace(go.Scatter(x=estaciondf.index,y=estaciondf[variable_1],
                                   name=estacion,mode="lines",line=dict(color=est_color[estacion]),showlegend=True))
                     fig.add_trace(go.Scatter(x=estaciondf.index,y=estaciondf["ppt_acumulado"],
                                   name="Acumulado "+estacion,mode="lines",line=dict(color=est_color[estacion],
                                   dash="dashdot",width=0.7),showlegend=False,yaxis="y2"))
                     fig.update_layout(yaxis2=dict(overlaying="y", anchor="x",side="right"))
                     title="Gráfico de Precipitación y Acumulado"

                     continue

              if anom:
                     fig.add_trace(go.Scatter(x=estaciondf.index,y=estaciondf["Anomaly "+variable_1],
                                   name=estacion, mode="lines",line=dict(color=est_color[estacion]),showlegend=True))
                     fig.add_trace(go.Scatter(x=estaciondf.index,y=[0]*len(estaciondf.index), mode="lines", line=dict(dash="dashdot",color="black",width=0.7),
                                   showlegend=False))
                     title="Gráfico de Anomalías"

              else:
                     fig.add_trace(go.Scatter(x=estaciondf.index,y=estaciondf[variable_1],name=estacion,
                            mode="lines", line=dict(color=est_color[estacion]),showlegend=True))
                     fig.add_trace(go.Scatter(x=estaciondf.index,y=estaciondf["mean "+variable_1], name="Promedio "+estacion,
                            mode="lines", line=dict(color=est_color[estacion],dash="dashdot", width=0.7),showlegend=False))
                     title="Gráfico de Valores del Rango y Ciclo Diurno"
       fig.update_layout(title=dict(text=title,x=0.5,y=1))
       return fig

#Graficar plot unitario para todas las estaciones. 



if check_val:


       if len(estaciones_list)==0:
              estaciones_list=["Oriente","Turbo"]

       st.write(plot_estaciones(varpred,estaciones_list,False))
       st.write("""
              """)

if check_anom:

       if len(estaciones_list)==0:
              estaciones_list=["Oriente","Turbo"]


       if var1=="Rain Rate":
              st.write("##### Lo Sentimos, para Precipitación no se cuenta con Anomalías ni ciclo diurno del rango :confused:")
              pass
       else:
              st.write(plot_estaciones(varpred,estaciones_list,True))

       #Graficar solo ciclo diurno del rango.


#Crear Diccionario de df's de rango
if check_cd:
       st.markdown("---")
       st.markdown("### _Ciclo diurno del rango_")

       dbrango={}
       for i in dbs.keys():
              if len(dbs[i])==0:
                     continue
              dbrango[i]=ciclo_anom(dbs[i].resample("10min").mean())

       md3="""<p style='text-align:justify;font-size:16px'><i>En esta gráfica se podrá visualizar el ciclo diurno del rango escogido, es decir, el comportamiento diurno de la variable obtenido de los valores medios de cada momento del día para el rango
       escogido, y la linea sombreada es el error asociado a éste. Las gráficas se desplegarán para cada estación disponible</i></p>"""
       st.markdown(md3,unsafe_allow_html=True)

       for i in dbs.keys():
              if i not in dbrango.keys():
                     st.markdown("")
                     st.markdown(f"<p style='text-align:justify;font-size:16px;color:red'><b> Lo sentimos, pero no se cuenta con esta gráfica para la estación {i} en el periodo de tiempo escogido </b></p>", 
                            unsafe_allow_html=True)

       if var1=="Rain Rate":
              st.write("### Lo Sentimos, en este momento no se cuenta con el ciclo diurno del rango para la precipitación :umbrella_with_rain_drops:")
       else:
              cf1,cf2=st.columns((2,2))
              cf3,cf4=st.columns((2,2))
              cf5,cf6=st.columns((2,2))

              for columna, db in zip(range(1,    len(dbrango.keys())+1),dbrango.keys()):
                     booleano=False
                     if columna%2!=0:
                            booleano=True
                     globals()[f"cf{columna}"].write(plot_cd_plotly(dbrango[db],var1,booleano,db))

def plot_multi(estacion,variables,anom):
       global colors,labels,linewidth, dic, dbanom
       df=dbanom[estacion]

       variables_1=[0]*len(variables)
       for i in range(len(variables)):
              variables_1[i]=dic[variables[i]]
       title="Valores en el Rango para "+estacion
       if anom is True:
              for i in range(len(variables)):
                     if variables_1[i]=="Rain Rate":
                            variables_1[i]="Rain Rate"
                     else:
                            variables_1[i]="Anomaly "+variables_1[i]
              title="Anomalías en el Rango para "+estacion
       
       fig=go.Figure()
       yi=0.075
       yd=0.925
       fig.update_layout(xaxis=dict(title="Fecha",domain=[yi,yd]))
       for i in range(len(variables)):
              if i%2==0:
                     side="left"
                     y=yi
              else:
                     side="right"
                     y=yd
              s=i+1

              fig.add_trace(go.Scatter(x=df.index,y=df[variables_1[i]],name=variables[i],line=dict(color=colors[variables_1[i]],width=1.2),
              yaxis="y"+str(s), showlegend=True))

              if anom:
                     fig.add_trace(go.Scatter(x=df.index,y=[0]*len(df.index),line=dict(color=colors[variables_1[i]],dash="dashdot",width=0.6),
              yaxis="y"+str(s), showlegend=False))
              
              # fig.update_layout(exec(f"""yaxis{s}=dict(title=variables[i],
              # titlefont=dict( color=colors[variables[i]]),anchor='free',overlaying='y',
              # side=side,position={y})"""))

       try:
              fig.update_layout(yaxis=dict(title=variables[0]+" "+labels[variables_1[0]],
              titlefont=dict(color=colors[variables_1[0]])
              ))
       except: 
              pass

       try:
              fig.update_layout(yaxis2=dict(title=variables[1]+" "+labels[variables_1[1]],
              titlefont=dict(color=colors[variables_1[1]]),side="right",anchor="x",overlaying="y"))
       except:
              pass

       try:
              fig.update_layout(yaxis3=dict(title=variables[2]+" "+labels[variables_1[2]],
              titlefont=dict(color=colors[variables_1[2]]),side="left",anchor="free",position=0,overlaying="y"))
       except:
              pass
       try:
              fig.update_layout(yaxis4=dict(title=variables[3]+ " "+labels[variables_1[3]],
              titlefont=dict(color=colors[variables_1[3]]), side="right",anchor="free",position=1, overlaying="y"))
       except:
              pass

       fig.update_layout(width=1050, height=400, margin=dict(autoexpand=True))
       fig.update_layout(margin=dict(l=0,r=0,b=0,t=20))
       fig.update_layout(title=dict(text=title, x=0.5,y=1))

       return fig

if check_multi:
       st.markdown("---")
       st.markdown("### _Gráficos Multivariable_")
       md="""<p style='font-size:16px;'><i>Esta sección mostrará un panel de dos gráficas por estación, donde se podrá analizar el comportamiento en el rango escogido de las variables escogidas de la siguiente manera: </i></p>
              <ul style='list-style-type:circle;'>
              <li><b>Variables en el rango: </b>En esta gráfica podrá visualizar el comportamiento de los valores medidos para las variables que escoja, para así tener un espectro amplio de cómo se comporta el sistema en ese momento</li>
              <li><b>Anomalías en el rango: </b>En esta gráfica podrá visualizar las anomalías de las variables en el rango escogido, donde una anomalía en este caso es el valor medido menos el ciclo diurno mensual.</li>
              </ul>"""
       st.write(md,unsafe_allow_html=True)
       c1,c2=st.columns((1,2))
       variables=c1.multiselect("Seleccione las variables que desea graficar: ",["Precipitación", "Temperatura", "Humedad Relativa", "Presión","Radiación Solar"])

       if len(variables)==0:
              variables=["Precipitación","Temperatura"]
       st.write(plot_multi(estacion,variables,False))
       st.write(plot_multi(estacion,variables,True))


nombres={"Outside Temperature":"Temperatura","Barometer":"Presión","Outside Humidity":"Humedad Relativa",
         "Solar Radiation":"Radiación Solar","Rain Rate":"Precipitación", "Anomaly Barometer":"Presión",
         "Anomaly Outside Humidity":"Humedad Relativa", "Anomaly Outside Temperature":"Temperatura"}

def plot_composite(estacion,anom):
    global colors, labels, nombres,nombredb
    df=nombredb[estacion]
    path=df
    con=sql.connect(path)
    query="SELECT * FROM Composite"
    df=pd.read_sql(query,con)
    df.set_index("index",inplace=True)


    fig=go.Figure()

    dominio=[0.075,0.925]
    fig.update_layout(xaxis=dict(domain=dominio))

    positions={1:dominio[0],2:dominio[1],3:0,4:1}

    c=1
    vars_plot=["Rain Rate", "Barometer", "Outside Humidity", "Outside Temperature"]

    title="Análisis compuesto para "+estacion
    if anom:
        vars_plot=["Rain Rate", "Anomaly Barometer", 
                "Anomaly Outside Humidity", "Anomaly Outside Temperature"]
        title="Anomalía de análisis compuesto para "+estacion
    for i in vars_plot:

        if anom:
            fig.add_trace(go.Scatter(x=df.index,y=[0]*len(df.index),mode="lines",
                    line=dict(color=colors[i], dash="dashdot",width=0.7),
                        yaxis="y"+str(c),showlegend=False))

        fig.add_trace(go.Scatter(x=df.index,y=df[i],mode="lines", name=nombres[i],
                line=dict(color=colors[i]),yaxis="y"+str(c)))
        side="left"
        if c%2 == 0:
            side="right"
        yaxis=dict(title=nombres[i]+" "+labels[i],
            side=side,titlefont=dict(color=colors[i]))
        if c>1:
            yaxis["overlaying"]="y"
            yaxis["anchor"]="x"
            if c>2:
                yaxis["anchor"]="free"
                yaxis["position"]=positions[c]
        yax=f"yaxis{str(c)}"

        fig.update_layout({yax:yaxis})
        c=c+1
    fig.update_layout(width=1050, height=400, margin=dict(autoexpand=True))
    fig.update_layout(margin=dict(l=0,r=0,b=0,t=20))
    fig.update_layout(title=dict(text=title, x=0.5,y=1))
    return fig

if check_composite:
       st.markdown("---")
       st.markdown("### _Análisis Compuesto_")
       md="""<p style='text-align:justify;font-size:16px'><i> Un análisis compuesto consta de aislar todos los eventos de precipitación, y 
              realizar así el promedio de todas las variables 6 horas atrás y 3 horas luego del inicio del evento de precipitación. Los eventos de precipitación
              se separaban entre sí si pasaban más de 15 minutos sin llover; con el fin de observar la relación de las otras variables antes y después de un evento. 
              Se cuenta con dos gráficas, la primera que data de los valores únicamente, y la segunda representa anomalías."""
       st.markdown(md,unsafe_allow_html=True)
       st.markdown("")
       st.write(plot_composite(estacion,False))
       st.write(plot_composite(estacion,True))

if check_pronostico:
       st.write("---")
       st.write("### _Pronóstico de la variable_")
       
       md="""<p style='text-align:justify;font-size:16px'><i>En esta gráfica se podrá visualizar el pronóstico de la variable para los siguientes días,
              obtenido mediante aproximaciones numéricas. Se podrá visualizar para cada estación al mismo tiempo en gráficas separadas.</i></p>"""
       st.markdown(md,unsafe_allow_html=True)
       if varpred1=="Rain Rate":
              st.write("### En el momento no se encuentra disponible el pronóstico para la variable precipitación :rain_cloud:")
       else:
              cf1,cf2=st.columns((2,2))
              cf3, cf4=st.columns((2,2))
              cf1.write(fig_pron_ori)
              cf2.write(fig_pron_turbo)
              cf3.write(fig_pron_sonson)
              cf4.write(fig_pron_yarumal)

## Descarga 

st.sidebar.write("Haga click en la caja si quiere la información para descarga")

fecha_inicial = st.sidebar.date_input("Fecha incial")
hora_inicial = st.sidebar.time_input("Hora inicial")
fecha_final = st.sidebar.date_input("Fecha final")
hora_final = st.sidebar.time_input("Hora final")

datetime_inicial = str(fecha_inicial) + " " + str(hora_inicial)
datetime_final = str(fecha_final) + " " + str(hora_final)
estacion=st.sidebar.selectbox("Escoja la estación", nombredb.keys())
mail = st.sidebar.text_input("Escriba su correo")


if st.sidebar.button("Oprima para mandar correo con la información suministrada"):
       mail_downloaded_data(datetime_inicial, datetime_final, nombredb[estacion],
                     mail, estacion)