WITH max_date AS (
	SELECT
	id_estacion,
	max(fecha) fecha 
	FROM observaciones
	GROUP BY id_estacion),
max_values AS (
	SELECT 
	max_date.id_estacion,
	max_date.fecha,
	temperatura,
	humedad_relativa,
	presion,
	precipitacion 
	FROM observaciones
	INNER JOIN max_date
	ON max_date.id_estacion = observaciones.id_estacion
	AND max_date.fecha = observaciones.fecha)
SELECT 
estaciones.id_estacion,
max_values.fecha,
max_values.temperatura,
max_values.humedad_relativa,
max_values.presion,
max_values.precipitacion,
nombre_completo,
latitud,
longitud,
color,
estado
FROM max_values
RIGHT JOIN estaciones
ON estaciones.id_estacion = max_values.id_estacion;
