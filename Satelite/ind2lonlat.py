import os
import numpy as np
import pandas as pd
from pyproj import Proj

red = pd.read_csv('coord.csv')

fls = np.sort(os.listdir('Rain_maps'))

sat_h = 35786023.0
sat_lon = -75.0
sat_sweep ='x'

p = Proj(proj='geos', h=sat_h, lon_0=sat_lon, sweep=sat_sweep)

for i,ff in enumerate(fls):
	rr = pd.read_csv(f'Rain_maps/{ff}')
	wr = open(f'Rain_maps/rcoord_{ff[5:]}','w')
	print('lon,lat',file=wr)
	#print(rr.keys())
	for j in range(len(rr[' x'])):
		xi, yi = rr[' x'][j], rr['y'][j]
		lon, lat = p(red['x_red'][xi]*sat_h, red['x_red'][yi]*sat_h, inverse=True)
		print(f'{lon},{lat}',file=wr)
		
