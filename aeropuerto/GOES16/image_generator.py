import os
import wget
import imageio
from urllib.error import HTTPError
from datetime import datetime, timedelta
from PIL import Image , ImageDraw, ImageFont

directory = os.getcwd()
os.chdir(directory)
datetime_utc=datetime.now()
hora={'0':'00','1':'10','2':'20','3':'30','4':'40','5':'50'}


while True:
    try:
        date=''
        for format in ['%Y','%j','%H','%M']:
            date = date+(hora[datetime_utc.strftime(format)[0]] if format == '%M' else datetime_utc.strftime(format))
        url='https://cdn.star.nesdis.noaa.gov/GOES16/ABI/FD/GEOCOLOR/{}_GOES16-ABI-FD-GEOCOLOR-5424x5424.jpg'.format(date)
        if '{}_GOES16-ABI-FD-GEOCOLOR-5424x5424.jpg'.format(date) in os.listdir('images'):
            os.remove('images/{}_GOES16-ABI-FD-GEOCOLOR-5424x5424.jpg'.format(date))
        wget.download(url, out = 'images')
        break
    except HTTPError:
        datetime_utc = datetime_utc - timedelta(minutes = 10)

# Cortar la imagen para el tamaño de colombia
map = Image.open(f'images/{date}_GOES16-ABI-FD-GEOCOLOR-5424x5424.jpg')
map = map.crop((2350,1950,3250,3040))


#Crear Rectángulo con la información 
rectangle = Image.new('RGB',(900, 100), (255, 255, 255))
draw = ImageDraw.Draw(rectangle)
fuente = ImageFont.truetype('Fuentes/NunitoSans-Regular.ttf', 25)
fuente_italic = ImageFont.truetype('Fuentes/NunitoSans-Italic.ttf', 25)
draw.text((5,5), 'GOES16 Geocolor     |     Color real de dia - Multiespectral IR de noche', 
            fill=(0,0,0), font = fuente) 
draw.text((5,55), f'Fecha (UTC) {datetime_utc.strftime("%Y-%m-%d %H:")}'+
            hora[datetime_utc.strftime("%M")[0]], fill=(0,0,0), font = fuente_italic)
draw.text((900-5-draw.textlength(f'Hora Local: {(datetime_utc-timedelta(hours=5)).strftime("%H:%M")}',
            font = fuente_italic), 55),f'Hora Local: {(datetime_utc-timedelta(hours=5)).strftime("%H:")}' +
            hora[(datetime_utc-timedelta(hours=5)).strftime("%M")[0]],
            fill = (0,0,0), font = fuente_italic)


# Juntar las imágenes
concat_img=Image.new('RGB', (900, 1190), (255,255,255))
concat_img.paste(rectangle, (0,0))
concat_img.paste(map, (0,100))
concat_img.save(f'images/{date}_GOES16-ABI-FD-GEOCOLOR-5424x5424.jpg')

#Generar Gif
images = []
imagenes = os.listdir('images')
imagenes.sort()
os.remove('geocolor.gif') if "geocolor.gif" in os.listdir() else 0
for image in imagenes:
    images.append(imageio.imread('images/'+image))
imageio.mimsave('geocolor.gif', images, 'GIF', duration = 0.17)
print(len(os.listdir()))

#Borrar primer file para que en la carpeta siempre hayan 30 registros
if len(os.listdir('images')) > 30:
    dir = os.listdir('images')
    dir.sort(reverse= False)
    for image in dir[30:]:
        os.remove("images/"+image)

