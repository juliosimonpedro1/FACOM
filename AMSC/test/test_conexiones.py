import pytest
from src.utils.conexiones import conexion

@pytest.mark.parametrize(
        "connection_type",
        [
            "PostgreSQL",
            "rdS",
            "RdS"
        ]
)
def test_conexion_postgresql_error(connection_type):
    with pytest.raises(TypeError) as excinfo:
        conexion(connection_type)
    assert str(excinfo.value) == "Escoga una conexión válida"