from time import time
import matplotlib.pyplot as plt
import sqlite3 as sql
import pandas as pd
import numpy as np
import os 
from datetime import datetime, timedelta
from funciones_server import tabla_anom, cd_anom
from tqdm import tqdm


t1=datetime.now()
path="/home/ubuntu/Data"

os.chdir(path)

class estacion:
    def __init__(self, nombre):
        self.nombre=str.capitalize(nombre)
        if nombre =="santafe":
            self.nombre="SantaFe"
        elif nombre == 'itm':
            self.nombre = 'ITM'
        self.nombredb=str.lower(nombre)
        self.con=sql.connect(f"DatosMeteo{self.nombre}/meteoro-{self.nombredb}.db")
        self.cursor=self.con.cursor()
        self.colors={"Outside Temperature":"firebrick", "Outside Humidity":"forestgreen",
        "Rain Rate":"teal", "Barometer":"darkviolet"}
        self.labels={"Outside Temperature":"Temperatura [°C]",
                "Outside Humidity":"Humedad Relativa [%]",
                "Barometer":"Presión [hPA]",
                "Rain Rate":"Precipitación [mm/h]"}
        if os.path.exists("DatosMeteo{}/{}".format(self.nombre,self.nombre)):
            pass
        else:
            os.mkdir("DatosMeteo{}/{}".format(self.nombre,self.nombre))
    def __str__(self):
        return self.nombre
    
    def lista_eventos(self,resolucion):
        df=pd.read_sql("SELECT * FROM DatosMeteoro",self.con)
        df.drop_duplicates(subset="Tiempo Sistema", inplace=True)
        df.dropna(subset=["Tiempo Sistema"], inplace=True)
        df["Tiempo Sistema"]=pd.to_datetime(df["Tiempo Sistema"],format="%Y-%m-%d %H:%M:%S")
        rain=df[df["Rain Rate"]>0][["Tiempo Sistema","Rain Rate"]]
        rain.reset_index(inplace=True)
        rain.drop(labels="index", axis=1, inplace=True)
        rain.sort_values(by="Tiempo Sistema", inplace=True, ascending=True)
        rain["Evento"]=1
        loct=rain.columns.get_loc("Tiempo Sistema")
        evento=1
        c=0
        for index in range(len(rain.index)):
            rain.iloc[c,2]=evento
            if index==rain.shape[0]-1:
                break
            if (rain.iloc[c+1,loct] - rain.iloc[c,loct]).total_seconds() >60*resolucion:
                evento+=1 
            c+=1
        rain.drop_duplicates(subset="Evento", keep="first",inplace=True)
        rain.to_sql("Eventos_ppt",con=self.con,if_exists="replace",index=False)
        self.eventos=rain.drop_duplicates(subset="Evento").shape[0]
    
    def composite_individual(self,fecha_inicial):
        query=f"""SELECT "Tiempo Sistema" from DatosMeteoro where "Tiempo Sistema" 
                LIKE "{fecha_inicial}%" """
        inicio=self.cursor.execute(query).fetchall()[0][0]
        if type(inicio)==list:
            return print("Esa fecha tiene dos iguales")
        
        inicio=datetime.strptime(inicio,"%Y-%m-%d %H:%M:%S.%f")
        maximo=(inicio+timedelta(hours=3)).strftime("%Y-%m-%d %H:%M:%S")
        minimo=(inicio-timedelta(hours=6)).strftime("%Y-%m-%d %H:%M:%S")

        query=f"""SELECT * FROM DatosMeteoro where "Tiempo Sistema" >=
         "{minimo}" and "Tiempo Sistema" <= "{maximo}" """

        df=pd.read_sql(query,self.con)
        director=f"DatosMeteo{self.nombre}/{self.nombre}"
        path=os.path.join(director,f"{fecha_inicial}")
        if os.path.exists(path):
            pass
        else:
            os.mkdir(path)
        df.to_csv(f"DatosMeteo{self.nombre}/{self.nombre}/{fecha_inicial}/{fecha_inicial}.csv")

        df["Tiempo Sistema"]=pd.to_datetime(df["Tiempo Sistema"], format="%Y-%m-%d %H:%M:%S")
        df.set_index("Tiempo Sistema", inplace=True)

        figure=plt.figure(figsize=(20,10))

        tiempo=df.index

        ax1=figure.gca()
        ax2=ax1.twinx()
        ax3=ax1.twinx()
        ax4=ax3.twinx()

        for i,j in zip([ax1,ax2,ax3,ax4],["Outside Temperature","Rain Rate","Barometer","Outside Humidity"]):
            i.plot(tiempo,df[j], color=self.colors[j])
            i.set_ylabel(self.labels[j],color=self.colors[j])
            i.grid(alpha=0.2)
        
        ax4.spines["right"].set_position(("outward",60))
        ax3.spines["left"].set_position(("outward",60))

        plt.title(f"Comportamiento de variables para evento {fecha_inicial}")

        plt.xlim((tiempo.min(),tiempo.max()))

        plt.show()
        plt.savefig(path+f"/{fecha_inicial}.jpg")
    
    def composite_mean(self,resample):
        variables=["Tiempo Sistema","Outside Temperature","Outside Humidity",
            "Barometer","Rain Rate","Solar Radiation"]
        eventos=pd.read_sql("SELECT * from Eventos_ppt", self.con)
        eventos.drop(labels="Rain Rate", inplace=True, axis=1)
        eventos_arr=eventos["Evento"].tolist()
        #Create string to query variables
        string=""
        c=1
        for i in variables:
            string+='"'+i+'"'
            if c<len(variables):
                string+=','
            c+=1
        
        data=pd.read_sql(f"SELECT {string} from DatosMeteoro", self.con)

        for i in [data,eventos]:
            i["Tiempo Sistema"]=pd.to_datetime(i["Tiempo Sistema"],format="%Y-%m-%d %H:%M:%S")
            i.drop_duplicates(subset="Tiempo Sistema",inplace=True)
            i.dropna(axis=0,subset=["Tiempo Sistema"],inplace=True)
            i.set_index("Tiempo Sistema", inplace=True)
            i.sort_values(by="Tiempo Sistema", inplace=True)
        eventos=eventos.resample(f"{str(resample)}min").mean()
        # data=tabla_anom(data,f"meteoro-{self.nombredb}.db",resample)
        data=tabla_anom(data,f"DatosMeteo{self.nombre}/meteoro-{self.nombredb}.db",resample) #AWS path
        df=pd.merge(data,eventos,how="left", left_index=True,right_index=True)
        dateindex=pd.date_range(start=df.index.min()-timedelta(hours=6),end=df.index.max()+timedelta(hours=3),freq=f"{str(resample)}min").to_frame()
        df=pd.merge(dateindex,df, how="left",left_index=True,right_index=True)
        vector_res=np.arange(-360,181,resample)
        variables.remove("Tiempo Sistema")
        comp_vars=variables.copy()
        for i in variables:
            if i=="Rain Rate":
                continue
            comp_vars.append("Anomaly "+i)

        dic_vars={}

        for i in comp_vars:
            dic_vars[i]=[[]]

        composite=pd.DataFrame(data=dic_vars,index=vector_res)
        print(len(eventos_arr))

        for evento in tqdm(eventos_arr):
            start_event=df.index[df["Evento"]==evento].values[0]
            for variable in comp_vars:
                for minuto in range(len(vector_res)):
                    if minuto==0:
                        value=df.loc[start_event+np.timedelta64(int(vector_res[minuto]),"m"),variable]
                    else:
                        value=(df.loc[start_event+np.timedelta64(int(vector_res[minuto]),"m"),variable] +\
                                df.loc[start_event+np.timedelta64(int(vector_res[minuto-1]),"m"),variable])/2
                    l=composite.loc[vector_res[minuto],variable].copy()
                    l.append(value)
                    composite.loc[vector_res[minuto],variable]=l
        composite_nan=composite.applymap(lambda z: np.count_nonzero(np.isnan(z)))            
        composite=composite.applymap(lambda x: np.nanmean(x))
        composite_nan.to_sql("Composite_nan", self.con, if_exists="replace")
        composite.to_sql("Composite",self.con,if_exists="replace")
        self.con.commit()
        self.con.close()
        return composite
    
    def plot_composite(self):
        df=pd.read_sql("SELECT * from Composite",self.con, index_col="index")
        
        figure=plt.figure(figsize=(20,10))
        ax1=figure.gca()
        ax2=ax1.twinx()
        ax3=ax1.twinx()
        ax4=ax3.twinx()

        for i,j in zip([ax1,ax2,ax3,ax4],["Outside Temperature","Rain Rate","Barometer","Outside Humidity"]):
            i.plot(df.index,df[j], color=self.colors[j])
            i.set_ylabel(self.labels[j],color=self.colors[j])
            i.grid(alpha=0.2)
        
        ax4.spines["right"].set_position(("outward",60))
        ax3.spines["left"].set_position(("outward",60))
        plt.show()

l=[]
resolucion=15
for i in ["itm","caucasia","santafe","oriente","turbo","sonson","yarumal"]:
    if i == "santafe" or i == 'itm':
        v = {"santafe": "SantaFe", "itm" : "ITM"}
        folder = v[i]
    else: 
        folder = i.capitalize()
    cd_anom(f"/home/ubuntu/Data/DatosMeteo{folder}/meteoro-{i}.db","DatosMeteoro")
    print("Stats ready for ", i)
    globals()[f"{i}"]=estacion(i)
    l.append(globals()[f"{i}"])
    globals()[f"{i}"].lista_eventos(resolucion)
    print("Eventos listo para", i)
    globals()[f"{i}"].composite_mean(1)
    print("Composite mean ready for", i)

print(datetime.now()-t1)
