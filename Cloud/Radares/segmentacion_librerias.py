import os
import shutil

math_package = ["scipy", "numpy", "pandas"]
graph_package = ["bokeh", "matplotlib"]
net_cdf = ["xarray", "pyart", "netCDF4"]
amazon = ["botocore", "aiobotocore", "boto3"]

packages = os.listdir("Cloud/Radares/deployment-package")

groups = {"math_package":math_package, 
          "graph_package":graph_package, 
          "net_cdf":net_cdf,
          "amazon":amazon}

group_lists = []
otros = []


for lists in groups.keys():
    group_lists.extend(groups[lists])

for package in packages:
    for group_pack in group_lists:
        if (group_pack in package) and ("info" in package):
            group_lists.append(package)
            break
    otros.append(package) if package not in group_lists else 0


for grupo in groups.keys():
    for packages_group in groups[grupo]:
        for pack in group_lists:
            if packages_group == pack:
                continue
            elif packages_group in pack:
                groups[grupo].append(pack)

groups["otros"] = otros

os.mkdir("Cloud/Radares/segmented_deployment") if "segmented_deployment" not in os.listdir("Cloud/Radares") else 0

packs_locs = {}

for group in groups.keys():
    for pack in groups[group]:
        packs_locs[pack] = group

for package in packages:
    try:
        shutil.copytree(
            f"Cloud/Radares/deployment-package/{package}",
            f"Cloud/Radares/segmented_deployment/{packs_locs[package]}/{package}"
        )
    except:
        shutil.copy(
            f"Cloud/Radares/deployment-package/{package}",
            f"Cloud/Radares/segmented_deployment/{packs_locs[package]}/{package}"
        )