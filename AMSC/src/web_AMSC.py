import os
import folium
import streamlit as st
from datetime import date, timedelta
from streamlit_folium import folium_static
from utils import queries_comandos, mails

st.set_page_config(
    page_icon = ":sun_small_cloud:",
    layout="wide")

st.sidebar.write("""<p style='text-align:center; font-size:9px;'>
                <i><b> ADVERTENCIA:</b> ESTE SITIO ES PRODUCIDO CON FINES NETAMENTE ACADÉMICOS. LA INFORMACIÓN AQUÍ PUBLICADA NO SIGUE LOS MISMOS ESTÁNDARES DE AGENCIAS OFICIALES COMO EL <a href='http://www.siata.gov.co'>SIATA</a> O <a href='http://www.ideam.gov.co'>IDEAM</a>. 
                POR TANTO NO NOS HACEMOS RESPONSABLES DEL USO QUE USTED LE DÉ A LA INFORMACIÓN DESPLEGADA EN ESTA PÁGINA. 
                PARA INFORMACIÓN OFICIAL POR FAVOR VISITAR LAS PÁGINAS COMPARTIDAS </i></p>""",unsafe_allow_html=True)

st.sidebar.markdown("---")

st.title('Antioquia Mira su Cielo :sun_small_cloud:')

m=folium.Map(location=[7.15,-75.492212], zoom_start=7.5)
ant=f"config/Antioquia.geo.json"
style_function = lambda x: {'fillColor': '#a14216', "fillOpacity":0.1,
                            "color":"#000", "weight":2}
folium.GeoJson(data=ant,zoom_on_click=True,style_function=style_function).add_to(m)

#Definir último dato en sidebar y en mapa
estaciones = queries_comandos.query().query_estaciones_web()

for estacion in estaciones:
    utc = 'UTC' if estacion[0] == 1 else ''
        
    html=f"""
        <p style='font-size:14px;'><i><b>{estacion[6]}</p></i></b>
        <ul style='list-style-type:square;'>
        <li><b>Fecha:</b> {estacion[1]} {utc}</li>
        <li><b>Temperatura:</b> {estacion[2]} °C</li>
        <li><b>Humedad Relativa:</b> {estacion[3]} %</li>
        <li><b>Presión:</b> {estacion[4]} hPa</li>
        <li><b>Precipitación:</b> {estacion[5]} mm/h</li></ul>
        """
    popup = folium.Popup(html = html, unsafe_allow_html = True, max_width = 200)
    icon = folium.Icon(color = "lightgreen", icon="cloud", prefix="fa") if estacion[-1] == "Habilitada" else folium.Icon(color = "red", icon="cloud", prefix="fa")
    folium.Marker(location=[estacion[-4], estacion[-3]], icon = icon, popup = popup).add_to(m)

    st.sidebar.markdown(f" <span style='color: #045FB4'> {estacion[6]}: </span> {estacion[1]} {utc}", unsafe_allow_html=True)
    
folium_static(m,width=750,height=700)

estaciones_dic = {}
for estacion in estaciones:
    estaciones_dic[estacion[6]] = estacion[0]

if st.sidebar.checkbox("Marque si desea ver el menú de descarga"):
    st.subheader("Sección de descarga", divider = "blue")
    columnas = st.columns((4))
    fecha_inicial = columnas[0].date_input("Seleccione fecha inicial")
    fecha_final = columnas[1].date_input("Seleccione fecha final")
    var = columnas[2].multiselect("Seleccione las variables", ["Temperatura", "Presion", "Humedad Relativa", "Precipitacion"])
    var_names = [f'observaciones.{variable.replace(" ","_").lower()}' for variable in var]
    estaciones_multi = columnas[3].multiselect("Seleccione la estación", [estacion[6] for estacion in estaciones])
    estaciones_multi_index = []
    for estacion_multi in estaciones_multi:
        estaciones_multi_index.append(estaciones_dic[estacion_multi])

    destinatario = columnas[0].text_input("Ingrese su correo", "ejemplo@gmail.com")
    if len(estaciones_multi) != 0:
        resultados = queries_comandos.query().query_multivar_rango(tuple(estaciones_multi_index), var_names, fecha_inicial, fecha_final)
        boton = columnas[0].button("Haga Click para recibir la información a su correo", type="primary")
        if boton and "@" not in destinatario:
            st.write(":warning: Escriba un correo válido por favor", unsafe_allow_html=True)
        elif boton:
            mail = mails.Mail()
            df = mail.send_mail_descarga_amsc(destinatario=destinatario, resultados=resultados)
            st.write(f"Se ha enviado con éxito el archivo de descarga a {destinatario} :white_check_mark:. A continuación puede ver una vista Previa de sus datos", unsafe_allow_html=True)
            st.dataframe(df)