import sqlite3 as sql
import pandas as pd
import numpy as np
import glob
import os
import re

x=glob.glob("Satelite/*.csv")
estaciones={}

for i in x:
    nombre=i.split("/")[-1].split("_")[1]
    estaciones[nombre]=pd.read_csv(i)

    con=sql.connect("Satelite/GOES.db")
    estaciones[nombre].to_sql(nombre,con)
    con.commit()
    con.close()

print(estaciones["Turbo"].columns)