import os
import sqlite3
import smtplib
import numpy as np
import pandas as pd
from datetime import datetime
from email.mime.text import MIMEText
from email.message import EmailMessage
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication


def cd_anom(nombredb, nombre_tabla):
    c=sqlite3.connect(nombredb)
    
    query=f"""SELECT * FROM {nombre_tabla}
    """
    df=pd.read_sql_query(query,c)

    df["Tiempo Sistema"]=pd.to_datetime(df["Tiempo Sistema"], format="%Y-%m-%d %H:%M:%S")
    df.drop_duplicates(subset="Tiempo Sistema", inplace=True)
    df.sort_values(by="Tiempo Sistema", ascending=True, inplace=True)
    df.set_index("Tiempo Sistema", inplace=True)

    df["Hora-Min"]=df.index.strftime("%H:%M")
    df["Mes"]=df.index.month

    vars=["Outside Temperature","Outside Humidity","Solar Radiation","Barometer","Hora-Min","Mes"]

    std=df[vars].groupby(["Mes","Hora-Min"]).std()
    mean=df[vars].groupby(["Mes","Hora-Min"]).mean()

    for i in std.columns:
        std.rename({i:"std "+i}, axis=1, inplace=True)
    for i in mean.columns:
        mean.rename({i:"mean "+i}, axis=1, inplace=True)
    stats=pd.merge(mean,std, left_index=True, right_index=True, how="inner")
    stats.reset_index(inplace=True)

    try:
        stats.to_sql("Stats", c, index=False, if_exists="replace")
    except:
        print('Falla al crear tabla')
        pass

    c.commit()
    c.close()

def ciclo_anom(df):
    df["Hora-Min"]=df.index.strftime("%H:%M")
    df["Mes"]=df.index.month

    cd=df.groupby(["Mes","Hora-Min"]).mean()
    std=df.groupby(["Mes","Hora-Min"]).std()
    for i in std.columns:
        std.rename({i:"std "+i},axis=1,inplace=True)
    name={}
    for i in cd.columns:
        name[i]="CD "+i
        cd.rename(name, axis=1, inplace=True)
    cd=pd.merge(cd,std,how="inner",left_index=True,right_index=True)
    minut=np.arange(0,60,1)
    cd.reset_index(inplace=True)
    cd["Minutes"]=pd.to_datetime(cd["Hora-Min"], format="%H:%M").dt.minute
    vhora=cd["Hora-Min"][cd["Minutes"].isin(minut)].drop_duplicates().array
    vhora=np.append(vhora,["23:59"])
    cd.drop(["Minutes"],axis=1, inplace=True)

    df.reset_index(inplace=True)
    merge=pd.merge(df,cd, how="left", left_on=["Mes", "Hora-Min"], right_on=["Mes","Hora-Min"]).drop_duplicates(subset="Tiempo Sistema")
    merge.set_index("Tiempo Sistema", inplace=True)
    df.set_index("Tiempo Sistema", inplace=True)

    v=["Barometer","Outside Humidity", "Outside Temperature", "Solar Radiation", "Wind Speed", "Rain Rate"]

    for i in v:
        merge["Anomally "+i]=merge[i]-merge["CD "+i]
        merge["Lim inf "+i]=merge["CD "+i]-merge["std "+i]
        merge["Lim sup "+i]=merge["CD "+i]+merge["std "+i]
    return merge


#Función para unir la información traída de stats, con la infromación corriente; y así calcular anomalías
def tabla_anom(est_escog,nombredb, resolucion):

       con=sqlite3.connect(nombredb)
       meses=est_escog.index.month.drop_duplicates().array
       if len(meses)==1:
                query=f"""SELECT * FROM 'Stats' 
                WHERE Mes IS {(meses[0])} """
       else:
                query=f"""SELECT * FROM 'Stats' WHERE Mes IN {tuple(meses)}"""
       anom=pd.read_sql_query(query,con)
       variables=["Outside Temperature","Outside Humidity","Barometer", "Solar Radiation"]
       # Agregar el acumulado de precipitación del rango
       est_escog["Mes"]=est_escog.index.month
       est_escog["Hora-Min"]=est_escog.index.strftime("%H:%M")
       est_escog.reset_index(inplace=True)
       anomalias=pd.merge(est_escog,anom,left_on=["Mes", "Hora-Min"], right_on=["Mes", "Hora-Min"], how="left")
       anomalias.set_index("Tiempo Sistema", inplace=True)
       est_escog.set_index("Tiempo Sistema", inplace=True)
       for i in variables:
              anomalias["Anomaly "+i]=anomalias[i]-anomalias["mean "+i]
              
       listtokeep=variables.copy()
       for i in variables:
              listtokeep.append("mean "+i)
              listtokeep.append("std "+i)
              listtokeep.append("Anomaly "+i)
       listtokeep.append("Rain Rate")
       dicagg={} # Diccionario para la función agg
       for columna in listtokeep:
              if columna=="Rain Rate":
                     dicagg[columna]="sum"
              else:
                     dicagg[columna]="mean"
       try: 
              anomalias=anomalias.resample(f"{str(resolucion)}min").agg(dicagg)
              anomalias["ppt_acumulado"]=anomalias["Rain Rate"].cumsum()
              return anomalias
       except:
              return anomalias
       # anomalias=anomalias.resample("5min").sum()
       
## Send Email


def send_mail(estacion, hora_max):
    msg=EmailMessage()
    msg["Subject"]=f"Estacion {estacion}"
    msg["From"]="julio.bohorquez@udea.edu.co"

    msg_to=["esteban.silvav","marcela.echeverrig","fernanda.buritica",
                "daniel.arbelaezc","julio.bohorquez"]

    to_specific = {"Turbo" : ["vladimir.toro",'isaac.rivera'],
                "Sonson": ["tarsicio.arias"],
                "SantaFe": ["santiago.macia"],
                "Caucasia": ["marco.canas", "leidy.delaossa", "anibal.jimenez"],
                "Yarumal": ["yeyson.betancur"]}

    if estacion != "Oriente":
        msg_to.extend(to_specific[estacion])          
    
    for i in range(len(msg_to)):
        msg_to[i]=msg_to[i]+"@udea.edu.co"
    msg["To"]=msg_to

    html = f""" <h1 style="color:red;"> <i><b> ALERTA </i></b>  &#128680; </h1>
                <p style="size:16px;"> La estación estación  {estacion} no reporta datos 
                desde hace más de una hora, la última lectura de la estación fue {hora_max} revisar por favor
                la conexión de la estación. &#127758; </p>"""

    part=MIMEText(html,"html")
    msg.set_content(part)

    with smtplib.SMTP("smtp.gmail.com", 587) as smtp:
        smtp.ehlo()
        smtp.starttls()
        smtp.ehlo()

        smtp.login('julio.bohorquez@udea.edu.co',"blgtyjthlblzsufx")
        smtp.send_message(msg)

variables=["Tiempo Sistema","Barometer","Outside Temperature","Wind Speed","Wind Direction",
            "Outside Humidity","Rain Rate","UV","Solar Radiation","Day Rain","Month Rain","Year Rain"]
            
# Send Downloaded data

def mail_downloaded_data (date_min, date_max, database, mail, estacion):
    con = sqlite3.connect(database)

    date_min = datetime.strptime(date_min, "%Y-%m-%d")# %H:%M:%S")
    date_max = datetime.strptime(date_max, "%Y-%m-%d")# %H:%M:%S")


    query = f"""SELECT * FROM DatosMeteoro
            WHERE ([Tiempo Sistema] >= '{date_min}')
            AND ([Tiempo Sistema] <= '{date_max}')"""

    if "data_to_send" not in os.listdir():
        os.mkdir("data_to_send")
    name = mail.split("@")[0].replace(".","_") + "_" + database.split(".")[0]
    pd.read_sql(query, con).to_csv("data_to_send/" + name + ".csv", index = False)

    msg = MIMEMultipart()
    html = f""" En el contenido de este correo usted encontrará la solicitud de descarga para la estación
            {estacion} entre las fechas {str(date_min)} y {str(date_max)}"""

    part=MIMEText(html,"html")
    msg["Subject"] = f"Información solicitada de Antioquia Mira Su Cielo para {estacion}"
    msg["To"] = mail
    msg["From"] = "julio.bohorquez@udea.edu.co"
    msg.attach(part)
    
    with open("data_to_send/" + name + ".csv", "rb") as file:
        msg.attach(MIMEApplication(file.read(), name = name + ".csv" ))

    with smtplib.SMTP("smtp.gmail.com", 587) as smtp:
        smtp.ehlo()
        smtp.starttls()
        smtp.ehlo()
        smtp.login('julio.bohorquez@udea.edu.co',"blgtyjthlblzsufx")
        smtp.send_message(msg)
    os.remove("data_to_send/" + name + ".csv")

    con.commit()
    con.close()
