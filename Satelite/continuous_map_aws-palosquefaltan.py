#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
from datetime import datetime, timedelta, timezone
import matplotlib.pyplot as plt
from netCDF4 import Dataset
from pyproj import Proj
from mpl_toolkits.basemap import Basemap
from download_goes_v5 import sat_info, gen_Rad, def_coord
import os


# In[2]:


home = '/home/ubuntu/BanRep/'
#home = '/home/daniel/Documentos/Proyecto_BR/'

def menor10(d):
    if d < 10:
        d = f'0{d}'
    return(d)

def dir_exist(path):
    isExist = os.path.exists(path)
    if not isExist:
        os.makedirs(path)

pre_dir = f'{home}Predic/' # donde se corre el programa
dir_exist(pre_dir)

# In[3]:

tiempos = pd.DataFrame()
tiempos['dic'] = np.arange(pd.to_datetime('2022-12-01'),pd.to_datetime('2022-12-10'),pd.to_timedelta(1,'h'))

for todost in tiempos['dic']:
    tya = pd.to_datetime(f'{todost.year}-{todost.month}-{todost.day} {menor10(todost.hour)}')

    #tya = pd.to_datetime(datetime.now(timezone.utc))# - pd.to_timedelta(2,'h')
    #tya = pd.to_datetime(f'{tya.year}-{tya.month}-{tya.day} {menor10(tya.hour)}')
    #time


    # In[4]:


    canales = ['08','09','10']
    #time0 = pd.to_datetime('2022-11-30 17:00:00')-pd.to_timedelta(2,'h')
    time0 = tya-pd.to_timedelta(2,'h')
    tms = pd.to_datetime(np.arange(time0,time0+pd.Timedelta(hours=2),
                                   pd.to_timedelta(10,'min')))

    list_files = {}
    for cc in canales:   
        nc = []
        for i in range(1,17):
            if i<10:
                nc.append(f'0{i}')
            else:
                nc.append(f'{i}')
        #ch = [input(f'Select a channel: \n {nc} \n')]
        ch = [cc]
        dt = 10
        #name = 'RadC'+ch[0]
        name = f'Rad/C{ch[0]}'

        Nt = (60/dt)*24

        #out_fold = '%sImages/%s/%s/M%s/'%(home,name,time0.year,time0.month)
        out_fold = f'{pre_dir}Im_temp/{name}/'
        dir_exist(out_fold)
        f_old = np.sort(os.listdir(out_fold))
        f_dates = [fifi[27:38] for fifi in f_old]
        #out_fold = 'img_temp/'

        #for dd in range(time0.days_in_month):
            #time1 = time0+pd.to_timedelta(dd,'D')
        #for i in range(int(Nt)):
        for time in tms:
            #time = time0+pd.to_timedelta(dt*i,'min')
            T_ini = str(time)
            T_ini = T_ini.replace('-', '')
            T_ini = T_ini.replace(':', '')
            T_ini = T_ini.replace(' ', '-')
            T_fin = str(time+pd.to_timedelta(dt-1,'min'))
            T_fin = T_fin.replace('-', '')
            T_fin = T_fin.replace(':', '')
            T_fin = T_fin.replace(' ', '-')

            diia = time.day_of_year
            if diia < 10:
                diia = f'00{diia}'
            elif 10<=diia<100:
                diia = f'0{diia}'
            ss = f'{time.year}{diia}{menor10(time.hour)}{menor10(time.minute)}'

            #print(T_ini,T_fin,file=log_file)
            if ss in f_dates:
                #print(ss)
                bien = 'ok'
            else:
                image = gen_Rad(T_ini,T_fin,out_fold,ch,ltmin=-4.1,ltmax=12.78,lnmin=-81.03, lnmax=-64,temp = pre_dir+'LST_temp/')#,log_missing=log_miss)
                #print(image,file=log_file)
                #print(image)
        list_files[cc] = f_old
        #log_file.close()


    # In[5]:


    # Imagen de  muestra
    if len(f_old)!=0:
        ds = Dataset(out_fold+f_old[0])
    else:
        ds = Dataset(out_fold+image[0][9:])
    yc = ds.variables['y'][:]
    xc = ds.variables['x'][:]
    rad = ds.variables['Rad'][:]
    #yc,xc,rad,gip = crop2D(ds,'Rad',(-81.03,-64),(-4.1, 12.78))
    sat_h = ds.variables['goes_imager_projection'].perspective_point_height
    sat_lon = ds.variables['goes_imager_projection'].longitude_of_projection_origin
    sat_sweep = ds.variables['goes_imager_projection'].sweep_angle_axis


    # In[33]:


    def redu(ds,npx):
        #ds = Dataset(image)
        yc = ds.variables['y'][:]
        xc = ds.variables['x'][:]
        rad = ds.variables['Rad'][:]
        #yc,xc,rad,gip = crop2D(ds,'Rad',(-81.03,-64),(-4.1, 12.78))
        sat_h = ds.variables['goes_imager_projection'].perspective_point_height
        sat_lon = ds.variables['goes_imager_projection'].longitude_of_projection_origin
        sat_sweep = ds.variables['goes_imager_projection'].sweep_angle_axis

        r_soft = np.zeros((int(rad.shape[0]/npx),int(rad.shape[1]/npx)))
        yrd = np.zeros(int(len(yc)/npx))
        xrd = np.zeros(int(len(xc)/npx))
        for j in range(int(len(yc)/npx)):
            #cs_row = []
            yrd[j] = np.mean(yc[j*npx:j*npx+npx])
            for i in range(int(len(xc)/npx)):
                #cs_row.append(np.mean(rad.data[j*5:j*5+5,i*5:i*5+5]))
                r_soft[j,i] = np.mean(rad.data[j*npx:j*npx+npx,i*npx:i*npx+npx])
                if j==0:
                    xrd[i] = np.mean(xc[i*npx:i*npx+npx])
        return(r_soft,yrd,xrd)

    def date2name(date):
        yy = date.year
        dd = date.day_of_year
        hh = date.hour
        if dd < 10:
            dd = f'00{dd}'
        elif 10<=dd<100:
            dd = f'0{dd}'
        if hh<10:
            hh = f'0{hh}'
        mm = date.minute
        if mm<10:
            mm = f'0{mm}'
        return f'{yy}{dd}{hh}{mm}'


    # In[38]:


    def slope_calc(t0,t1):
        #r_soft = redu(image,10)
        tms = pd.to_datetime(np.arange(t1,t0,pd.to_timedelta(10,'min')))
        flist = [date2name(tt) for tt in tms]
        ch = ['08','09','10']
        npx = 10
        #rad_shape = (920, 920)
        slopes = {'C08': np.zeros((int(rad.shape[0]/npx),int(rad.shape[1]/npx))), 
                  'C09': np.zeros((int(rad.shape[0]/npx),int(rad.shape[1]/npx))), 
                  'C10': np.zeros((int(rad.shape[0]/npx),int(rad.shape[1]/npx)))}
        mes = t0.month
        if mes<10:
            mes=f'0{mes}'
        for band in ch:
            files = []
            path = f'{pre_dir}Im_temp/Rad/C{band}/'
            dir_exist(path)
            #media = mus[f'C{band}']
            #desv = sigs[f'C{band}']
            for ff in np.sort(os.listdir(path)):
                if ff[27:38] in flist:
                    files.append(ff)
            cont = 0
            for i in range(len(files)-1):
                f1 = files[i]
                f2 = files[i+1]
                #canal = f1[18:21]
                # Tomo los tiempos del nombre de cada imagen
                year1 = int(f1[27:31])
                day1 = int(f1[31:34])
                year2 = int(f2[27:31])
                day2= int(f2[31:34])
                time1 = pd.to_datetime(day1, unit='D', origin=pd.Timestamp(f'{year1}-01-01')-pd.to_timedelta(1,'d'))
                time1 = time1 + pd.to_timedelta(int(f1[34:36]),'h') + pd.to_timedelta(int(f1[36:38]),'min')
                time2 = pd.to_datetime(day2, unit='D', origin=pd.Timestamp(f'{year2}-01-01')-pd.to_timedelta(1,'d'))
                time2 = time2 + pd.to_timedelta(int(f2[34:36]),'h') + pd.to_timedelta(int(f2[36:38]),'min')
                # Leo los datos
                rad1 = Dataset(path+f1)#.variables['Rad'][:]
                rad2 = Dataset(path+f2)#.variables['Rad'][:]
                # Estandarizo **********
                #rad1 = (rad1 - media)/desv
                #rad2 = (rad2 - media)/desv
                #yc,xc,rad1,gip = crop2D(rad1,'Rad',(-81.03,-64),(-4.1, 12.78))
                #yc,xc,rad2,gip = crop2D(rad2,'Rad',(-81.03,-64),(-4.1, 12.78))
                # Promedio para el # de pixeles
                rad1,yrd,xrd = redu(rad1,npx)
                rad2,yrd,xrd = redu(rad2,npx)
                dx = (time2 - time1)/pd.Timedelta(hours=1) # El delta-x en la pediendte "siempre" es 10 minutos
                slopes[f'C{band}'] += (rad2 - rad1)/dx
                cont += 1
            slopes[f'C{band}'] /= cont
        #prueba = open(f'{pre_dir}coord.csv','w')
        #print('x_red, y_red',file=prueba)
        #for ii in range(slopes['C08'].shape[0]):
        #    print(*rain[ii],file=prueba,sep=',')
        #    print(f'{xrd[ii]},{yrd[ii]}',file=prueba)
        #prueba.close()
        return(slopes,yrd,xrd)


    # In[8]:

    def create_Rad(data,y,x,val,gip,path,out_name):
        '''Create a dataset and write to a netCDF file.
        This function was designed for Rad C09 images'''
        ds = Dataset(path+out_name,'w',format='NETCDF4')

        #Crear dimensiones
        ys, xs = def_coord(data,ds,y,x)
        fill = data.variables['Rad']._FillValue
        #fill = 2047
        #units = data.variables['Rad'].units
        #units = 'mW m-2 sr-1 (cm-1)-1'

        #Agregar variables
        Rad = ds.createVariable('Rain','i2',('y','x',),fill_value=fill)
        #Rad.units = data.variables['Rad'].units
        #Rad._FillValue = data.variables['Rad']._FillValue
        #Rad.setncatts({k: data.variables['Rad'].getncattr(k) for k in data.variables['Rad'].ncattrs()})

        # Goes Imager projection 
        G_ = ds.createVariable('goes_imager_projection','i4')
        G_.setncatts({k: gip.getncattr(k) for k in gip.ncattrs()})

        #Asignar valores
        ys[:] = y
        xs[:] = x
        Rad[:] = val

        ds.close()


    ti = time0 #d.to_datetime('2022-10-05 06:00')
    tf = time


    # In[9]:


    #tiempos = pd.to_datetime(np.arange(ti,tf,pd.to_timedelta(1,'h')))


    # In[39]:


    medias = [2.6,7,12]
    pend = [-0.1,-0.5,-0.8]

    slopes, yrd, xrd = slope_calc(tf,ti)
    rain = np.zeros(slopes['C08'].shape)
    rain[(slopes['C08']<pend[0]) & (slopes['C09']<pend[1]) & (slopes['C10']<pend[2])] = 1

    '''
    fig = plt.figure(figsize=(5,5),dpi=180)

    m = Basemap(resolution='l', projection='geos', lon_0=sat_lon,
                llcrnrx=xc.min()*sat_h, llcrnry=yc.min()*sat_h, rsphere=(6378137.00,6356752.3142), 
                urcrnrx=xc.max()*sat_h, urcrnry=yc.max()*sat_h)

    im = m.imshow(rain,origin='upper',cmap='Blues')#,vmax=np.ceil(TPW.max()))
    m.drawcoastlines()
    m.drawcountries(linewidth=1.5)
    m.drawstates()
    m.drawparallels(np.arange(-10,15,5),linewidth=0.3, 
                    color='black',labels=[True,False,False,True])
    m.drawmeridians(np.arange(-85,-60,5),linewidth=0.3, 
                    color='black',labels=[True,False,False,True])
    #plt.colorbar(shrink=0.8,ticks=[0, 1])
    #plt.show()
    plt.title(tya)
    #plt.savefig(f'mapas/{tya}.png')
    plt.close()
    '''

    # In[85]:

    rain_dir = f"{pre_dir}Rain_maps/" # donde se guardan los mapas
    dir_exist(rain_dir)
    map_name = str(tya)[:16]
    map_name = map_name.replace(' ','_')
    map_name = map_name.replace(':','')

    ################################
    # imprimir: 1 index
    #           2 Lon-Lat
    #           0 Na
    ################################

    imprimir = 0
    solo_rain = np.where(rain==1)

    if imprimir == 1:
        prueba = open(f'{rain_dir}rain_{map_name}.csv','w')
        print('y, x', file=prueba)
        for i in range(len(rain_site)):
            print(f'{solo_rain[0][i]},{solo_rain[1][i]}',file=prueba)
        prueba.close()

    elif imprimir == 2:
        prueba = open(f'{rain_dir}rcoord_{map_name}.csv','w')
        print('lon,lat', file=prueba)
        p = Proj(proj='geos', h=sat_h, lon_0=sat_lon, sweep=sat_sweep)
        rain_site = [p(xrd[solo_rain[1][i]]*sat_h,yrd[solo_rain[0][i]]*sat_h,inverse=True) for i in range(len(solo_rain[0]))]
        for i in range(len(rain_site)):
            print(*rain_site[i],sep=',',file=prueba) # Descomentar para escribir lon-lat
        prueba.close()

    ####################################
    # Crear NetCDF
    #################################

    netcdf_path = f'{pre_dir}Netcdf/'
    dir_exist(netcdf_path)
    gip = ds.variables['goes_imager_projection']
    create_Rad(ds,yrd,xrd,rain,gip,netcdf_path,f'rain_{map_name}.nc')

    ####################################

    print(f'Mapa {tya} hecho')

    # Borro las 6 imagenes mas antiguas
    for cc in canales:
        name = f'Rad/C{cc}'
        out_fold = f'{pre_dir}Im_temp/{name}/'
        if len(list_files[cc]) >= 6:
            for i in range(6):
                os.remove(f'{out_fold}{list_files[cc][i]}')

