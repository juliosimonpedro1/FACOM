import os
import folium
import base64
import numpy as np
import pandas as pd
import sqlite3 as sql
from PIL import Image
import streamlit as st
import plotly.graph_objects as go
from streamlit_folium import folium_static
from datetime import date, timedelta, datetime, time
from funciones_aeropuertos import mail_downloaded_data

st.set_page_config(layout ="wide")
st.markdown("## Página para visualizar el estado actual del tiempo :airplane:")

#Cargar los gif
c1,c2=st.columns((1,1))
path_visible = 'GOES16/geocolor.gif'

# with open(path_visible, "rb") as file:
#     contenido=file.read()
#     data_url = base64.b64encode(contenido).decode("utf-8")
# c1.markdown(
#     f'<img src="data:image/gif;base64,{data_url}" alt="geocolor gif" width=600 height=610>',
#     unsafe_allow_html=True)


#Última imagen estática
images = os.listdir("GOES16/images")
images.sort(reverse = False)
ult_img = "GOES16/images/" + images[-1]
image = Image.open(ult_img)
image = image.resize((600, 620))
c1.image(image)

# Cargar GIF
path_gif="LST.gif"

file=open(path_gif,"rb")
contenido=file.read()
data_url = base64.b64encode(contenido).decode("utf-8")
file.close()

c2.markdown(
    f'<img src="data:image/gif;base64,{data_url}" alt="LST gif" width=600 height=610>',
    unsafe_allow_html=True)

st.markdown("""
            """)

c1,c2=st.columns((2,5)) #columnas donde irá el mapa y las opciones de gŕafico

fecha=(datetime.now()-timedelta(days=14)).date()
hora=time(0,0,0)
date_to_query=(datetime.combine(fecha,hora))


with c1:
    dic_choice=st.selectbox("Escoja La variable que desea graficar: ",["Temperatura","Humedad Relativa","Presión"])
    st.markdown("---")
    md="""<p style='font-size:16px;'> <i>A continuación escoja las características de la consulta que se desea realizar para visualizar la evolución temporal 
    de la variable en un aeropuerto en específico. </i></p>"""
    st.markdown(md,unsafe_allow_html=True)
    st.markdown("")
    start_date=st.date_input("Escoja la fecha inicial de la consulta: ",date_to_query)
    end_date=st.date_input("Escoja la fecha final de la consulta: ", datetime.now()+timedelta(days=1))

con=sql.connect("aeropuertos.db")
cursor=con.cursor()
query=f"""SELECT * from datos_aeropuerto
    WHERE Hora<='{end_date}' AND Hora>='{start_date}'"""

query_lat="""SELECT * FROM ubicacion WHERE Latitud IS NOT NULL"""

df1=pd.read_sql(query,con)
ubi=pd.read_sql(query_lat,con)

con.commit()
con.close()


hora_now=(datetime.now()-timedelta(hours=5)).hour
df1["Hora"]=pd.to_datetime(df1["Hora"],format="%Y-%m-%d %H:%M:%S")
df1["Hora"].where(df1["Hora"].dt.minute<50,df1["Hora"]+timedelta(minutes=20),inplace=True)
df1["Presion"]=round(df1["Presion"]*3386.389/100,2)
if df1[df1["Hora"].dt.hour==hora_now].shape[0]==0:
    df=df1[df1["Hora"].dt.hour==hora_now-1]
else:
    df=df1[df1["Hora"].dt.hour==hora_now]
df=pd.merge(ubi,df,how="left",left_on="Nombre",right_on="Nombre")
dfchoro=df.groupby("Departamento").mean() #DF to choropleth


#Empezar a realizar el mapa

#GeoJson con los dptos
dptos=f"Colombia.geo.json"


if datetime.now().hour>=0 and datetime.now().hour <= 11 :
    theme="dark_matter"
else:
    theme="positron"
m = folium.Map(location=[4.57,-74.29], tiles='CartoDB '+theme, name="Light Map",
               zoom_start=6, attr="My Data attribution")


hora=df["Hora"].max()
c2.markdown(f"<p style='font-size: 10px;'><i> La Fecha de los datos desplegados es: {hora} </i></p>", unsafe_allow_html=True)


dic={"Temperatura":"Temperatura", "Humedad Relativa":"Humedad_Relativa", "Presión":"Presion"}
choice_selected=dic[dic_choice]

color={"Temperatura":"YlOrRd", "Humedad_Relativa":"BuPu", "Presion":"YlGn"}
dic_unidades={"Temperatura":"°C","Humedad_Relativa":"%", "Presion":"hPa"}
folium.Choropleth(
    geo_data=dptos,
    name="choropleth",
    data=dfchoro,
    columns=[dfchoro.index,choice_selected],
    key_on="feature.properties.NOMBRE_DPT",
    fill_color=color[choice_selected],
    fill_opacity=0.7,
    line_opacity=.1,
    legend_name=dic_choice
).add_to(m)
# ADD markers
fg=folium.FeatureGroup(name="District Info")
df.fillna({"Temperatura":"No Reporta","Humedad_Relativa":"No Reporta","Presion":"No Reporta"},inplace=True)
for i in df.index:
    lat=df.loc[i,["Latitud"]].values
    lon=df.loc[i,["Longitud"]].values
    nombre=df.loc[i,["Nombre"]].values[0]
    dpto=df.loc[i,["Departamento"]].values[0]
    valor_f=df.loc[i,["Hora"]].values[0]
    valor_var=df.loc[i,[choice_selected]].values[0]
    if df.loc[i,[choice_selected]].values[0] == "No Reporta":
        color="red"
        unidades=""
        valor_f=""
    else:
        color="lightblue"
        unidades=dic_unidades[choice_selected]
        valor_f=valor_f.time()
    html=f"""<h7>{nombre}</h7>
    <ul style="list-style-type:square">
    <li>Hora: {valor_f}</li>
    <li> {dic_choice}: {valor_var} {unidades}</li>
    </ul>"""
    popup=folium.Popup(html,unsafe_allow_html=True)
    icon=folium.Icon(color=color, icon="cloud", prefix="fa")
    folium.Marker(location=[lat,lon],icon=icon,popup=popup).add_to(m)

with c2:
    folium_static(m,width=800,height=800)

with c1:
    dpto=st.selectbox("Escoja el Departamento que desea consultar", ubi["Departamento"].sort_values(ascending=True).drop_duplicates())

with c1:
    aeropuerto=st.selectbox("Escoja el aeropuerto que desea conslutar", ubi["Nombre"][ubi["Departamento"]==dpto])


if c1.checkbox("Escoja si desea observar la localización de los aeropuertos"):
    st.dataframe(ubi.drop("id",axis=1).rename({"Nombre":"Nombre Aeropuerto","Longitud":"Longitud",
                "Latitud":"Latitud","Departamento":"Departamento"},axis=1))


dfplot=df1[df1["Nombre"]==aeropuerto]
dfplot.sort_values(by="Hora", inplace=True)
color={"Temperatura":"firebrick","Humedad_Relativa":"darkviolet","Presion":"forestgreen"}


unidades={"Temperatura": "[°C]", "Humedad Relativa":"[%]", "Presión":"hPa"}


fig=go.Figure()

fig.add_trace(go.Scatter(x=dfplot["Hora"],y=dfplot[choice_selected],
    line=dict(color=color[choice_selected]), name=dic_choice))

fig.update_layout(title=dict(text="Valores de "+dic_choice+" de "+date_to_query.strftime("%Y-%m-%d %H:%M:%S")+" a " +\
    (datetime.now()-timedelta(hours=5)).strftime("%Y-%m-%d %H:%M:%S"),
    x=0.5,y=0.9, font=dict(size=18,family="Times New Roman")),
    width=1200, height=700)

fig.update_layout(yaxis=dict(title=dict(text=dic_choice+" "+unidades[dic_choice]),
    titlefont=dict(color=color[choice_selected])))
st.write(fig)


## Mandar Correo
st.title("Sección de descarga")

c1, c2 = st.columns((2.5,5))

c1.write("""
        <p class="text-justify"> Esta sección será dedicada a descargar información de todos los aeropuertos, 
            la cual será enviada por correo en formato csv </p>""", unsafe_allow_html=True)

df = pd.read_csv("Aeropuertos.csv")[["Departamento", "Nombre", "Longitud", "Latitud"]]\
    .sort_values(by="Departamento", ascending = True).set_index("Departamento")
c2.dataframe(df, use_container_width=True, height = 500)

opciones = ["Todos los aeropuertos", "Aeropuertos específicos"]
opcion = c1.selectbox("Marque tipo de consulta", opciones)

aeropuertos = True
if opcion == opciones[-1]:
    df["Dpto-Aeropuerto"] = df.index + " - " + df["Nombre"]
    aeropuertos = c1.multiselect("Seleccione los aeropuertos que requiere", df["Dpto-Aeropuerto"])
    aeropuertos = list(map(lambda x : x.split(" - ")[1], aeropuertos))

correo = c1.text_input("Escriba su correo")
fecha_inicial = c1.date_input("Ingrese Fecha inicial de la consulta", datetime.now()-timedelta(days=1))
fecha_final = c1.date_input("Ingrese Fecha final de la consulta")


if c1.button("Presione para enviar la información"):
    mail_downloaded_data (fecha_inicial, fecha_final, correo, aeropuertos)

st.write("""<p style='text-align:center; font-size:12px;'>
              <i><b> ADVERTENCIA:</b> ESTE SITIO ES PRODUCIDO CON FINES NETAMENTE ACADÉMICOS. LA INFORMACIÓN AQUÍ PUBLICADA NO SIGUE LOS MISMOS ESTÁNDARES DE AGENCIAS OFICIALES COMO EL <a href='http://www.siata.gov.co'>SIATA</a> , el <a href='http://www.ideam.gov.co'>IDEAM</a> o la
              <a href=https://www.aerocivil.gov.co> AEROCIVIL </a>
              POR TANTO NO NOS HACEMOS RESPONSABLES DEL USO QUE USTED LE DÉ A LA INFORMACIÓN DESPLEGADA EN ESTA PÁGINA. 
              PARA INFORMACIÓN OFICIAL POR FAVOR VISITAR LAS PÁGINAS COMPARTIDAS </i></p>""",unsafe_allow_html=True)
