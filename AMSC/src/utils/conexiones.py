import os
import json
import psycopg2
from sqlalchemy import create_engine

with open("config/conexiones.json", "r") as fp:
    conexiones = json.load(fp)

class conexion():
    global conexiones 
    def __init__(self, connection_type):
        connection_types = ["host", "RDS"]
        if connection_type not in connection_types:
            raise TypeError (f"Escoga una conexión válida")
        self.connection_type = connection_type
        if self.connection_type == "RDS":
            self.host = conexiones[self.connection_type]["host"]
            self.port = conexiones[self.connection_type]["port"]
            self.database = conexiones[self.connection_type]["database"]
            self.username = os.environ.get("PostgreSQL_AMSC_username")
            self.password = os.environ.get("PostgreSQL_AMSC_password")

    def conectar(self):
        if self.connection_type == "RDS":
            con = psycopg2.connect(
                    host = self.host,
                    port = self.port,
                    dbname = self.database,
                    user = self.username,
                    password = self.password)
            return con
        
        elif self.connection_type == "host":
            self.path = conexiones[self.connection_type]["path"]
            return self.path
    
    def create_engine_sql(self):
        return create_engine(f'postgresql+psycopg2://{self.username}:{self.password}@{self.host}:{self.port}/{self.database}')