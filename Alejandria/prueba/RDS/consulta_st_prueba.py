import streamlit as st
import pandas as pd
from sqlalchemy import create_engine

hostname="database-1.c6l43uy718kg.us-east-1.rds.amazonaws.com"
username="postgres"
password="Cripi123456+"
database="prueba_ideam"

engine=create_engine(
    f"postgresql://{username}:{password}@{hostname}/{database}"
)


st.title("Prueba página en la nube :sun_small_cloud:")

codigos_var={"temperatura": "firebrick",
                "precipitacion": "teal",
                "presion":"navy",
                "direccion_viento":"darkpurple",
                "velocidad_viento":"peru",
                "humedad":"forestgreen"}


variable=st.sidebar.selectbox("Seleccione la variable", codigos_var.keys())



db=pd.read_sql(f'SELECT * FROM {variable}', con=engine)
db["fechaobservacion"]=pd.to_datetime(db["fechaobservacion"], format="%Y-%m-%dT%H:%M:%S")

st.dataframe(db.head(10))

