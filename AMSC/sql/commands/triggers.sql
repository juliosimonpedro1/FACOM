CREATE OR REPLACE TRIGGER trg_max_values
AFTER INSERT ON observaciones
FOR EACH ROW
EXECUTE FUNCTION func_trg_max_values();