CREATE OR REPLACE FUNCTION func_trg_max_values()
RETURNS TRIGGER AS $func_trg_max_values$
	BEGIN
		WITH max_date AS (
			SELECT
			id_estacion,
			max(fecha) fecha 
			FROM observaciones
			GROUP BY id_estacion),
		max_values AS (
			SELECT 
			max_date.id_estacion,
			max_date.fecha,
			temperatura,
			humedad_relativa,
			presion,
			precipitacion 
			FROM observaciones
			INNER JOIN max_date
			ON max_date.id_estacion = observaciones.id_estacion
			AND max_date.fecha = observaciones.fecha)
		INSERT INTO ultimo_dato(id_estacion, fecha, temperatura, humedad_relativa, presion, precipitacion)
		SELECT
		*
		FROM max_values
		WHERE max_values.id_estacion = NEW.id_estacion
		ON CONFLICT (id_estacion) DO UPDATE 
		SET fecha = EXCLUDED.fecha,
		temperatura = EXCLUDED.temperatura,
		humedad_relativa = EXCLUDED.humedad_relativa,
		presion = EXCLUDED.presion,
		precipitacion = EXCLUDED.precipitacion;
		RETURN NEW;
	END;
$func_trg_max_values$ LANGUAGE plpgsql;
	