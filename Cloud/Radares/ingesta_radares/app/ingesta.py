import os
import json
import boto3
import pyart
import shutil
import netCDF4
import logging
import numpy as np
import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
from botocore import UNSIGNED
from botocore.config import Config
from pyart.config import get_metadata
from matplotlib.transforms import Bbox
from datetime import datetime, timedelta



logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(getattr(logging, os.getenv('LOG_LEVEL', 'INFO')))
logger.info('Start execution')


s3_ideam=boto3.client("s3", config=Config(signature_version=UNSIGNED), region_name="us-east-1")
Bucket_ideam="s3-radaresideam"
Prefix_ideam='l2_data'

# session = boto3.Session(profile_name="FACOM_IaC")
# sts_facom = session.client("sts")
# RoleArn = "arn:aws:iam::599754027366:role/OrganizationAccountAccessRole"
# RoleSessionName = "FACOM"

# role_assumed = sts_facom.assume_role(
#     RoleArn = RoleArn,
#     RoleSessionName = RoleSessionName
# )
# credentials = role_assumed["Credentials"]


# s3_facom = boto3.client("s3",
#                         aws_access_key_id = credentials["AccessKeyId"],
#                         aws_secret_access_key=credentials["SecretAccessKey"],
#                         aws_session_token=credentials["SessionToken"])

s3_facom = boto3.client("s3")
Bucket_facom = "facom-radares-ideam"

with open("params/datos_radares.json", "r") as f:
    radares = json.load(f)

class clase_radar:
    global radares, Bucket_facom, Bucket_ideam, Prefix_ideam

    def __init__(self, nombre):
        self.nombre=nombre
        self.folder_s3=radares[self.nombre]["Nombre s3"] #Nombre en s3
        self.lat=radares[self.nombre]["Coordenadas"]["lat"] #Latitud del radar
        self.lon=radares[self.nombre]["Coordenadas"]["lon"] #Longitud del radar

    def ingesta(self):
        
        logger.info(self.nombre)
        fecha=datetime.now()# + timedelta(hours=5)
        dia=fecha.strftime("%d")
        mes=fecha.strftime("%m")
        ano=fecha.strftime("%Y")

        path_aws=f"{Prefix_ideam}/{str(ano)}/{str(mes)}/{str(dia)}/{self.folder_s3}"
        logger.info(f"El path inicial es: {path_aws}")
        while "Contents" not in list(s3_ideam.list_objects(Bucket=Bucket_ideam, Prefix=path_aws).keys()):
            fecha=fecha-timedelta(days=1)
            path_aws=f"{Prefix_ideam}/{fecha.strftime('%Y')}/{fecha.strftime('%m')}/{(fecha).strftime('%d')}/{self.folder_s3}/"
        
        logger.info(f"En la ingesta se encontró este que este path tiene contenido --> {path_aws}")
        paginator=s3_ideam.get_paginator('list_objects_v2') 
        pages=paginator.paginate(Bucket=Bucket_ideam, Prefix=path_aws)
        

        datos = []
        for page in pages:
            datos.extend(page["Contents"])
        
        datos_df = pd.DataFrame(datos)[["Key","LastModified"]]
        datos_df = datos_df[datos_df.LastModified >= datos_df.LastModified.max() - timedelta(minutes= 3)]
        datos_df = datos_df[~datos_df.Key.str.contains(".nc.gz")]

        self.archivos = datos_df.Key.tolist()
        self.archivos = list(map(lambda x: x.split('/')[-1], self.archivos))

        for Key in datos_df.Key:
            objeto = s3_ideam.get_object(Bucket = Bucket_ideam, Key = Key)["Body"]
            try: 
                with open(f"/tmp/{Key.split('/')[-1]}", "wb") as f:
                    shutil.copyfileobj(objeto, f)
                logger.info(f"Se copió con éxito el archivo {Key.split('/')[-1]}")
            except:
                logger.info(f"Se encontró un error copiando el archivo {Key.split('/')[-1]}")
                self.archivos.remove(Key.split("/")[-1])
        logger.info(self.archivos)

    def plot(self):
        nc_list = ["Bogotá", "Santa Elena"]
        archivos = {}

        for file in self.archivos:
            archivo = f"/tmp/{file}"
            shape = 1000

            if self.nombre in nc_list:
                dataset = netCDF4.Dataset(archivo)
                radar = pyart.testing.make_empty_ppi_radar(dataset.variables["range"].shape[0], dataset.variables["azimuth"].shape[0], 1)
                radar.time["data"] = np.array(dataset.variables["time"][:])
                radar.elevation["data"] = dataset.variables["elevation"][:]
                radar.azimuth["data"] = dataset.variables["azimuth"][:]
                radar.range["data"]=dataset.variables["range"][:]
                radar.latitude["data"] = np.array([float(dataset.variables["longitude"][:])])
                radar.longitude["data"] = np.array([float(dataset.variables["latitude"][:])])
                radar.sweep_number["data"] = np.array(dataset.variables["sweep_number"])
                ref_dict = get_metadata("DBZH")
                ref_dict["data"] = dataset.variables["DBZH"][:]
                radar.fields = {"DBZH": ref_dict}
                fields=["DBZH"]
                fecha = dataset.variables["time"].units.replace("since", "").split(" ")[-1] if self.nombre == "Santa Elena" else dataset.variables["time"].units.split(" ")[-1]

            else:
                radar = pyart.io.read(archivo)
                fields = ["reflectivity"]
                fecha = radar.time["units"].split(" ")[-1]
            elevacion = radar.elevation["data"][0]
            radio = radar.range["data"].max() * np.cos(np.radians(elevacion))
           
            nombre_archivo = file.split('.')[0]
            archivos[nombre_archivo] = {}
            archivos[nombre_archivo]["Radio"] = float(radio)
            archivos[nombre_archivo]["Elevacion"] = float(elevacion)
            archivos[nombre_archivo]["Fecha"] = str(datetime.strptime(fecha.replace("T"," ").replace("Z",""), "%Y-%m-%d %H:%M:%S") - timedelta(hours = 5))

            grid=pyart.map.grid_from_radars(
                radar,
                grid_shape=(1,shape, shape),
                grid_limits=((0,10000), (-radio, radio), (-radio, radio)),
                fields=fields
            )

            try:
                pyart.io.write_grid(
                    grid=grid,
                    filename=f"/tmp/grid_{file.split('.')[0]}.nc",
                    include_fields=fields[0],
                    format="NETCDF4"
                )

                logger.info(f"Se acaba de hacer la conversión a objeto de grilla {file}")
                nc = netCDF4.Dataset(f"/tmp/grid_{file.split('.')[0]}.nc", mode = "r")

            except:
                logger.info(f"No se pudo cargr el Dataset /tmp/grid_{file.split('.')[0]}.nc")
                continue

            ref=nc.variables[fields[0]][::,::]
            fig=plt.figure(figsize=(15,15))
            plt.imshow(np.where(ref[0][0].filled(np.nan)<0, np.nan, ref[0][0].filled(np.nan)),
                origin="lower", cmap = pyart.graph.cm.NWSRef, vmin=0, vmax=70)
            plt.axis("off")
            bbox=Bbox([[0,0],[1000,1000]])
            bbox = bbox.transformed(fig.gca().transData).transformed(fig.dpi_scale_trans.inverted())
            plt.savefig(f"/tmp/plot_{file.split('.')[0]}.png", transparent=True, bbox_inches=bbox)
            s3_facom.upload_file(f"/tmp/plot_{file.split('.')[0]}.png", Bucket_facom, f"lista_radares/{self.folder_s3}/plot_{file.split('.')[0]}.png")
            logger.info(f"Se acaba de guardar plot_{file.split('.')[0]}.png")
            
        with open(f"/tmp/{self.folder_s3}.json", "w") as f:
            json.dump(archivos, f)

        s3_facom.upload_file(f"/tmp/{self.folder_s3}.json", Bucket_facom, f"lista_radares/{self.folder_s3}/{self.folder_s3}.json")

def handler(event,context):
    objects = list(map(lambda x: {"Key":x["Key"]},s3_facom.list_objects(Bucket = Bucket_facom)["Contents"]))
    images =[]
    for object in objects: images.append(object) if ".png" in object["Key"] else 0
    s3_facom.delete_objects(Bucket = Bucket_facom, Delete = {"Objects": images})

    for radar in radares.keys():
        radares[radar]["Objeto"]=clase_radar(radar)
        radares[radar]["Objeto"].ingesta()
        radares[radar]["Objeto"].plot()
    logger.info(f"Se eliminaron {images}")
    return 

# handler("","")