WITH estaciones_data(nombre_folder, nombre_completo, latitud, longitud, color, estado) AS (
    VALUES
    ('Oriente', 'UdeA Oriente',6.106154134522639, -75.38783679205099, 'forestgreen', 'Habilitada'),
    ('Turbo', 'UdeA Turbo', 8.094314856114538, -76.7338430092313, 'firebrick', 'Habilitada'),
    ('Sonson', 'UdeA Sonson', 5.719219419105346, -75.29528859371956, 'peru', 'Habilitada'),
    ('Yarumal', 'UdeA Yarumal', 6.962234887882262, -75.41762081687851, 'pink', 'Habilitada'),
    ('SantaFe', 'UdeA Santa Fé', 6.555843977773638, -75.82621410794272, 'teal', 'Habilitada'),
    ('ITM', 'ITM Medellín', 6.274057264413763, -75.5887129303584, 'slategray', 'Habilitada'),
    ('Arboletes', 'UdeA Arboletes', 8.84891699901207, -76.42779770043498, 'navy', 'Deshabilitada')
)
INSERT INTO estaciones (nombre_folder, nombre_completo, latitud, longitud, color, estado)
SELECT
estaciones_data.*
FROM estaciones_data
LEFT JOIN estaciones
ON estaciones_data.nombre_completo = estaciones.nombre_completo
WHERE estaciones.nombre_completo IS NULL 
