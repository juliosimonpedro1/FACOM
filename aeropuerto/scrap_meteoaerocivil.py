import os
import re
import numpy as np
import pandas as pd
import sqlite3 as sql
import urllib.request
from tqdm import tqdm
from bs4 import BeautifulSoup
from datetime import date, datetime, timedelta


# Set URL to scrap
URL = "http://meteorologia.aerocivil.gov.co/obs_map"
pag = urllib.request.urlopen(URL).read().decode()
soup = BeautifulSoup(pag)

# Target tag
tags = soup("a")
labels=['Hora','Temperatura','Punto de Roció','Presión']
masterlist=[]

for tag in tqdm(tags):
    if "<br/><b>" in str(tag.get("onmouseover")):
        li=[]
        string=str(tag)
        string=string.split('onmouseover')[1][1:]
        li+=[string.split('&lt')[0][6:]]
        for i in labels:
            try:
                st2=(''.join(((string.split(i)[1]).split("/b"))[0:2])).split()[1:]
                st3=(' '.join(st2[0:3])).split('&lt')[0]
                li+=[st3]
            except:
                li+=['']
        masterlist+=[li]
df = pd.DataFrame (masterlist,columns=['Nombre']+labels)
for i in range(len(df['Hora'])):
    if datetime.now().hour>=5 and datetime.now().hour <=7 and (datetime.strptime(df["Hora"][i][:-1],"%H:%M")).hour >=4 \
        and (datetime.strptime(df["Hora"][i][:-1],"%H:%M")).hour <5:
        df['Hora'][i] = pd.to_datetime(str((datetime.now()-timedelta(days=1,hours=5)).date())+' '+\
            str((datetime.strptime(df['Hora'][i][:-1],"%H:%M")-timedelta(hours=5)).time()))
    else:
        df['Hora'][i] = pd.to_datetime(str((datetime.now()-timedelta(hours=5)).date())+' '+\
            str((datetime.strptime(df['Hora'][i][:-1],"%H:%M")-timedelta(hours=5)).time()))
for i in range(len(df['Temperatura'])):
    df['Temperatura'][i] = df['Temperatura'][i][:-2]
    df['Punto de Roció'][i] = df['Punto de Roció'][i][:-2]
df['Temperatura'] = pd.to_numeric(df['Temperatura'],errors='coerce')
df['Punto de Roció'] = pd.to_numeric(df['Punto de Roció'],errors='coerce')


for i in range(len(df['Presión'])):
    df['Presión'][i] = df['Presión'][i][:-5]
df['Presión'] = pd.to_numeric(df['Presión'],errors='coerce')
df.rename({"Presión":"Presion", "Punto de Roció":"Punto_de_Rocio"}, axis=1, inplace= True)
df["Humedad_Relativa"] = 100-5*(df["Temperatura"] - df["Punto_de_Rocio"])
df["Hora"] = df["Hora"].astype(str)

con = sql.connect('aeropuertos.db')
cursor = con.cursor()
for index in df.index:
    fila = df.iloc[index]
    query_insert = f"""
        INSERT OR IGNORE INTO datos_aeropuerto
        (Nombre, Hora, Temperatura, Punto_de_Rocio, Presion, Humedad_Relativa) 
        VALUES
        ('{fila["Nombre"]}','{fila["Hora"]}',?,?,?,?)"""
    cursor.execute(query_insert, ( fila["Temperatura"], fila["Punto_de_Rocio"], fila["Presion"], fila["Humedad_Relativa"]))
con.commit()
con.close()