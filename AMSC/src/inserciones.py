import re
import os
import logging
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from utils.queries_comandos import query, comando

logging.basicConfig(filename=f"logs/ingesta.txt", 
                    filemode='w', 
                    level=logging.INFO, 
                    format = "[%(asctime)s] %(levelname)s:  %(message)s",
                    datefmt = '%Y-%m-%d %H:%M:%S')

class ingesta():
	def __init__(self):
		self.estaciones = query().query_estaciones_habilitadas()
		print("Se conecta")

	def transformaciones(self, dataframe, id, estacion):
		columns_rename = {"Tiempo Sistema":"fecha", 
			"Barometer":"presion", 
			"Outside Temperature":"temperatura", 
			"Wind Speed":"velocidad_viento", 
			"Wind Direction":"direccion_viento", 
			"Outside Humidity":"humedad_relativa", 
			"Solar Radiation":"radiacion_solar", 
			"UV":"uv", 
			"Rain Rate":"precipitacion", 
			"Day Rain":"precipitacion_dia",
			"Month Rain":"precipitacion_mes", 
			"Year Rain":"precipitacion_ano"}
		dtypes = {}
		for key in columns_rename.values():
			if key in ["presion", "temperatura", "precipitacion"]:
				dtypes[key] = float
			elif key == "fecha":
				dtypes[key] = str
			else:
				dtypes[key] = int
		#Rename
		dataframe = dataframe[columns_rename.keys()].rename(columns_rename, axis = 1)
		dataframe = dataframe.astype(dtypes, errors = "ignore")
		dataframe["id_estacion"] = id

		#Transformaciones
		if estacion in ["Caucasia", "SantaFe"]:
			dataframe['presion'] = (dataframe['presion']/1000.*(3386.389/100.0))
		else:
			dataframe['presion'] = (dataframe['presion']/1000.*(3386.389/100.0)) - (1011.0 - 788.0)
		dataframe['temperatura'] = (dataframe['temperatura']/10. - 32.) * (5.0/9.0)
		dataframe.loc[(dataframe['temperatura'] > 50) | (dataframe['temperatura'] < -15)] = np.nan
		dataframe['precipitacion'] = dataframe['precipitacion']*0.2/60

		#Fechas
		dataframe["fecha"] = pd.to_datetime(dataframe.fecha, errors = 'coerce', format="%Y-%m-%d %H:%M:%S")
		# if estacion == "Oriente":
		# 	dataframe["fecha"] = dataframe["fecha"] - pd.to_timedelta(5,unit='h')
		dataframe.dropna(subset = "fecha", axis = 0, inplace = True)
		dataframe.drop_duplicates(subset = "fecha", inplace = True)

		# Aproximaciones
		dataframe["temperatura"] = round(dataframe["temperatura"], 5)
		dataframe["presion"] = round(dataframe["presion"], 5)
		dataframe['precipitacion'] = round(dataframe['precipitacion'], 5)
		return dataframe
	
	def iterate_month(self, path_estacion, ano_mes, id):
		df_dia = pd.DataFrame()
		path = f"{path_estacion}/{ano_mes}"
		for file in os.listdir(path):
			if not os.path.isfile(f"{path}/{file}"):
				continue
			try:
				df_nuevo = pd.read_csv(f"{path}/{file}")
			except:
				logging.error(f"[Carga Inicial] No se puede abrir el archivo {path}/{file}")
				continue
			df_dia = pd.concat([df_dia, df_nuevo])
		if "Tiempo Sistema" not in df_dia.columns:
			return False
		df_month = self.transformaciones(df_dia, id, path_estacion.split("DatosMeteo")[1])
		return df_month
	
	def iterate_estaciones(self):
		for estacion in self.estaciones:
			id = estacion[0]
			nombre = estacion[1]
			path = f"{query().return_path()}/DatosMeteo{nombre}"
			result = query().query_observaciones_max(id)
			meses = []
			for item in os.listdir(path):
				m = re.match("A.*M.*", item)
				meses.append(m[0]) if (m!= None) and not (os.path.isfile(f"{path}/{item}")) else 0
			meses.sort()
			if result[0] == None:
				for mes in meses:
					df_month = self.iterate_month(path, mes, id)
					if type(df_month) == bool:
						continue
					comando().insertar_batch(df_month, 'observaciones')
			else:
				fechas = pd.DataFrame(pd.date_range(result[0], datetime.now(), freq="1h", inclusive='both'))
				fechas["fecha"] = fechas[0].dt.date
				fechas.drop_duplicates(subset='fecha', inplace = True)
				df_cdc = pd.DataFrame()
				for fecha in fechas["fecha"]:
					folder = fecha.strftime("A%YM%m")
					file = fecha.strftime("DatosEstacion%Y-%m-%d.csv")
					try:
						df = pd.read_csv(f"{path}/{folder}/{file}")
					except:
						logging.error(f"[CDC] No se puede abrir el archivo {path}/{folder}/{file}")
						continue
					df_cdc = pd.concat([df_cdc, df])
				if 'Tiempo Sistema' not in df_cdc.columns:
					continue
				df_cdc = self.transformaciones(df_cdc, id, nombre)
				df_cdc = df_cdc[df_cdc["fecha"] > result[0]]
				if len(df_cdc) == 0:
					continue
				comando().insertar_batch(df_cdc, 'observaciones')
t1 = datetime.now()
if __name__ == "__main__":
	ingesta().iterate_estaciones()
	comando().insertar_max()
print(f"Se demora {datetime.now()- t1}")
