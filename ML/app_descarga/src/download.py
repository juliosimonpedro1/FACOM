import os
import sys
import json
import boto3
import shutil
import logging
import patoolib
import pandas as pd
import xarray as xr
from ftplib import FTP
from datetime import datetime
from urllib.parse import urlparse 

#Cargar parametros
with open("params/params.json", "r") as params:
    params = json.load(params)

carpeta_siata = params["Carpeta-Siata"]
bucket = params["Bucket"]
rar_sufix = params["RAR-sufix"]
ano = sys.argv[1]
meses = pd.date_range(start=f'1/1/{str(ano)}', periods=12, freq='M').to_period(freq = "M").strftime("%Y%m").to_list()
#Set s3 Client
s3 = boto3.client("s3")

#Logging config

os.mkdir(f"logs/{ano}") if ano not in os.listdir("logs") else 0

logging.basicConfig(filename=f"logs/{ano}/download_logs.txt", 
                    filemode='w', 
                    level=logging.INFO, 
                    format = "[%(asctime)s] %(levelname)s:  %(message)s",
                    datefmt = '%Y-%m-%d %H:%M:%S')

def unzip_months(
        mes):
    global rar_sufix

    #Log with ftp
    ftp = params["FTP"]
    ftp = urlparse(ftp)
    ftp = FTP(ftp.netloc)
    ftp.login()
    ftp.cwd(carpeta_siata)
    nombre_archivo_rar = f"{mes}_{rar_sufix}.rar"
    folder = nombre_archivo_rar.split('.')[0]
    try:
        with open(f"tmp/{nombre_archivo_rar}", "wb") as  fp:
            ftp.retrbinary(f"RETR {nombre_archivo_rar}", fp.write)
            size = os.path.getsize(f"tmp/{nombre_archivo_rar}")/1000
            logging.info(f"Se copió con éxito {nombre_archivo_rar} con tamaño {size}")
    except:
        logging.error(f"No se pudo copiar el archivo {nombre_archivo_rar}")
    if folder not in os.listdir("tmp/"):
        os.mkdir(f"tmp/{folder}")
        patoolib.extract_archive(f"tmp/{nombre_archivo_rar}", outdir=f"tmp/{folder}")
    ftp.close()
    return folder

def load_image_s3(
        path_siata):
    global bucket
    fecha = path_siata.split("/")[-1].split(".")[0].split("_")[0]
    fecha = datetime.strptime(fecha, "%Y%m%d%H%M")
    s3.upload_file(
        path_siata, 
        bucket,
        f"{fecha.strftime('%Y')}/{fecha.strftime('%m')}/{fecha.strftime('%d')}/{fecha.strftime('%Y-%m-%d-%H:%M')}_reflectivity.nc")
    return f"{fecha.strftime('%Y-%m-%d-%H:%M')}_reflectivity.nc"

def load_parquet_s3(
        metadata_list,
        path = f"logs/{ano}/metadata_{ano}.parquet"): 
    df = pd.DataFrame(metadata_list, columns=["longitud", "latitud", "fecha","flag","imagen"])
    df.to_parquet(path)
    s3.upload_file(
        path,
        bucket,
        f"{ano}/metadata_{ano}.parquet")
    return 

def extract_metadata_s3(
        netcdf_image):
    dataset = xr.open_dataset(netcdf_image)
    lon = dataset.lon.size
    lat = dataset.lat.size
    datetime = dataset.time.values[0]
    return [lon, lat, datetime]

def delete_zip_folder(
        folder):
    folder = f"tmp/{folder}"
    os.remove(f"{folder}.rar")
    shutil.rmtree(folder)
    return

def walk_month_day(
        mes_carpeta):
    list_metadata = []
    for dia in os.listdir(f"tmp/{mes_carpeta}"):
        dia_fecha = (datetime.strptime(dia, '%Y%m%d')).strftime('%Y-%m-%d')
        logging.info(f"Se está iterando el día {dia_fecha}")
        for imagen in os.listdir(f"tmp/{mes_carpeta}/{dia}"):
            try:
                metadata = extract_metadata_s3(
                        f"tmp/{mes_carpeta}/{dia}/{imagen}")
                metadata.append(1)
            except:
                logging.error(f"No se puede extraer la metadata del archivo tmp/{mes_carpeta}/{dia}/{imagen}")
                metadata = [0]*4
            metadata.append(imagen)
            metadata = tuple(metadata)
            list_metadata.append(metadata)
            try:
                load_image_s3(
                        f"tmp/{mes_carpeta}/{dia}/{imagen}")
            except:
                logging.error(f"Fallo la carga del archivo {imagen}")
                continue
    return list_metadata

list_metadata_ano = []
for mes in meses:
    logging.info(f"Se comienza a trabajar con el mes {mes}")
    try: 
        folder = unzip_months(mes) 
        logging.info(f"Se realizó el Unzip de {mes}")
    except: 
        logging.error(f"Fallo descomprimiendo el {ano}/{mes}")
        continue
    list_metadata_ano.extend(walk_month_day(folder))
    try:
        delete_zip_folder(folder)
        logging.info(f"Se borró con éxito {folder}")
    except:
        logging.error(f"Se encuentra un error borrando {folder}")

load_parquet_s3(
    list_metadata_ano)