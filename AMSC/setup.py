import setuptools

setuptools.setup(
    name="AMSC-library",
    author="Julio Simón Pedro Bohórquez Hurtado",
    author_email="juliosimonpedro@gmail.com",
    packages=setuptools.find_packages())