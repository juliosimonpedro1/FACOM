#!/usr/bin/env python
# coding: utf-8
import time
import GOES
import pandas as pd
import os
from netCDF4 import Dataset
import numpy as np
from datetime import datetime, timedelta

from funciones_goes_v2 import *
from func_download import download_pts
from download_goes_v5 import crop2D

def file_name(city,date):
    return f'{city}_{date}.csv'

def try_dir(path):
    isExist = os.path.exists(path)
    if not isExist:
        os.makedirs(path)

def erase_last(fname):
    #with open(sys.argv[1], "r+", encoding = "utf-8") as file:
    with open(fname, "r+", encoding = "utf-8") as file:

        # Move the pointer (similar to a cursor in a text editor) to the end of the file
        file.seek(0, os.SEEK_END)

        # This code means the following code skips the very last character in the file -
        # i.e. in the case the last line is null we delete the last line
        # and the penultimate one
        pos = file.tell() - 1

        # Read each character in the file one at a time from the penultimate
        # character going backwards, searching for a newline character
        # If we find a new line, exit the search
        while pos > 0 and file.read(1) != "\n":
            pos -= 1
            file.seek(pos, os.SEEK_SET)

        # So long as we're not at the start of the file, delete all the characters ahead
        # of this position
        if pos > 0:
            file.seek(pos, os.SEEK_SET)
            file.truncate()
    file.close()

def less10(nn):
    if nn>=10:
        nn = f'{nn}'
    else:
        nn = f'0{nn}'
    return nn
    
def time_goes_format(dd,HH):
    '''Covierte datetime a formato de descarga para libreria GOES
    INPUT 
    dd: datetime type variable
    HH: huso horario (int)'''
    #T_ini = str(dd)
    dd = dd - timedelta(hours=HH)
    T_ini = f'{dd.year}{less10(dd.month)}{less10(dd.day)}-{less10(dd.hour)}{less10(dd.minute)}00'
    #T_ini = T_ini.replace('-', '')
    #T_ini = T_ini.replace(':', '')
    #T_ini = T_ini.replace(' ', '-')
    #T_ini = T_ini[:15]
    return(T_ini)

def check_last(path,date,pre=False):
    last = get_ipython().getoutput('tail -n 3 "$path"')
    if pre==True:
        date = date+timedelta(days=1)
    #print(len(last))
    if len(last)==3:
        d0 = last[0]
        d1 = last[1]
        d2 = last[2]
        t1 = pd.to_datetime(d1[:16])
        t2 = pd.to_datetime(d2[:16])
        if t2==date:
            dat = [d0,d1]
        elif t2==date-timedelta(hours=1) and t1==date-timedelta(hours=2):
            dat = [d1,d2]
        elif t2==date-timedelta(hours=1) and t1!=date-timedelta(hours=2):
            dat = d2
        elif t2==date-timedelta(hours=2):
            dat = d2
        else:
            dat = False
    elif len(last)==2:
        d1 = 0
        d2 = last[1]
        t2 = pd.to_datetime(d2[:16])
        #print(date-timedelta(hours=2))
        if t2==date-timedelta(hours=1):
            dat = d2
        elif t2==date-timedelta(hours=2):
            dat = d2
        else:
            dat = False
    else:
        dat = False
    return dat

def grab_last(date,name,fder):
    #fname = f'Temperatura_{date.date()}.csv'
    fname = f'{name}_{date.date()}.csv'
    #fder = f'Tablas/Radianzas/A{date.year}/'
    path1 = fder+fname
    
    try_dir(fder)
    
    if date.hour>=2:
        isExist = os.path.exists(path1)
        if isExist==True:
            dat = check_last(path1,date)
        else:
            dat = False
    else:
        pre_d = date - timedelta(days=1)
        pre_name = f'{name}_{pre_d.date()}.csv'
        path2 = fder+pre_name
        isExist = os.path.exists(path2)
        if isExist==True:
            dat = check_last(path2,pre_d,pre=True)
        else:
            dat = False
    return(dat)

def remplazar(ss):
    return ss.replace(' ','_')

principal = '/home/ubuntu/BanRep/Tablas_GOES/'

air = pd.read_csv(principal+'Aeropuertos.csv',usecols=['Ciudad','Lon','Lat'])
air['Ciudad'] = air['Ciudad'].apply(remplazar)
air = air.sort_values(by=['Ciudad'], ignore_index=True)

'''
air = [['LETICIA', -69.942, -4.192],
       ['CALI', -76.382, 3.543],
       ['RIONEGRO',-75.423, 6.164], 
       ['BOGOTA', -74.147, 4.702], 
       ['BARRANQUILLA',-74.781, 10.889],
       ['PEREIRA',-75.739, 4.812],
       ['BUCARAMANGA',-73.184, 7.126],
       ['CUCUTA',-72.511, 7.927],
       ['ARMENIA',-75.766, 4.451],
       ['MONTERIA',-75.825, 8.822],
       ['NEIVA',-75.294, 2.950],
       ['VALLEDUPAR',-73.249, 10.435],
       ['PASTO',-77.291, 1.396],
       ['VILLAVICENCIO',-73.614, 4.168],
       ['IBAGUE',-75.133, 4.421],
       ['BARRANCABERMEJA',-73.806, 7.024],
       ['IPIALES',-77.667, 0.861],
       ['SNICOLAS',-75 - 23/60. - 15.29/3600.,6 + 6/60. + 20.16/3600.],
       ['TURBO', -76.716500, 8.100646]]

air = pd.DataFrame(air,columns=['Ciudad','Lon', 'Lat'])
'''
HH = 0 # Definir huso horario

channel = ['08','09','10','13','14']

#while 1:
date = datetime.today()
#date = datetime.today() + timedelta(hours=1)
#date = datetime(2022,4,5,22,50)
date = datetime(date.year,date.month,date.day,date.hour) # Omitir minutos (LST resolution = 1h)
print(date)
ini = time_goes_format(date,HH)
#fin = time_goes_format(f'{date.date()} {date.hour}:50')
fin = time_goes_format(date+timedelta(minutes=9),HH) #Cambio por latencia

####
# Se debe cambiar el siguiente path a la carpeta donde se ejecuta el script y se guardan los resultados
# 


#principal = '/home/daniel/Documentos/Proyecto_BR/Tablas_GOES/'

#
####

temporal = principal+'img_temp_2/'
try_dir(temporal)

for ch in channel:

    fname = f'rad{ch}_{date.date()}_all.csv'
    dire = principal+f'Tablas/Radianzas/{ch}/A{date.year}/'
    path = dire+fname
    try_dir(dire)

    #png_dir = principal+f'LST_imgs/'

    if not os.path.exists(path):
        file = open(path,'w')
        print('Time',*list(air['Ciudad']),sep=',',file=file)
        file.close()

    last = grab_last(date,f'rad{ch}',dire)

    dd1 = date-timedelta(hours=2)
    dd2 = date-timedelta(hours=1)
    if last==False:
        #print('Descargando anteriores 2 horas')
        t1_i = time_goes_format(dd1,HH)
        t1_f = time_goes_format(dd1+timedelta(minutes=9),HH) #Cambio por latencia
        t2_i = time_goes_format(dd2,HH)
        t2_f = time_goes_format(dd2+timedelta(minutes=9),HH) #Cambio por latencia
        #print(t1_i,t1_f,t2_i,t2_f)
        # Cambiar ['LST'] Por la lista########
        data1 = download_pts(air,t1_i,t1_f,temporal,[ch], save_png=False)
        data2 = download_pts(air,t2_i,t2_f,temporal,[ch], save_png=False)
        #t1 = [str(date-timedelta(hours=2)),*list(data1['LST'].values)] # Meter acá los datos de las bandas
        t1 = [str(date-timedelta(hours=2)),*list(data1[ch].values)]
        t2 = [str(date-timedelta(hours=1)),*list(data1[ch].values)]
        file = open(path,'a')
        if dd1.day==date.day:
            print(*t1,sep=',',file=file)
            print(*t2,sep=',',file=file)
        elif dd1.day<date.day and dd2.day==date.day:
            p_name = f'{name}_{dd1.date()}.csv'
            if not os.path.exists(dire+p_name):
                p_file = open(dire+p_name,'a')
                print('Time',*list(air['Ciudad']),sep=',',file=p_file)
            else: 
                p_file = open(dire+p_name,'a')
            print(*t1,sep=',',file=p_file)
            print(*t2,sep=',',file=file)
            p_file.close()
        else:
            p_name = f'{name}_{dd1.date()}.csv'
            if not os.path.exists(dire+p_name):
                p_file = open(dire+p_name,'a')
                print('Time',*list(air['Ciudad']),sep=',',file=p_file)
            else: 
                p_file = open(dire+p_name,'a')
            print(*t1,sep=',',file=p_file)
            print(*t2,sep=',',file=p_file)
            p_file.close()
        file.close()
    elif len(last)==2:
        t1 = [last[0]]
        t2 = [last[1]]
    else:
        if pd.to_datetime(last[:16])==date-timedelta(hours=1):
            t1_i = time_goes_format(date-timedelta(hours=2),HH)
            t1_f = time_goes_format(date-timedelta(hours=1,minutes=51),HH) #Cambio por latencia
            data1 = download_pts(air,t1_i,t1_f, temporal,[ch]) ### meter Bandas
            t1 = [str(date-timedelta(hours=2)),*list(data1[ch].values)] ### meter Bandas
            t2 = [last]
            t1_day = pd.to_datetime(t1[0][:16]).day
            t2_day = pd.to_datetime(t2[0][:16]).day
            p_name = f'rad{ch}_{dd1.date()}.csv'
            p_path = dire+p_name
            if t2_day<date.day:
                erase_last(p_path)
                file = open(p_path,'a')
                print('',file=file)
                print(*t1,sep=',',file=file)
                print(*t2,sep=',',file=file)
                file.close()
            elif t2_day>date.day and t1_day<date.day:
                p_file = open(p_path,'a')
                file = open(path,'a')
                print(*t1,sep=',',file=p_file)
                print(*t2,sep=',',file=file)
                file.close()
                p_file.close()
            else:
                erase_last(path)
                file = open(path,'a')
                print('',file=file)
                print(*t1,sep=',',file=file)
                print(*t2,sep=',',file=file)
                file.close()
        elif pd.to_datetime(last[:16])==date-timedelta(hours=2):
            t2_i = time_goes_format(date-timedelta(hours=1),HH)
            t2_f = time_goes_format(date-timedelta(minutes=51),HH) #Cambio por latencia
            data2 = download_pts(air,t2_i,t2_f,temporal,[ch])
            t2 = [str(date-timedelta(hours=1)),*list(data1[ch].values)]
            t1 = [last]
            t1_day = pd.to_datetime(t1[0][:16]).day
            t2_day = pd.to_datetime(t2[0][:16]).day
            p_name = f'rad{ch}_{dd1.date()}.csv'
            p_path = dire+p_name
            if t2_day<date.day:
                file = open(p_path,'a')
                print(*t2,sep=',',file=file)
                file.close()
            else:
                file = open(path,'a')
                print(*t2,sep=',',file=file)
                file.close()

    last = get_ipython().getoutput('tail -n 1 "$path"')
    #print(date)
    #print(last)

    ### No guardar imagen

    data3 = download_pts(air,ini,fin,temporal,[ch],save_png=False)
    try:
        time2 = pd.to_datetime(last[:16])
        #print('yes',time2)
    except:
        time2 = pd.to_datetime(t2[0][:16])
    if time2 != date and len(data3[data3[ch].isna()])==0:
        file = open(path,'a')
        t3 = [str(date),*list(data3[ch].values)]
        print(*t3,sep=',',file=file)
        file.close()

    #### No crear gif.

    #make_gif = principal+'create_gif.sh'
    #get_ipython().getoutput('bash "$make_gif"')
