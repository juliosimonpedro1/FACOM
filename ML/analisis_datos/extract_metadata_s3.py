import os
import sys
import boto3
import numpy as np
import pandas as pd
import xarray as xr
from datetime import datetime

ano = sys.argv[1]
meses = list(map(lambda x: (datetime.strptime(str(x), "%m")).strftime("%m"), range(1,13)))

s3 = boto3.client("s3")
Bucket = "historico-siata"


def extract_metadata_s3(
        netcdf_image
):
    dataset = xr.open_dataset(netcdf_image)
    lon = dataset.lon.size
    lat = dataset.lat.size
    datetime = dataset.time.values[0]
    dbzh = dataset.Reflectivity.values
    size = dbzh.size

    no_nan = np.count_nonzero(~np.isnan(dbzh))
    mayor_cero = np.count_nonzero(dbzh > 0)
    mayor_cero = round((mayor_cero / no_nan), 4) *100
    no_nan = round(no_nan / size ,2) *100
    return [lon, lat, datetime, no_nan, mayor_cero]


def iterate_dia(
        ruta_dia
):
    global Bucket
    
    lista_dia = []
    contenido = s3.list_objects_v2(Bucket = Bucket, Prefix = ruta_dia)["Contents"]
    for object in contenido:
        imagen = object["Key"]
        file = imagen.split("/")[-1]
        ruta = f"imagenes/{file}"
        s3.download_file(Bucket, imagen, ruta)
        try:
            metadata = extract_metadata_s3(ruta)
            metadata.append(file)
            lista_dia.append(metadata)
        except:
            continue
        try:
            os.remove(ruta)
        except:
            continue
    df = pd.DataFrame(lista_dia, columns = ["lon", "lat", "fecha", "no_nan", "mayor_cero", "imagen"])
    df.to_csv(f"{ano}_metadata.csv", mode ="a", index = False, header = False)


def iterate_ano(
        ano
):
    dias = pd.date_range(datetime.strptime(f"{ano}/01/01", "%Y/%m/%d"), 
                        datetime.strptime(f"{int(ano) +1}/01/01", "%Y/%m/%d"), freq="d")
    pd.DataFrame(columns = ["lon", "lat", "fecha", "no_nan", "mayor_cero", "imagen"]).to_csv(f"{ano}_metadata.csv", index = False)
    for dia in dias:
        ano_ = dia.strftime("%Y")
        mes = dia.strftime("%m")
        dia = dia.strftime("%d")
        iterate_dia(f"{ano_}/{mes}/{dia}")
iterate_ano(ano)
