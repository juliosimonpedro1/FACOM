# Funcion principal para descarga de imagenes y extraccion de datos
import GOES
import pandas as pd
import os
from netCDF4 import Dataset
import numpy as np
from datetime import datetime, timedelta
from download_goes_v5 import crop2D
from funciones_goes_v2 import *
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap

def less10(nn):
    if nn>=10:
        nn = f'{nn}'
    else:
        nn = f'0{nn}'
    return nn

def download_pts(air,ini,fin,temp,channel,delt = 0.23,save_png=False, HH=0, png_dir = 'LST_imgs/'):
    # Canales GOES:
    #channel = ['08','09','10','13','14','LST']
    # Creamos un dataframe para almacenar los datos
    data = pd.DataFrame(index=list(air['Ciudad']), columns=channel)
    for i in channel:
        ch = [i]
        print(f'Procesando {ch} at {ini}')
        if i=='LST':
            v_name = 'LST'
            flist = GOES.download('goes16', 'ABI-L2-LSTF',
                                  DateTimeIni = ini, DateTimeFin = fin, path_out=temp,
                                  show_download_progress=False)
        else:
            v_name = 'Rad'
            flist = GOES.download('goes16', 'ABI-L1b-RadF', channel=ch,
                                  DateTimeIni = ini, DateTimeFin = fin, path_out=temp, 
                                  show_download_progress=False)
        fname = np.sort(os.listdir(temp))
    
        if len(fname)==0: # In case that there is not image
            print(f'No existe {ch}')
            #if v_name!='LST':
                #if log!=False:
                #    print(f'Not {v_name} image found at {ini}',file=lmiss)
            # Dejamos el valor para este dato igual a NaN
        else:
            # Try por si la imagen no se puede leer
            try:
                ds = Dataset(temp+fname[0])
            except:
                print('No se pudo leer')
                # Dejamos el valor para este dato igual a NaN
                for ff in fname:
                    os.remove(temp+ff)
                continue
            
            if save_png==True and v_name=='LST':
                isExist = os.path.exists(png_dir)
                if not isExist:
                    os.makedirs(png_dir)
                plims = [-5,15]
                llims = [-80,-65]
                y_col,x_col,LST_col,gip = crop2D(ds,'LST')
                #y_col,x_col,LST_col,gip = crop2D(ds,'LST',ltmin = -5, ltmax = 15, lnmin = -80, lnmax=-65)
                sat_h = gip.perspective_point_height
                sat_lon = gip.longitude_of_projection_origin
                
                fig = plt.figure(figsize=(5,5),dpi=180)
                m = Basemap(resolution='l', projection='geos', lon_0=sat_lon,
                            llcrnrx=x_col.min()*sat_h, llcrnry=y_col.min()*sat_h, 
                            urcrnrx=x_col.max()*sat_h, urcrnry=y_col.max()*sat_h,
                           rsphere=(6378137.00,6356752.3142))
                im = m.imshow(LST_col-273.15,origin='upper',cmap='jet')
                m.drawcoastlines()
                m.drawcountries(linewidth=1.5)
                m.drawstates()
                m.drawparallels(np.arange(-10,15,5),linewidth=0.3, 
                                color='black',labels=[True,False,False,True])
                #m.drawmeridians(np.arange(llims[0]+5,-60,5),linewidth=0.3, 
                m.drawmeridians(np.arange(-85,-60,5),linewidth=0.3, 
                                color='black',labels=[True,False,False,True])
                #plt.show()
                #11.34477532775408, -73.30121133565346
                #lat = 11.54477532775408 # latitud
                #lon = -73.30121133565346 # longitud
                #x,y=m(lon,lat)
                #m.plot(x,y,'bo',markersize=0.5)
                
                tname = datetime(int(ini[:4]),int(ini[4:6]),int(ini[6:8]),int(ini[9:11])) -timedelta(hours=HH)

                tname = f'{tname.year}{less10(tname.month)}{less10(tname.day)}-{less10(tname.hour)}0000'
                plt.title(f'LST {tname}')
                plt.clim(278.15-273.15,308.15-273.15)
                plt.colorbar(shrink=0.8, label='Temperatura (°C)')
                plt.savefig(png_dir+f'LST_Col_{tname}.png', bbox_inches='tight')
                plt.close()
            
            # Extraer datos para cada coordenadas
            for jj in range(len(air)):
                city = air['Ciudad'][jj]
                rad,lmin,lmax,pmin,pmax,x_sel,y_sel = sel_region(ds,air['Lon'][jj]-delt, 
                                                                 air['Lon'][jj]+delt,
                                                                 air['Lat'][jj]-delt, 
                                                                 air['Lat'][jj]+delt,
                                                                 v_name)
                data[i][city] = rad.mean()
            # Borro todas las imagenes en el directorio temporal
            for ff in fname:
                os.remove(temp+ff)
    # Cierro el log, si lo hay
    #if log!=False:
    #    lmiss.close()
    #if save_png==True and v_name=='LST':
    #    return data,y_col,x_col,LST_col,gip
    return data
