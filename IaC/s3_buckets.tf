# Buckets de S3 para primer intento de arquitectura
resource "aws_s3_bucket" "radares"{
  bucket = "facom-radares"
  tags = {
    Name        = "Author"
    Environment = "Julio Simon Pedro"
  }
}

resource "aws_organizations_policy" "allow_s3" {
  name        = "AllowS3"
  description = "Allows S3 access for the Radares bucket"
  type        = "SERVICE_CONTROL_POLICY"
  content     = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "s3:ListBucket",
          "s3:PutObject",
          "s3:GetObject",
          "s3:DeleteObject"
        ],
        "Resource": [
          "arn:aws:s3:::${aws_s3_bucket.facom-radares.bucket}",
          "arn:aws:s3:::${aws_s3_bucket.facom-radares.bucket}/*"
        ]
      }
    ]
  })

    policy_type = "SERVICE_CONTROL_POLICY"
  targets     = [aws_organizations_account.this.id]
}