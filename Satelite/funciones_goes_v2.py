import os
import numpy as np
from netCDF4 import Dataset
from pyproj import Proj
import pandas as pd

def sat_info(dataset):
    #dataset = Dataset(image)
    # Height of the satellite's orbit
    x = dataset.variables['x'][:]
    y = dataset.variables['y'][:]
    
    sat_h = dataset.variables['goes_imager_projection'].perspective_point_height

    # Longitude of the satellite's orbit
    sat_lon = dataset.variables['goes_imager_projection'].longitude_of_projection_origin

    # Which direction do we sweep in?
    sat_sweep = dataset.variables['goes_imager_projection'].sweep_angle_axis
    p = Proj(proj='geos', h=sat_h, lon_0=sat_lon, sweep=sat_sweep)
    return(x,y,sat_h,sat_lon,sat_sweep,p)


def sel_region(dataset,lonmin,lonmax,latmin,latmax,v_name):
    x, y, sat_h, sat_lon, sat_sweep, p = sat_info(dataset)
    var = dataset.variables['%s'%v_name][:]
    l_up = p(lonmin,latmax)
    l_down = p(lonmax,latmin)
    y_lims = [l_down[1]/sat_h, l_up[1]/sat_h]
    x_lims = [l_up[0]/sat_h, l_down[0]/sat_h]

    y_sel = np.where((y<=y_lims[1]) & (y_lims[0]<=y))
    x_sel = np.where((x<=x_lims[1]) & (x_lims[0]<=x))
    n_cut = var[y_sel[0][0]:y_sel[0][-1]+1, x_sel[0][0]:x_sel[0][-1]+1]
    #print(n_cut.shape)
    lgmax,ltmax = p(x[x_sel].max()*sat_h, y[y_sel].max()*sat_h,inverse=True)
    lgmin,ltmin = p(x[x_sel].min()*sat_h, y[y_sel].min()*sat_h,inverse=True)
    #print('max value',n_cut.max())
    return(n_cut,lgmin,lgmax,ltmin,ltmax,x_sel,y_sel)