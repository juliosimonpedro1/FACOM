import os
import pandas as pd

packages_size={"Package":[], "Size":[]}
path_deployment = "Cloud/Radares/deployment-package"



def get_size(start_path = '.'):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            if not os.path.islink(fp):
                try:
                    total_size += os.path.getsize(fp)
                except: pass
    return total_size / 1e6

for package in os.listdir(path_deployment):
    print(package)
    size = get_size(os.path.join(path_deployment, package))
    packages_size["Package"].append(package)
    packages_size["Size"].append(size)


packages = pd.DataFrame(packages_size)
packages.sort_values(by = "Size", ascending=True, inplace=True)
packages.to_excel("Cloud/Radares/packages.xlsx", index = False)

