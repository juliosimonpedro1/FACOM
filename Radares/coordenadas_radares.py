import os
import json
import pyart
import boto3
import shutil
import pandas as pd
import xarray as xr
from botocore import UNSIGNED
from botocore.config import Config


s3=boto3.client("s3", config=Config(signature_version=UNSIGNED), region_name="us-east-1")

Bucket="s3-radaresideam"

radares={"Barrancabermeja":"Barrancabermeja",
         "Bogotá":"Bogota",
         "Carimagua":"Carimagua",
         "Corozal":"Corozal",
         "San José del Guaviare":"Guaviare",
         "Cerro Munchique":"Munchique",
         "Tablazo":"Tablazo",
         "Santa Elena":"santa_elena"}

radares_json={}
os.mkdir("Radares/Muestras") if "Muestras" not in os.listdir("Radares") else 0

for radar in radares.keys():
    Prefix = f"l2_data/2022/11/22/{radares[radar]}/"
    paginator=s3.get_paginator('list_objects_v2') 
    pages = paginator.paginate(Bucket = Bucket, Prefix = Prefix)
    
    # Leer toda la data con las llaves y con las fechas de modificacion y ponerlas en un Df

    key_date = {"Key":[], "Date":[]}
    for page in pages:
        contenido = page["Contents"]
        for item in contenido:
            key_date["Key"].append(item["Key"])
            key_date["Date"].append(item["LastModified"])

    key_date = pd.DataFrame(key_date).sort_values(by = 'Date', ascending = True)
    key_date["Diferencia"] = key_date.Date.diff().dt.seconds
    key_date.sort_values(by = 'Date', ascending = False, inplace=True)

    #Crear un bache conlos archivos que se subieron en menos de un minuto
    for i in range(len(key_date)):
        if key_date.iloc[i]["Diferencia"] > 60:
            ult_fecha = i + 1
            break
    key_date = key_date[:ult_fecha]

    radares_json[radar] = {}
    radares_json[radar]["Nombre s3"]=radares[radar]
    radares_json[radar]["Elevacion"] = []
    radares_json[radar]["Coordenadas"] = {}

    for Key in key_date.Key:
        obj = s3.get_object(Bucket = Bucket, Key =Key)["Body"]
        with open(f"Radares/Muestras/{Key.split('/')[-1]}", "wb") as f:
            shutil.copyfileobj(obj,f)
        if "RAW" in Key:
            radar_=pyart.io.read("Radares/Muestras/" + Key.split('/')[-1] )
            lat=radar_.latitude["data"][0]
            lon=radar_.longitude["data"][0]
            elevation = radar_.elevation["data"][0]
        else:
            radar_=xr.open_dataset("Radares/Muestras/"+ Key.split('/')[-1], decode_times=False)
            lat=radar_.variables["latitude"].values
            lon=radar_.variables["longitude"].values           
            elevation = radar_.variables["elevation"].values[0]

        radares_json[radar]["Elevacion"].append(float(elevation)) if float(elevation) not in radares_json[radar]["Elevacion"] else 0
    
    radares_json[radar]["Elevacion"].sort()
    radares_json[radar]["Coordenadas"]["lat"]=float(lat)
    radares_json[radar]["Coordenadas"]["lon"]=float(lon)

if "datos_radares.json" in os.listdir("Radares"):
    os.remove("Radares/datos_radares.json")
with open("Radares/datos_radares.json","w") as f:
    json.dump(radares_json, f,ensure_ascii= False)



    