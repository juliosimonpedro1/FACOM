import pandas as pd
from sqlalchemy import create_engine
import glob
import numpy as np
import datetime as dt
import os
from tqdm import tqdm
import shutil
import sqlite3 as sql
from datetime import timedelta, datetime


#######################################
# ########### ORIENTE #################
#######################################

# Remove DB
os.chdir("/home/ubuntu/Data/DatosMeteoOriente/")
data_base_name = "meteoro-oriente.db"
try:
  os.remove(data_base_name)
except:
  print('No file found')

# Create and Conect Engine with SQLite
engine = create_engine('sqlite:///'+data_base_name)
sqlite_connection = engine.connect()

# Read the data
pth = '/home/ubuntu/Data/DatosMeteoOriente/'
files_only = glob.glob(pth+'*/*.csv')

# Drop columns
cols_to_drop=['Bar Trend', 'Next Record',
    'Inside Temperature', 'Inside Humidity',
    '10 Min Avg Wind Speed', 'Extra Temperatures', 'Soil Temperatures', 'Leaf Temperatures',
    'Extra Humidties', 'Storm Rain', 'Start Date of current Storm',
 #   'Month Rain', 'Year Rain', 'Day ET', 'Month ET', 'Year ET',
    'Day ET', 'Month ET', 'Year ET',
    'Soil Moistures', 'Leaf Wetnesses', 'Inside Alarms', 'Rain Alarms',
    'Outside Alarms', 'Extra Temp/Hum Alarms', 'Soil & Leaf Alarms',
    'Transmitter Battery Status', 'Console Battery Voltage',
    'Forecast Icons', 'Forecast Rule number', 'Time of Sunrise',
    'Time of Sunset', '<LF> = 0x0A', '<CR> = 0x0D', 'CRC']

for i in tqdm(files_only): 
    df = pd.read_csv(i) 
    df.drop(cols_to_drop, axis=1, inplace = True)
    # Apply basic transformations
    df['Tiempo Sistema'] = pd.to_datetime(df['Tiempo Sistema']) - pd.to_timedelta(5,unit='h')
    df['Rain Rate'] = df['Rain Rate']*0.2/60. #in units of cm/hour
    df['Barometer'] = (df['Barometer']/1000.*(3386.389/100.0)) - (1011.0 - 788.0)
    df['Outside Temperature'] = ( df['Outside Temperature']/10. - 32.) * (5.0/9.0)
    df.loc[(df['Outside Temperature'] > 50) | (df['Outside Temperature'] < -15)] = np.nan
    df.to_sql(name='DatosMeteoro',con=sqlite_connection,index=False,if_exists='append') 

sqlite_connection.close()
shutil.copy('meteoro-oriente.db', '/home/ubuntu/scripts/')

#######################################
# ########### TURBO #################
#######################################

# Remove DB
os.chdir("/home/ubuntu/Data/DatosMeteoTurbo/")
data_base_name = "meteoro-turbo.db"
try:
  os.remove(data_base_name)
except:
  print('No file found')

# Create and Conect Engine with SQLite
engine = create_engine('sqlite:///'+data_base_name)
sqlite_connection = engine.connect()

# Read the data
pth = '/home/ubuntu/Data/DatosMeteoTurbo/'
files_only = glob.glob(pth+'*/*.csv')

# Drop columns
cols_to_drop=['Bar Trend', 'Next Record',
    'Inside Temperature', 'Inside Humidity',
    '10 Min Avg Wind Speed', 'Extra Temperatures', 'Soil Temperatures', 'Leaf Temperatures',
    'Extra Humidties', 'Storm Rain', 'Start Date of current Storm',
 #   'Month Rain', 'Year Rain', 'Day ET', 'Month ET', 'Year ET',
    'Day ET', 'Month ET', 'Year ET',
    'Soil Moistures', 'Leaf Wetnesses', 'Inside Alarms', 'Rain Alarms',
    'Outside Alarms', 'Extra Temp/Hum Alarms', 'Soil & Leaf Alarms',
    'Transmitter Battery Status', 'Console Battery Voltage',
    'Forecast Icons', 'Forecast Rule number', 'Time of Sunrise',
    'Time of Sunset', '<LF> = 0x0A', '<CR> = 0x0D', 'CRC']

for i in tqdm(files_only):
    df = pd.read_csv(i)
    df.drop(cols_to_drop, axis=1, inplace = True)
    # Apply basic transformations
    df['Tiempo Sistema'] = pd.to_datetime(df['Tiempo Sistema'])# - pd.to_timedelta(5,unit='h')
    df['Rain Rate'] = df['Rain Rate']*0.2/60. #in units of cm/hour
    df['Barometer'] = (df['Barometer']/1000.*(3386.389/100.0)) - (1011.0 - 788.0)
    df['Outside Temperature'] = ( df['Outside Temperature']/10. - 32.) * (5.0/9.0)
    df.loc[(df['Outside Temperature'] > 50) | (df['Outside Temperature'] < -15)] = np.nan
    df.to_sql(name='DatosMeteoro',con=sqlite_connection,index=False,if_exists='append')

sqlite_connection.close()
shutil.copy('meteoro-turbo.db', '/home/ubuntu/scripts/')

#######################################
# ########### Sonson #################
#######################################


# Remove DB
os.chdir("/home/ubuntu/Data/DatosMeteoSonson/")


data_base_name = "meteoro-sonson.db"
try:
  os.remove(data_base_name)
except:
  print('No file found')

# Create and Conect Engine with SQLite
engine = create_engine('sqlite:///'+data_base_name)
sqlite_connection = engine.connect()

# Read the data
pth = '/home/ubuntu/Data/DatosMeteoSonson/'

files_only = glob.glob(pth+'*/*.csv')

# Drop columns
cols_to_drop=['Bar Trend', 'Next Record',
    'Inside Temperature', 'Inside Humidity',
    '10 Min Avg Wind Speed', 'Extra Temperatures', 'Soil Temperatures', 'Leaf Temperatures',
    'Extra Humidties', 'Storm Rain', 'Start Date of current Storm',
 #   'Month Rain', 'Year Rain', 'Day ET', 'Month ET', 'Year ET',
    'Day ET', 'Month ET', 'Year ET',
    'Soil Moistures', 'Leaf Wetnesses', 'Inside Alarms', 'Rain Alarms',
    'Outside Alarms', 'Extra Temp/Hum Alarms', 'Soil & Leaf Alarms',
    'Transmitter Battery Status', 'Console Battery Voltage',
    'Forecast Icons', 'Forecast Rule number', 'Time of Sunrise',
    'Time of Sunset', '<LF> = 0x0A', '<CR> = 0x0D', 'CRC']

for i in tqdm(files_only):
    df = pd.read_csv(i)
    df.drop(cols_to_drop, axis=1, inplace = True)
    # Apply basic transformations
    df['Tiempo Sistema'] = pd.to_datetime(df['Tiempo Sistema'])# - pd.to_timedelta(5,unit='h')
    df['Rain Rate'] = df['Rain Rate']*0.2/60. #in units of cm/hour
    df['Barometer'] = (df['Barometer']/1000.*(3386.389/100.0)) - (1011.0 - 788.0)
    df['Outside Temperature'] = ( df['Outside Temperature']/10. - 32.) * (5.0/9.0)
    df.loc[(df['Outside Temperature'] > 50) | (df['Outside Temperature'] < -15)] = np.nan
    df.to_sql(name='DatosMeteoro',con=sqlite_connection,index=False,if_exists='append')

sqlite_connection.close()
shutil.copy('meteoro-sonson.db', '/home/ubuntu/scripts/') 


#######################################
# ########### Yarumal #################
#######################################

# Remove DB
os.chdir("/home/ubuntu/Data/DatosMeteoYarumal/")


data_base_name = "meteoro-yarumal.db"
try:
  os.remove(data_base_name)
except:
  print('No file found')

# Create and Conect Engine with SQLite
engine = create_engine('sqlite:///'+data_base_name)
sqlite_connection = engine.connect()

# Read the data
pth = '/home/ubuntu/Data/DatosMeteoYarumal/'

files_only = glob.glob(pth+'*/*.csv')

# Drop columns
cols_to_drop=['Bar Trend', 'Next Record',
    'Inside Temperature', 'Inside Humidity',
    '10 Min Avg Wind Speed', 'Extra Temperatures', 'Soil Temperatures', 'Leaf Temperatures',
    'Extra Humidties', 'Storm Rain', 'Start Date of current Storm',
 #   'Month Rain', 'Year Rain', 'Day ET', 'Month ET', 'Year ET',
    'Day ET', 'Month ET', 'Year ET',
    'Soil Moistures', 'Leaf Wetnesses', 'Inside Alarms', 'Rain Alarms',
    'Outside Alarms', 'Extra Temp/Hum Alarms', 'Soil & Leaf Alarms',
    'Transmitter Battery Status', 'Console Battery Voltage',
    'Forecast Icons', 'Forecast Rule number', 'Time of Sunrise',
    'Time of Sunset', '<LF> = 0x0A', '<CR> = 0x0D', 'CRC']

for i in tqdm(files_only):
    df = pd.read_csv(i)
    df.drop(cols_to_drop, axis=1, inplace = True)
    # Apply basic transformations
    df['Tiempo Sistema'] = pd.to_datetime(df['Tiempo Sistema'])# - pd.to_timedelta(5,unit='h')
    df['Rain Rate'] = df['Rain Rate']*0.2/60. #in units of cm/hour
    df['Barometer'] = (df['Barometer']/1000.*(3386.389/100.0)) - (1011.0 - 788.0)
    df['Outside Temperature'] = ( df['Outside Temperature']/10. - 32.) * (5.0/9.0)
    df.loc[(df['Outside Temperature'] > 50) | (df['Outside Temperature'] < -15)] = np.nan
    df.to_sql(name='DatosMeteoro',con=sqlite_connection,index=False,if_exists='append')

sqlite_connection.close()
shutil.copy('meteoro-yarumal.db', '/home/ubuntu/scripts/') 


# #Agregar tabla de stats a la base de datos. 

# from funciones_server import cd_anom, abrir_stats

# cd_anom("/home/ubuntu/scripts/meteoro-oriente.db","DatosMeteoro")
# cd_anom("/home/ubuntu/scripts/meteoro-turbo.db","DatosMeteoro")
# cd_anom("/home/ubuntu/scripts/meteoro-sonson.db","DatosMeteoro")
# cd_anom("/home/ubuntu/scripts/meteoro-yarumal.db","DatosMeteoro")

# #Agregar Composite

# def tabla_anom(est_escog,anom):

#        variables=["Outside Temperature","Outside Humidity","Barometer", "Solar Radiation"]
#        # Agregar el acumulado de precipitación del rango
#        est_escog["Mes"]=est_escog.index.month
#        est_escog["Hora-Min"]=est_escog.index.strftime("%H:%M")
#        est_escog.reset_index(inplace=True)
#        anomalias=pd.merge(est_escog,anom,left_on=["Mes", "Hora-Min"], right_on=["Mes", "Hora-Min"], how="left")
#        anomalias.set_index("Tiempo Sistema", inplace=True)
#        est_escog.set_index("Tiempo Sistema", inplace=True)
#        for i in variables:
#               anomalias["Anomaly "+i]=anomalias[i]-anomalias["mean "+i]

#        anomalias=anomalias.resample("5min").mean()
#        anomalias["ppt_acumulado"]=anomalias["Rain Rate"].cumsum()
#        return anomalias

# def composite(db,resolucion):
#     path="/home/ubuntu/scripts/"+db
#     con=sql.connect(path)

#     variables=["Tiempo Sistema","Outside Temperature","Outside Humidity",
#             "Barometer","Rain Rate","Solar Radiation"]

#     string=""
#     c=1
#     for i in variables:
#         string+='"'+i+'"'
#         if c<len(variables):
#             string+=","
#         c+=1

#     query=f'SELECT {string} from DatosMeteoro'
#     df=pd.read_sql(query,con)
#     df["Tiempo Sistema"]=pd.to_datetime(df["Tiempo Sistema"],format="%Y-%m-%d %H:%M:%S")
#     df.sort_values(by="Tiempo Sistema",ascending=True,inplace=True)
#     df.dropna(axis=0,subset=["Tiempo Sistema"],inplace=True)

#     df.set_index("Tiempo Sistema",inplace=True)
#     stats=abrir_stats(path,np.arange(1,13,1))
#     df=tabla_anom(df,stats) 
#     df.reset_index(inplace=True)

#     rain=df[df["Rain Rate"]>0]
#     rain["Evento"]=1

#     evento=1
#     c=0
#     for i in range(rain.shape[0]):

#         loc=rain.columns.get_loc("Evento")
#         rain.iloc[c,loc]=evento 
#         if c==rain.shape[0]-1:
#             break
#         loc=rain.columns.get_loc("Tiempo Sistema")
#         if (rain.iloc[c+1,loc]-rain.iloc[c,loc]).total_seconds() >= 60*15:
#             evento+=1
#         c+=1
    
#     df=pd.merge(df,rain[["Tiempo Sistema","Evento"]],
#                 how="left",left_on="Tiempo Sistema",right_on="Tiempo Sistema")
#     df.reset_index(inplace=True)

#     minutos=resolucion #Cambiar si se quiere otra resolucióm
#     composite_df=pd.DataFrame()
#     composite_df["Hora"]=np.arange(-360,181,minutos)
#     composite_df.set_index("Hora",inplace=True)
#     df.set_index("Tiempo Sistema",inplace=True)
#     df=df.resample(f"{str(minutos)}min").mean()
#     df.reset_index(inplace=True)
#     df.drop(axis=1,labels=["index"],inplace=True)
    
#     variables=["Rain Rate", "Barometer", "Outside Temperature","Outside Humidity",
#                 "Anomaly Outside Temperature", "Anomaly Outside Humidity","Anomaly Barometer"]
#     # variables=["Rain Rate", "Barometer", "Outside Temperature","Outside Humidity"]

#     keep=variables.copy()
#     for i in ["Evento","Tiempo Sistema"]:
#         keep.append(i)
#     df=df[keep]
#     for i in variables:
#         composite_df[i]=np.nan

#     evendic=[]
#     for i in df["Evento"].dropna().drop_duplicates():
#         cero=df["Tiempo Sistema"][df["Evento"]==i].min()
#         even=df[(df["Tiempo Sistema"]>=cero-timedelta(hours=6))\
#                 & (df["Tiempo Sistema"]<=cero+timedelta(hours=3))]
#         if len(even) < len(np.arange(-360,181,minutos)):
#             fechas=pd.date_range(cero-timedelta(hours=6),cero+timedelta(hours=3),
#                             freq=f"{str(minutos)}min").to_frame(index=True)
#             even.set_index("Tiempo Sistema",inplace=True)
#             even=pd.merge(fechas,even,how="outer",left_index=True,right_index=True)
#             even.reset_index(inplace=True)
#             even.drop(0,axis=1,inplace=True)
#             even.rename({"index":"Tiempo Sistema"},axis=1,inplace=True)
#         even["Hora"]=np.arange(-360,181,minutos)
#         evendic.append(even)

#     for hora in tqdm(composite_df.index):
        
#         for variable in variables:
#             valor=[]
#             for event in evendic:
#                 valor.append(event[variable][event["Hora"]==hora].values[0])
#             composite_df.loc[hora,[variable]]=np.nanmean(valor)
#     # composite_df.index=pd.to_datetime(composite_df.index,unit="m").strftime("%H:%M")

#     composite_df.to_sql("Composite",con=con, if_exists="replace")
#     con.commit()
#     con.close()

# dbs={}
# estaciones=["Oriente","Turbo","Sonson","Yarumal"]

# for i in estaciones:
#     dbs[i]="meteoro-"+i.lower()+".db"
#     composite(dbs[i],5)



# # fin del código